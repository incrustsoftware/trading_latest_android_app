package com.myfixcost.trade.Dashboard;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.myfixcost.trade.Dashboard.Orders_Menu.Trade_Orders_Fragment;
import com.myfixcost.trade.Dashboard.Position_Menu.Trade_Position_Fragment;
import com.myfixcost.trade.Dashboard.Settings_Menu.Trade_Settings_Fragment;
import com.myfixcost.trade.Dashboard.Trade.Trade_Fragment;
import com.myfixcost.trade.Dashboard.Watchlist_Menu.Trade_Watchlist_Fragment;
import com.myfixcost.trade.R;
import com.myfixcost.trade.UIViews.LogOutTimerUtil;

public class Trade_Dashboard_Activity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, LogOutTimerUtil.LogOutListener {

    Fragment fragment;
    protected BottomNavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trade_dashboard_activity);

        navigationView = findViewById(R.id.navigation);
        navigationView.setOnNavigationItemSelectedListener(this);

        loadFragment(new Trade_Position_Fragment());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        fragment = null;
        switch (item.getItemId()) {
            case R.id.navigation_position:
                fragment = new Trade_Position_Fragment();
                break;

            case R.id.navigation_trade:
                fragment = new Trade_Fragment();
                break;

            case R.id.navigation_watchlist:
                fragment = new Trade_Watchlist_Fragment();
                break;

            case R.id.navigation_orders:
                fragment = new Trade_Orders_Fragment();
                break;

            case R.id.navigation_settings:
                fragment = new Trade_Settings_Fragment();
                break;
        }
        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public void doLogout() {
    finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}
