package com.myfixcost.trade.UIViews;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DiffUtil;

import com.myfixcost.trade.Dashboard.Position_Menu.Trade_Position_Array;

import java.util.ArrayList;

public class MyDiffUtilCallBack extends DiffUtil.Callback {
    ArrayList<Trade_Position_Array> newList;
    ArrayList<Trade_Position_Array> oldList;


    public MyDiffUtilCallBack(ArrayList<Trade_Position_Array> newList, ArrayList<Trade_Position_Array> oldList) {
        this.newList = newList;
        this.oldList = oldList;
    }

    @Override
    public int getOldListSize() {
        return oldList != null ? oldList.size() : 0;
    }

    @Override
    public int getNewListSize() {
        return newList != null ? newList.size() : 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return newList.get(newItemPosition).getTicker() == oldList.get(oldItemPosition).getTicker();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        int result = newList.get(newItemPosition).getTicker().compareTo(oldList.get(oldItemPosition).getTicker());
        return result == 0;
    }

    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {

        Trade_Position_Array newModel = newList.get(newItemPosition);
        Trade_Position_Array oldModel = oldList.get(oldItemPosition);

        Bundle diff = new Bundle();

        if (newModel.getMarketPrice() != (oldModel.getMarketPrice())) {
            diff.putInt("price", Integer.parseInt(newModel.getMarketPrice()));
        }
        if (diff.size() == 0) {
            return null;
        }
        return diff;
        //return super.getChangePayload(oldItemPosition, newItemPosition);
    }
}
