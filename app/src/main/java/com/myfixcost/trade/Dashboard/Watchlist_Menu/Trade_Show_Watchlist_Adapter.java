package com.myfixcost.trade.Dashboard.Watchlist_Menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.myfixcost.trade.Dashboard.Trade.Trade_get_watchlist_array;
import com.myfixcost.trade.R;

import java.util.ArrayList;

public class Trade_Show_Watchlist_Adapter extends RecyclerView.Adapter<Trade_Show_Watchlist_Adapter.ViewHolder> {
    ArrayList<Trade_get_watchlist_array> trade_get_watchlist_arrays = new ArrayList<>();
    Context context;
    //    int lastSelectedPosition=-1;
    int lastSelectedPosition = -1;
    int firstPosition = 0;
    boolean state = true;
    Trade_watchlist_click_interface trade_watchlist_click_interface;

    // RecyclerView recyclerView;
    public Trade_Show_Watchlist_Adapter(ArrayList<Trade_get_watchlist_array> trade_get_watchlist_arrays, Trade_watchlist_click_interface trade_watchlist_click_interface, Context context) {
        this.trade_get_watchlist_arrays = trade_get_watchlist_arrays;
        this.trade_watchlist_click_interface = trade_watchlist_click_interface;
        this.context = context;
        firstPosition = 0;

    }

    @Override
    public Trade_Show_Watchlist_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.trade_show_watchlist_adapter, parent, false);
        Trade_Show_Watchlist_Adapter.ViewHolder viewHolder = new Trade_Show_Watchlist_Adapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Trade_Show_Watchlist_Adapter.ViewHolder holder, int position) {
        final Trade_get_watchlist_array myListData = trade_get_watchlist_arrays.get(position);
        holder.txt_watchlist_name.setText(myListData.getWatchName());

        if(position == firstPosition){
            holder.lin_watchlist_line.setVisibility(View.VISIBLE);
            holder.lin_watchlist_line.setBackgroundColor(ContextCompat.getColor(context, R.color.watchlist_line_color));
            holder.txt_watchlist_name.setTextColor(ContextCompat.getColor(context, R.color.app_color));
        } else {
            //holder.itemView.setSelected(false);
            holder.lin_watchlist_line.setVisibility(View.GONE);
            holder.lin_watchlist_line.setBackgroundColor(ContextCompat.getColor(context, R.color.watchlist_line_color));
            holder.txt_watchlist_name.setTextColor(ContextCompat.getColor(context, R.color.watchlist_color));
        }

        holder.lin_watch_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trade_watchlist_click_interface.getwatchlistdata(myListData.getM_id(), "0", myListData.getWatchName());
                int currentPosition = holder.getLayoutPosition();
//                lastSelectedPosition = position;
//                notifyDataSetChanged();

                if(firstPosition != currentPosition){
                    int lastSelectedPosition = firstPosition;
                    firstPosition = currentPosition;
                    notifyItemChanged(lastSelectedPosition);
                    notifyDataSetChanged();
                }
            }

        });

        holder.lin_watch_click.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                trade_watchlist_click_interface.getwatchlistdata(myListData.getM_id(), "1", myListData.getWatchName());
                return false;
            }
        });

//        if (lastSelectedPosition != -1 && lastSelectedPosition == position) {
//            holder.lin_watchlist_line.setVisibility(View.VISIBLE);
//            holder.lin_watchlist_line.setBackgroundColor(ContextCompat.getColor(context, R.color.watchlist_line_color));
//            holder.txt_watchlist_name.setTextColor(ContextCompat.getColor(context, R.color.app_color));
//            state = false;
//
//        } else {
//            holder.lin_watchlist_line.setVisibility(View.GONE);
//            holder.lin_watchlist_line.setBackgroundColor(ContextCompat.getColor(context, R.color.watchlist_line_color));
//            holder.txt_watchlist_name.setTextColor(ContextCompat.getColor(context, R.color.watchlist_color));
//            state = true;
//        }
    }

    @Override
    public int getItemCount() {
        return trade_get_watchlist_arrays.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_watchlist_name;
        LinearLayout lin_watch_click, lin_watchlist_line;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txt_watchlist_name = itemView.findViewById(R.id.txt_watchlist_name);
            this.lin_watch_click = itemView.findViewById(R.id.lin_watch_click);
            this.lin_watchlist_line = itemView.findViewById(R.id.lin_watchlist_line);
        }
    }
}
