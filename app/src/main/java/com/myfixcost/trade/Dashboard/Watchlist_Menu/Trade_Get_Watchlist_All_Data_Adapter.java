package com.myfixcost.trade.Dashboard.Watchlist_Menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;

import java.util.ArrayList;

public class Trade_Get_Watchlist_All_Data_Adapter extends RecyclerView.Adapter<Trade_Get_Watchlist_All_Data_Adapter.ViewHolder> {
    ArrayList<Trade_show_watchlist_array> trade_show_watchlist_arrays = new ArrayList<>();
    Context context;
    int lastSelectedPosition = -1;
    boolean state = true;
    String convertedDate;
    MySharedPreference mySharedPreference;

    // RecyclerView recyclerView;
    public Trade_Get_Watchlist_All_Data_Adapter(ArrayList<Trade_show_watchlist_array> trade_show_watchlist_arrays, Context context) {
        this.trade_show_watchlist_arrays = trade_show_watchlist_arrays;
        this.context = context;

    }

    @Override
    public Trade_Get_Watchlist_All_Data_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.trade_get_watchlist_data_adapter, parent, false);
        Trade_Get_Watchlist_All_Data_Adapter.ViewHolder viewHolder = new Trade_Get_Watchlist_All_Data_Adapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Trade_Get_Watchlist_All_Data_Adapter.ViewHolder holder, int position) {
        final Trade_show_watchlist_array myListData = trade_show_watchlist_arrays.get(position);
        mySharedPreference = new MySharedPreference(context);

        holder.txt_ticker_name.setText(myListData.getTicker());
//        convertedDate = mySharedPreference.dateFormatter(context, "Canada", myListData.getContractMonth());
        holder.txt_contract_month.setText(myListData.getContractMonth());
        holder.txt_price_change.setText(myListData.getPriceChange());

        String today_change = myListData.getTodayChange();
        today_change = today_change.replace("$", "");

        if (myListData.getTodayChange().startsWith("-")) {

            holder.txt_today_change.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
            holder.txt_today_change.setText("$" + "(" + today_change + ")");
        } else {
            holder.txt_today_change.setTextColor(ContextCompat.getColor(context, R.color.app_color));
            holder.txt_today_change.setText("$" + today_change);
        }

    }

    public void filterList(ArrayList<Trade_show_watchlist_array> trade_show_watchlist_arrays) {
        this.trade_show_watchlist_arrays = trade_show_watchlist_arrays;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return trade_show_watchlist_arrays.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_ticker_name, txt_contract_month, txt_price_change, txt_today_change;
        LinearLayout lin_watchlist_data;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txt_ticker_name = itemView.findViewById(R.id.txt_ticker_name);
            this.txt_contract_month = itemView.findViewById(R.id.txt_contract_month);
            this.txt_price_change = itemView.findViewById(R.id.txt_price_change);
            this.txt_today_change = itemView.findViewById(R.id.txt_today_change);
            this.lin_watchlist_data = itemView.findViewById(R.id.lin_watchlist_data);

        }
    }
}
