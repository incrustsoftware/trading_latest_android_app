package com.myfixcost.trade.Dashboard.Trade;

public class Trade_get_watchlist_array {

    String M_id, User_id, WatchName;

    public Trade_get_watchlist_array(String m_id, String user_id, String watchName) {
        M_id = m_id;
        User_id = user_id;
        WatchName = watchName;
    }

    public Trade_get_watchlist_array() {

    }

    public String getM_id() {
        return M_id;
    }

    public void setM_id(String m_id) {
        M_id = m_id;
    }

    public String getUser_id() {
        return User_id;
    }

    public void setUser_id(String user_id) {
        User_id = user_id;
    }

    public String getWatchName() {
        return WatchName;
    }

    public void setWatchName(String watchName) {
        WatchName = watchName;
    }


}
