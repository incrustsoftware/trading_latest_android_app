package com.myfixcost.trade.Dashboard.Position_Menu;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Trade_Position_Adapter extends RecyclerView.Adapter<Trade_Position_Adapter.ViewHolder> {
    public ArrayList<Trade_Position_Array> trade_position_arrays = new ArrayList<>();
    Context context;
    int lastSelectedPosition = -1;
    boolean state = true;
    String convertedDate;
    MySharedPreference mySharedPreference;
    DecimalFormat decimalFormat;

    // RecyclerView recyclerView;
    public Trade_Position_Adapter(ArrayList<Trade_Position_Array> trade_position_arrays, Context context) {
        this.trade_position_arrays = trade_position_arrays;
        this.context = context;

    }

    @Override
    public Trade_Position_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.trade_position_adapter, parent, false);
        Trade_Position_Adapter.ViewHolder viewHolder = new Trade_Position_Adapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Trade_Position_Adapter.ViewHolder holder, int position) {
        final Trade_Position_Array myListData = trade_position_arrays.get(position);

        mySharedPreference = new MySharedPreference(context);
        if (myListData.getTicker().equalsIgnoreCase("Total")) {
            holder.txt_ticker_name.setText(myListData.getTicker());
            holder.txt_ticker_name.setTextColor(ContextCompat.getColor(context, R.color.login_text_color));
            Typeface typeface = ResourcesCompat.getFont(context, R.font.lato_bold);
            holder.txt_ticker_name.setTypeface(typeface);
            holder.txt_ticker_count.setVisibility(View.GONE);
            holder.txt_contract_month.setText("");
            holder.txt_market_price.setText("");
            holder.txt_buy_price.setText("");

            if (myListData.getTotalPLOpen().startsWith("-")) {
                String pl_open_total = myListData.getTotalPLOpen().replace("-", "");
                Double d_pl_open = Double.valueOf(pl_open_total);
                decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
                holder.txt_pl_open.setText("$" + "(" + decimalFormat.format((d_pl_open)) + ")");
                holder.txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
            } else {
                holder.txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                Double d_pl_open = Double.valueOf(myListData.getTotalPLOpen());
                decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
                holder.txt_pl_open.setText("$" + decimalFormat.format(d_pl_open));
            }

            if (myListData.getTotalPLToday().startsWith("-")) {
                String pl_today_total = myListData.getTotalPLToday().replace("-", "");
                Double d_pl_today = Double.valueOf(pl_today_total);
                decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
                holder.txt_pl_today.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                holder.txt_pl_today.setText("$" + "(" + decimalFormat.format(d_pl_today) + ")");
            } else {
                Double d_pl_today = Double.valueOf(myListData.getTotalPLToday());
                decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
                holder.txt_pl_today.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                holder.txt_pl_today.setText("$" + decimalFormat.format(d_pl_today));
            }

            if (myListData.getTotalPLPercent().startsWith("-")) {
                String pl_percentage_total = myListData.getTotalPLPercent().replace("-", "");
                holder.txt_pl_percent.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                holder.txt_pl_percent.setText(pl_percentage_total + "%");
            } else {
                holder.txt_pl_percent.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                holder.txt_pl_percent.setText(myListData.getTotalPLPercent() + "%");
            }
        } else {
            holder.txt_ticker_name.setText(myListData.getTicker());
//            convertedDate = mySharedPreference.dateFormatter(context, "", myListData.getContractMonth());
            holder.txt_contract_month.setText(myListData.getContractMonth());

            Double d_pl_marketprice = Double.valueOf(myListData.getMarketPrice());
            decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
            holder.txt_market_price.setText("$" + decimalFormat.format(d_pl_marketprice));

            if (myListData.getQuantity().startsWith("-")) {
                String quantity = myListData.getQuantity().replace("-", "");
                holder.txt_ticker_count.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                holder.txt_ticker_count.setText("\"" + "-" + quantity + "\"");
            } else {
                holder.txt_ticker_count.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                holder.txt_ticker_count.setText("\"" + "+" + myListData.getQuantity() + "\"");
            }

            if (myListData.getPLOpen().startsWith("-")) {
                String pl_open = myListData.getPLOpen().replace("-", "");
                Double d_pl_open = Double.valueOf(pl_open);
                decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
                holder.txt_pl_open.setText("$" + "(" + decimalFormat.format((d_pl_open)) + ")");
                holder.txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));

            } else {
                holder.txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                Double d_pl_open = Double.valueOf(myListData.getPLOpen());
                decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
                holder.txt_pl_open.setText("$" + decimalFormat.format(d_pl_open));
            }

            if (myListData.getPLToday().startsWith("-")) {
                String pl_today = myListData.getPLToday().replace("-", "");
                Double d_pl_today = Double.valueOf(pl_today);
                decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
                holder.txt_pl_today.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                holder.txt_pl_today.setText("$" + "(" + decimalFormat.format(d_pl_today) + ")");
            } else {
                Double d_pl_today = Double.valueOf(myListData.getPLToday());
                decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
                holder.txt_pl_today.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                holder.txt_pl_today.setText("$" + decimalFormat.format(d_pl_today));
            }

            if (myListData.getPLPercent().startsWith("-")) {
                String pl_percentage = myListData.getPLPercent().replace("-", "");
                holder.txt_pl_percent.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                holder.txt_pl_percent.setText(pl_percentage + "%");
            } else {
                holder.txt_pl_percent.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                holder.txt_pl_percent.setText(myListData.getPLPercent() + "%");
            }

            Double d_pl_buyprice = Double.valueOf(myListData.getBuyPrice());
            decimalFormat = mySharedPreference.NumberDecimalFormatter(context, "");
            holder.txt_buy_price.setText("$" + decimalFormat.format(d_pl_buyprice));

        }

//        if (lastSelectedPosition != -1 && lastSelectedPosition == position) {
//            holder.img_tick.setVisibility(View.VISIBLE);
//            holder.txt_doc.setTextColor(ContextCompat.getColor(context, R.color.app_background_color));
//            final Typeface typeface = ResourcesCompat.getFont(context, R.font.opensans_semibold);
//            holder.txt_doc.setTypeface(typeface);
//
//            state = false;
//        }else{
//            holder.img_tick.setVisibility(View.GONE);
//            holder.txt_doc.setTextColor(ContextCompat.getColor(context, R.color.bottomsheet_doc_color));
//            final Typeface typeface = ResourcesCompat.getFont(context, R.font.opensans_regular);
//            holder.txt_doc.setTypeface(typeface);
//            state = true;
//
//        }


    }

    @Override
    public int getItemCount() {
        return trade_position_arrays.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_ticker_name, txt_ticker_count, txt_contract_month, txt_market_price, txt_pl_open, txt_pl_today, txt_buy_price, txt_pl_percent;
        LinearLayout lin_position_data;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txt_ticker_name = itemView.findViewById(R.id.txt_ticker_name);
            this.txt_ticker_count = itemView.findViewById(R.id.txt_ticker_count);
            this.txt_contract_month = itemView.findViewById(R.id.txt_contract_month);
            this.txt_market_price = itemView.findViewById(R.id.txt_market_price);
            this.txt_pl_open = itemView.findViewById(R.id.txt_pl_open);
            this.txt_pl_today = itemView.findViewById(R.id.txt_pl_today);
            this.txt_buy_price = itemView.findViewById(R.id.txt_buy_price);
            this.txt_pl_percent = itemView.findViewById(R.id.txt_pl_percent);
            this.lin_position_data = itemView.findViewById(R.id.lin_position_data);
        }
    }
}
