package com.myfixcost.trade.Dashboard.Position_Menu;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.myfixcost.trade.R;

public class Trade_Position_Dropdown_Adapter extends BaseAdapter implements SpinnerAdapter {
    String[] cars;
    Context context;
    int selectedCar;

    public Trade_Position_Dropdown_Adapter(Context context, String[] cars, int selectedCar) {
        this.cars = cars;
        this.context = context;
        this.selectedCar = selectedCar;
    }

    @Override
    public int getCount() {
        return cars.length;
    }

    @Override
    public Object getItem(int position) {
        return cars[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = View.inflate(context, R.layout.trade_position_text, null);
        TextView textView = (TextView) view.findViewById(R.id.txt_position);
        textView.setText(cars[position]);
        if (selectedCar == position) {
            textView.setTextColor((context.getResources().getColor(R.color.app_color)));
        } else if (position == 1) {
            textView.setTextColor((context.getResources().getColor(R.color.color_paper)));
        }

        return textView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View view;
        view = View.inflate(context, R.layout.position_dropdown, null);
        TextView textView = (TextView) view.findViewById(R.id.txt_position_name);
        textView.setText(cars[position]);
        if (selectedCar == position) {
            textView.setTextColor((context.getResources().getColor(R.color.app_color)));
        } else if (position == 1) {
            textView.setTextColor((context.getResources().getColor(R.color.color_paper)));
        }

        return view;
    }
}
