package com.myfixcost.trade.Dashboard.Position_Menu;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.Constant.MyGlobalClass;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;
import com.myfixcost.trade.UIViews.LinearLayoutManagerWrapper;
import com.myfixcost.trade.UIViews.RecyclerItemClickListener;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

public class Trade_Position_Fragment extends Fragment {

    RecyclerView rec_position;
    Trade_Position_Adapter trade_position_adapter;
    public ArrayList<Trade_Position_Array> trade_position_arrays = new ArrayList<>();
    TextView txt_pl_day, txt_pl_open, txt_net_liquidity, txt_available_funds;
    MySharedPreference mySharedPreference;
    Spinner spinner_acc_type;
    String account_type = "live", Guest_user;
    ArrayAdapter<String> adapter;
    private String[] acc_type = {"Live", "Paper"};
    public static int selectedCar = 0;
    DecimalFormat decimalFormat;
    Handler handler;
    Dialog dialog;
    boolean refreshValue = false;
    public Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mySharedPreference = new MySharedPreference(getContext());


        handler = new Handler();
//
//        final Runnable r = new Runnable() {
//            public void run() {
//                GetPositionData();
//                handler.postDelayed(this, 5000);
//            }
//        };
//
//        handler.postDelayed(r, 5000);

//        Thread thread = new Thread() {
//            @Override
//            public void run() {
//                try {
//                    while(true) {
//                        sleep(1000);
//                        GetPositionData();
//                        handler.post(this);
//                    }
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        };
//
//        thread.start();

//        Timer myTimer = new Timer();
//        myTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                // If you want to modify a view in your Activity
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        GetRecyclingPositionData();
//                    }
//                });
//
//            }
//        }, 8000, 8000);

    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.trade_position_fragment, container, false);

        initUI(root);
        setSpinnerData();
        getAccountType();
        GetPositionData();
        return root;
    }

    // initialization of UI...
    private void initUI(View root) {
        rec_position = root.findViewById(R.id.rec_position);
        txt_pl_day = root.findViewById(R.id.txt_pl_day);
        txt_pl_open = root.findViewById(R.id.txt_pl_open);
        txt_net_liquidity = root.findViewById(R.id.txt_net_liquidity);
        txt_available_funds = root.findViewById(R.id.txt_available_funds);
        spinner_acc_type = root.findViewById(R.id.spinner_acc_type);

        if (((MyGlobalClass) getContext().getApplicationContext()).getRithmic_id().equalsIgnoreCase("1")) {
            GetPersonalInfoData();
        } else {

        }
        clickPositionItem();
    }

    // Get all position data according to user id...
    private void GetPositionData() {

        String Url = Constant.POSITION_PAGE + "userid" + "=" + mySharedPreference.getisCustomerId() + "&" + "AccountType" + "=" + account_type;
        System.out.println("GetPositionData url : " + Url);
        trade_position_arrays.clear();
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("userid", mySharedPreference.getisCustomerId());
        postParams.put("AccountType", account_type);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons1, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons1 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                refreshValue = true;
                refreshAllContent(5000);
                JSONObject new_data = data.getJSONObject("data");
                JSONArray service = new_data.getJSONArray("PnlInfoList");

                String pl_day = new_data.getString("PLDay");
                String pl_open = new_data.getString("PLOpen");
                String pl_net_liquidity = new_data.getString("NetLiquidity");
                String pl_available_funds = new_data.getString("AvailableFunds");

                if (pl_day.startsWith("-")) {
                    pl_day = pl_day.replace("-", "");
                    Double d_pl_day = Double.valueOf(pl_day);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_day.setTextColor(ContextCompat.getColor(getContext(), R.color.color_order_red));
                    txt_pl_day.setText("$" + "(" + decimalFormat.format(d_pl_day) + ")");
                } else {

                    Double d_pl_day = Double.valueOf(pl_day);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_day.setTextColor(ContextCompat.getColor(getContext(), R.color.app_color));
                    txt_pl_day.setText("$" + decimalFormat.format(d_pl_day));
                }

                if (pl_open.startsWith("-")) {
                    pl_open = pl_open.replace("-", "");
                    Double d_pl_open = Double.valueOf(pl_open);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_open.setTextColor(ContextCompat.getColor(getContext(), R.color.color_order_red));
                    txt_pl_open.setText("$" + "(" + decimalFormat.format(d_pl_open) + ")");

                } else {
                    Double d_pl_open = Double.valueOf(pl_open);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_open.setTextColor(ContextCompat.getColor(getContext(), R.color.app_color));
                    txt_pl_open.setText("$" + decimalFormat.format(d_pl_open));
                }

                if (pl_net_liquidity.startsWith("-")) {
                    pl_net_liquidity = pl_net_liquidity.replace("-", "");
                    Double d_pl_equidity = Double.valueOf(pl_net_liquidity);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_net_liquidity.setTextColor(ContextCompat.getColor(getContext(), R.color.position_below_text));
                    txt_net_liquidity.setText("$" + "(" + decimalFormat.format(d_pl_equidity) + ")");
                } else {
                    Double d_pl_equidity = Double.valueOf(pl_net_liquidity);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_net_liquidity.setTextColor(ContextCompat.getColor(getContext(), R.color.position_below_text));
                    txt_net_liquidity.setText("$" + decimalFormat.format(d_pl_equidity));
                }

                if (pl_available_funds.startsWith("-")) {
                    pl_available_funds = pl_available_funds.replace("-", "");
                    Double d_pl_available_funds = Double.valueOf(pl_available_funds);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_available_funds.setTextColor(ContextCompat.getColor(getContext(), R.color.position_below_text));
                    txt_available_funds.setText("$" + "(" + decimalFormat.format(d_pl_available_funds) + ")");
                } else {
                    Double d_pl_available_funds = Double.valueOf(pl_available_funds);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_available_funds.setTextColor(ContextCompat.getColor(getContext(), R.color.position_below_text));
                    txt_available_funds.setText("$" + decimalFormat.format(d_pl_available_funds));
                }


                for (int i = 0; i < service.length(); i++) {
                    JSONObject history = service.getJSONObject(i);
                    Trade_Position_Array getService = new Trade_Position_Array();

                    getService.setTicker(history.getString("Ticker"));
                    getService.setContractMonth(history.getString("ContractMonth"));
                    getService.setMarketPrice(history.getString("MarketPrice"));
                    getService.setPLOpen(history.getString("PLOpen"));
                    getService.setPLToday(history.getString("PLToday"));
                    getService.setBuyPrice(history.getString("BuyPrice"));
                    getService.setPLPercent(history.getString("PLPercent"));
                    getService.setQuantity(history.getString("Quantity"));
                    getService.setBQty(history.getString("BQty"));
                    getService.setSQty(history.getString("SQty"));
                    getService.setTotalPLToday(history.getString("TotalPLToday"));
                    getService.setTotalPLPercent(history.getString("TotalPLPercent"));
                    getService.setTotalPLOpen(history.getString("TotalPLOpen"));

                    trade_position_arrays.add(getService);
                }
                trade_position_adapter = new Trade_Position_Adapter(trade_position_arrays, getContext());
                LinearLayoutManagerWrapper layoutManager = new LinearLayoutManagerWrapper(getContext(), LinearLayoutManager.VERTICAL, false);
                rec_position.setLayoutManager(layoutManager);
                rec_position.setAdapter(trade_position_adapter);
                trade_position_adapter.notifyDataSetChanged();

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    //method for refreshing position data...
    private void GetRecyclingPositionData() {

        String Url = Constant.POSITION_PAGE + "userid" + "=" + mySharedPreference.getisCustomerId() + "&" + "AccountType" + "=" + account_type;
        System.out.println("GetRecyclingPositionData url : " + Url);
        trade_position_arrays.clear();
        System.out.println("data size : " + trade_position_arrays.size());
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("userid", mySharedPreference.getisCustomerId());
        postParams.put("AccountType", account_type);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons2, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons2 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                //Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();

                if (refreshValue) {
                    refreshAllContent(5000);
                } else {
                    refreshValue = false;
                }

                JSONObject new_data = data.getJSONObject("data");
                JSONArray service = new_data.getJSONArray("PnlInfoList");

                String pl_day = new_data.getString("PLDay");
                String pl_open = new_data.getString("PLOpen");
                String pl_net_liquidity = new_data.getString("NetLiquidity");
                String pl_available_funds = new_data.getString("AvailableFunds");

                if (pl_day.startsWith("-")) {
                    pl_day = pl_day.replace("-", "");
                    Double d_pl_day = Double.valueOf(pl_day);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_day.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                    txt_pl_day.setText("$" + "(" + decimalFormat.format(d_pl_day) + ")");
                } else {

                    Double d_pl_day = Double.valueOf(pl_day);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_day.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                    txt_pl_day.setText("$" + decimalFormat.format(d_pl_day));
                }

                if (pl_open.startsWith("-")) {
                    pl_open = pl_open.replace("-", "");
                    Double d_pl_open = Double.valueOf(pl_open);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                    txt_pl_open.setText("$" + "(" + decimalFormat.format(d_pl_open) + ")");

                } else {
                    Double d_pl_open = Double.valueOf(pl_open);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                    txt_pl_open.setText("$" + decimalFormat.format(d_pl_open));
                }

                if (pl_net_liquidity.startsWith("-")) {
                    pl_net_liquidity = pl_net_liquidity.replace("-", "");
                    Double d_pl_equidity = Double.valueOf(pl_net_liquidity);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_net_liquidity.setTextColor(ContextCompat.getColor(context, R.color.position_below_text));
                    txt_net_liquidity.setText("$" + "(" + decimalFormat.format(d_pl_equidity) + ")");
                } else {
                    Double d_pl_equidity = Double.valueOf(pl_net_liquidity);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_net_liquidity.setTextColor(ContextCompat.getColor(context, R.color.position_below_text));
                    txt_net_liquidity.setText("$" + decimalFormat.format(d_pl_equidity));
                }

                if (pl_available_funds.startsWith("-")) {
                    pl_available_funds = pl_available_funds.replace("-", "");
                    Double d_pl_available_funds = Double.valueOf(pl_available_funds);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_available_funds.setTextColor(ContextCompat.getColor(context, R.color.position_below_text));
                    txt_available_funds.setText("$" + "(" + decimalFormat.format(d_pl_available_funds) + ")");
                } else {
                    Double d_pl_available_funds = Double.valueOf(pl_available_funds);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_available_funds.setTextColor(ContextCompat.getColor(context, R.color.position_below_text));
                    txt_available_funds.setText("$" + decimalFormat.format(d_pl_available_funds));
                }


                for (int i = 0; i < service.length(); i++) {
                    JSONObject history = service.getJSONObject(i);
                    Trade_Position_Array getService = new Trade_Position_Array();

                    getService.setTicker(history.getString("Ticker"));
                    getService.setContractMonth(history.getString("ContractMonth"));
                    getService.setMarketPrice(history.getString("MarketPrice"));
                    getService.setPLOpen(history.getString("PLOpen"));
                    getService.setPLToday(history.getString("PLToday"));
                    getService.setBuyPrice(history.getString("BuyPrice"));
                    getService.setPLPercent(history.getString("PLPercent"));
                    getService.setQuantity(history.getString("Quantity"));
                    getService.setBQty(history.getString("BQty"));
                    getService.setSQty(history.getString("SQty"));
                    getService.setTotalPLToday(history.getString("TotalPLToday"));
                    getService.setTotalPLPercent(history.getString("TotalPLPercent"));
                    getService.setTotalPLOpen(history.getString("TotalPLOpen"));

                    trade_position_arrays.add(getService);
                }
                System.out.println("data size fill: " + trade_position_arrays.size());
                trade_position_adapter = new Trade_Position_Adapter(trade_position_arrays, context);
                LinearLayoutManagerWrapper layoutManager = new LinearLayoutManagerWrapper(context, LinearLayoutManager.VERTICAL, false);
                rec_position.setLayoutManager(layoutManager);
                rec_position.setAdapter(trade_position_adapter);
                trade_position_adapter.notifyDataSetChanged();


            } else {
                Toast.makeText(context, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
        }
    };

    // method to get account type live or paper...
    public void getAccountType() {

        spinner_acc_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedCar = position;
                account_type = parent.getItemAtPosition(position).toString();
                //((TextView) parent.getChildAt(0)).setTextColor((getResources().getColor(R.color.app_color)));
//                GetPositionData();
                System.out.println("account_type :" + account_type);
                ((MyGlobalClass) getContext().getApplicationContext()).setAccountType(account_type);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    //method to set spinner data of live and paper...
    public void setSpinnerData() {
//        List<String> spinnerArray = new ArrayList<>();
//        spinnerArray.add("Live");
//        spinnerArray.add("Paper");
//
//        adapter = new ArrayAdapter<String>(
//                getContext(),
//                R.layout.trade_position_text,
//                spinnerArray
//        );
//        adapter.setDropDownViewResource(R.layout.trade_position_text);
//        spinner_acc_type.setAdapter(adapter);
//        spinner_acc_type.setSelection(0);

        Trade_Position_Dropdown_Adapter trade_position_dropdown_adapter = new Trade_Position_Dropdown_Adapter(getContext(), acc_type, selectedCar);
        spinner_acc_type.setAdapter(trade_position_dropdown_adapter);
        spinner_acc_type.setSelection(0);

    }

    // method of clicking on position items...
    public void clickPositionItem() {
        rec_position.addOnItemTouchListener(new
                RecyclerItemClickListener(getContext(), rec_position, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (trade_position_arrays.size() != 0) {
                    if (!trade_position_arrays.get(position).getTicker().equalsIgnoreCase("Total")) {
                        refreshValue = false;
                        ((MyGlobalClass) getContext().getApplicationContext()).setTicker_name(trade_position_arrays.get(position).getTicker());
                        ((MyGlobalClass) getContext().getApplicationContext()).setTicker_quantity(trade_position_arrays.get(position).getBQty());
                        ((MyGlobalClass) getContext().getApplicationContext()).setClick_position("1");
                        ((BottomNavigationView) getActivity().findViewById(R.id.navigation)).setSelectedItemId(R.id.navigation_trade);
                    } else {

                    }
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {
            }
        }));
    }

    // method to get personal info of user for rithmic id...
    private void GetPersonalInfoData() {
        String Url = Constant.GET_USER_PERSONAL_INFO;
        System.out.println("GetPersonalInfoData url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("userId", mySharedPreference.getisCustomerId());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons3, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons3 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(context, data.getString("message"), Toast.LENGTH_SHORT).show();
                JSONObject info = data.getJSONObject("data");
                String Rithimic_id = info.getString("Rithimic_id");
                if (!Rithimic_id.equalsIgnoreCase("")) {
                    ((MyGlobalClass) getContext().getApplicationContext()).setRithmic_id("1");
                } else {
                    ((MyGlobalClass) getContext().getApplicationContext()).setRithmic_id("0");
                }

            } else {
                Toast.makeText(context, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
        }
    };

    public void refreshAllContent(final long timetoupdate) {
        new CountDownTimer(timetoupdate, 3000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (refreshValue) {
                    System.out.println("UPDATE CONTENT HERE");
                    GetRecyclingPositionData();
                }

            }
        }.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        refreshValue = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        refreshValue = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        refreshValue = false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        refreshValue = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        refreshValue = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        refreshValue = false;
    }
}
