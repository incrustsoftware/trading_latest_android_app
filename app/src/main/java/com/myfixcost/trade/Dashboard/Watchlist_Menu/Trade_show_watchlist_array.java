package com.myfixcost.trade.Dashboard.Watchlist_Menu;

public class Trade_show_watchlist_array {

    String Id, Ticker, ContractMonth, PriceChange, TodayChange, Watch_list_id;

    public Trade_show_watchlist_array() {

    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getTicker() {
        return Ticker;
    }

    public void setTicker(String ticker) {
        Ticker = ticker;
    }

    public String getContractMonth() {
        return ContractMonth;
    }

    public void setContractMonth(String contractMonth) {
        ContractMonth = contractMonth;
    }

    public String getPriceChange() {
        return PriceChange;
    }

    public void setPriceChange(String priceChange) {
        PriceChange = priceChange;
    }

    public String getTodayChange() {
        return TodayChange;
    }

    public void setTodayChange(String todayChange) {
        TodayChange = todayChange;
    }

    public String getWatch_list_id() {
        return Watch_list_id;
    }

    public void setWatch_list_id(String watch_list_id) {
        Watch_list_id = watch_list_id;
    }

    public Trade_show_watchlist_array(String id, String ticker, String contractMonth, String priceChange, String todayChange, String watch_list_id) {
        Id = id;
        Ticker = ticker;
        ContractMonth = contractMonth;
        PriceChange = priceChange;
        TodayChange = todayChange;
        Watch_list_id = watch_list_id;
    }
}
