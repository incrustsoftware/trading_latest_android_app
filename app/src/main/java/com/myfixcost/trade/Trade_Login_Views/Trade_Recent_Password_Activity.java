package com.myfixcost.trade.Trade_Login_Views;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;

import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.R;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Pattern;

public class Trade_Recent_Password_Activity extends AppCompatActivity {

    LinearLayout lin_choose_way;
    RelativeLayout rel_update_password, rel_via_sms, rel_via_email, rel_full_email_id;
    EditText ed_email_id, ed_new_password, ed_verify_password, ed_enter_otp;
    Button btn_send_email;
    String email_data = "", new_password_data = "", verify_password_data = "", enter_otp_data = "";
    ImageView img_back;
    TextView txt_first_select, txt_second_select, txt_phone, txt_email, txt_Resend_Otp;
    String get_phone_no, get_email_id,get_Country_code,UserId;
    String post_phone_no = "", post_email,Country_code;
    Button btn_update_password;
    AppCompatButton btn_verify_otp;
    boolean newanConfirmPass = false;
    private boolean is8char = false, hasUpper = false, hasnum = false, hasSpecialSymbol = false;
    boolean otp_confirmed;

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,100}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trade_recent_password_activity);

        initUI();

        rel_via_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post_phone_no = get_phone_no;
                Country_code = get_Country_code;
                post_email = "";
                sendOTP();
            }
        });

        rel_via_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post_phone_no = "";
                Country_code = "";
                post_email = get_email_id;
                sendOTP();
            }
        });

        btn_send_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (email_data.equalsIgnoreCase("")) {
                    ed_email_id.setError("Enter Email Id");
                } else if (!matchEmail()) {
                    ed_email_id.setError("Invalid Email Id");
                } else {
                    // call api
                    GetUserDetails();
                }
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_update_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateAllChecks()) {

                } else {
                    Post_Reset_Password_Data();
                }
            }
        });

        btn_verify_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!enter_otp_data.equalsIgnoreCase("")) {
                    verifyOTP();
                } else {
                    ed_enter_otp.setError("Enter OTP");
                }
            }
        });

        txt_Resend_Otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (post_phone_no.equalsIgnoreCase("")) {
                    Toast.makeText(Trade_Recent_Password_Activity.this, "Phone no invalid", Toast.LENGTH_SHORT).show();
                } else {
                    sendOTP();
                }
            }
        });
    }

    // initialize UI..
    private void initUI() {
        lin_choose_way = findViewById(R.id.lin_choose_way);
        rel_update_password = findViewById(R.id.rel_update_password);
        rel_via_sms = findViewById(R.id.rel_via_sms);
        rel_via_email = findViewById(R.id.rel_via_email);
        ed_email_id = findViewById(R.id.ed_email_id);
        btn_send_email = findViewById(R.id.btn_send_email);
        img_back = findViewById(R.id.img_back);
        txt_second_select = findViewById(R.id.txt_second_select);
        txt_first_select = findViewById(R.id.txt_first_select);
        rel_full_email_id = findViewById(R.id.rel_full_email_id);
        txt_phone = findViewById(R.id.txt_phone);
        txt_email = findViewById(R.id.txt_email);
        ed_new_password = findViewById(R.id.ed_new_password);
        ed_verify_password = findViewById(R.id.ed_verify_password);
        btn_update_password = findViewById(R.id.btn_update_password);
        ed_enter_otp = findViewById(R.id.ed_enter_otp);
        btn_verify_otp = findViewById(R.id.btn_verify_otp);
        txt_Resend_Otp = findViewById(R.id.txt_Resend_Otp);

        editTextEmail(ed_email_id);
        editTextNewPassword(ed_new_password);
        editTextVerifyPassword(ed_verify_password);
        editTextenterOTP(ed_enter_otp);
    }

    // get email method...
    public void editTextEmail(EditText edEmail) {
        edEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    email_data = "";
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    email_data = s.toString();
                }
            }
        });
    }

    // get new password method...
    public void editTextNewPassword(EditText edEmail) {
        edEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    new_password_data = "";
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    new_password_data = s.toString();
                    passwordValidate(edEmail);
                }
            }
        });
    }

    // get verify password method...
    public void editTextVerifyPassword(EditText edEmail) {
        edEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    verify_password_data = "";
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    verify_password_data = s.toString();
                    passwordValidate(edEmail);
                }
            }
        });
    }

    // get enter otp method...
    public void editTextenterOTP(EditText edEmail) {
        edEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    enter_otp_data = "";
                    txt_Resend_Otp.setVisibility(View.GONE);
                    btn_verify_otp.setEnabled(false);
                    btn_verify_otp.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_send_otp));
                } else {
                    if (s.toString().length() == 6) {
                        btn_verify_otp.setEnabled(true);
                        txt_Resend_Otp.setVisibility(View.VISIBLE);
                        btn_verify_otp.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_email_login));
                        enter_otp_data = s.toString();
                    } else {
                        btn_verify_otp.setEnabled(false);
                        txt_Resend_Otp.setVisibility(View.GONE);
                        btn_verify_otp.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_send_otp));
                    }
                }
            }
        });
    }

    // validate email method...
    public boolean matchEmail() {
        boolean is_email = false;

        if (!EMAIL_ADDRESS_PATTERN.matcher(email_data.toString().trim()).matches()) {
            //Toast.makeText(this, "Invalid Email Id", Toast.LENGTH_SHORT).show();
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            is_email = false;
        } else {
            is_email = true;
        }
        return is_email;
    }

    // method is to check password validations...
    private void passwordValidate(EditText ed_new_password) {
        String password = ed_new_password.getText().toString();
        // 8 character
        if (ed_new_password.length() >= 8) {
            is8char = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            is8char = false;
        }
        //number
        if (password.matches("(.*[0-9].*)")) {
            hasnum = true;
        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasnum = false;

        }
        //upper case
        if (password.matches("(.*[A-Z].*)")) {
            hasUpper = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasUpper = false;

        }
        //symbol
        if (password.matches("^(?=.*[_.()$&@!]).*$")) {
            hasSpecialSymbol = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasSpecialSymbol = false;

        }
    }

    // method to check password and confirm password...
    public boolean checkNewAnConfirmPass() {
        if (new_password_data.equalsIgnoreCase(verify_password_data)) {
            newanConfirmPass = true;
        } else {
            newanConfirmPass = false;
            ed_verify_password.setError("Password and Confirm password should be same");
        }
        return newanConfirmPass;
    }

    private void GetUserDetails() {

        String Url = Constant.GET_USER_BY_EMAIL;
        System.out.println("GetUserDetails url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("email", email_data);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getApplicationContext(), getMasterResons1, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons1 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());
            RequestQueueService.cancelProgressDialog();
            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(Trade_Recent_Password_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                JSONObject info = data.getJSONObject("data");

                get_phone_no = info.getString("Contact");
                get_email_id = info.getString("Email");
                get_Country_code = info.getString("Country_code");
                UserId = info.getString("UserId");

                txt_first_select.setText("Select Method");
                txt_second_select.setText("Select which contact details should we use to reset your password?");
                rel_via_sms.setVisibility(View.VISIBLE);
                rel_via_email.setVisibility(View.VISIBLE);
                rel_full_email_id.setVisibility(View.GONE);

                String firstTwoChars = "";     //substring containing first 4 characters
                if (get_phone_no.length() > 2) {
                    firstTwoChars = get_phone_no.substring(0, 2);
                } else {
                    firstTwoChars = get_phone_no;
                }
                String ex_mob_no = get_phone_no.substring(2);
                String secure_mob_no = ex_mob_no.replaceAll("\\w(?=\\w{2})", "*");
//                txt_phone.setText("+1" + " " + firstTwoChars + "" + secure_mob_no);
                txt_phone.setText("+"+get_Country_code+" "+firstTwoChars + "" + secure_mob_no);

                String[] coming_email = get_email_id.split("@");
                String before_at = coming_email[0];
                String after_at = coming_email[1];
                String maskedEmail = before_at.replaceAll("(?<!^.?).(?!.?$)", "*");

                txt_email.setText(maskedEmail + "@" + after_at);

            } else {
                Toast.makeText(Trade_Recent_Password_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_Recent_Password_Activity.this);
        }
    };

    // method to send otp on mobile no...
    private void sendOTP() {
        String Url = Constant.SEND_OTP;
        System.out.println("sendOTP url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("id", UserId);
        postParams.put("contact", post_phone_no);
        postParams.put("email", post_email);
        postParams.put("flag", "Update");
        postParams.put("country_code", Country_code);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getApplicationContext(), getMasterResons, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_Recent_Password_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                rel_update_password.setVisibility(View.VISIBLE);
                lin_choose_way.setVisibility(View.GONE);

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_Recent_Password_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_Recent_Password_Activity.this);
        }
    };

    public boolean validateAllChecks() {
        boolean is_error = false;
        if (new_password_data.equalsIgnoreCase("")) {
            ed_new_password.setError("Invalid Password");
            is_error = false;
        } else if (verify_password_data.equalsIgnoreCase("")) {
            ed_verify_password.setError("Enter Confirm Password");
            is_error = false;
        } else if (!is8char || !hasnum || !hasUpper || !hasSpecialSymbol) {
            ed_new_password.setError("Invalid Password");
            is_error = false;
        } else if (!checkNewAnConfirmPass()) {
            is_error = false;
        } else if (!otp_confirmed) {
            ed_enter_otp.setError("OTP not confirmed");
            is_error = false;
        } else {
            is_error = true;
        }
        return is_error;
    }

    // method to post reset password data...
    private void Post_Reset_Password_Data() {
        String Url = Constant.POST_RESET_PASSWORD_DATA;
        System.out.println("Post_Reset_Password_Data url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("email", get_email_id);
        postParams.put("contact", get_phone_no);
        postParams.put("Password", new_password_data);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getApplicationContext(), postResetPasswordData, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner postResetPasswordData = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_Recent_Password_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(Trade_Recent_Password_Activity.this, Trade_Login_Activity.class));
            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_Recent_Password_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_Recent_Password_Activity.this);
        }
    };

    // method to verify OTP...
    private void verifyOTP() {
        String Url = Constant.VERIFY_OTP;
        System.out.println("verifyOTP url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("contact", get_phone_no);
        postParams.put("Otp", enter_otp_data);
        postParams.put("country_code", Country_code);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getApplicationContext(), verifyotp, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner verifyotp = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                otp_confirmed = true;
                Toast.makeText(Trade_Recent_Password_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();

            } else {
                RequestQueueService.cancelProgressDialog();
                otp_confirmed = false;
                Toast.makeText(Trade_Recent_Password_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
            otp_confirmed = true;
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_Recent_Password_Activity.this);
        }
    };
}
