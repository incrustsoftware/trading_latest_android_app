package com.myfixcost.trade.Dashboard.Orders_Menu.Pending_Orders;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.myfixcost.trade.Dashboard.Orders_Menu.Trade_Edit_Order_Activity;
import com.myfixcost.trade.R;

import java.util.ArrayList;

public class Trade_Pending_Orders_Adapter extends RecyclerView.Adapter<Trade_Pending_Orders_Adapter.ViewHolder> {
    public ArrayList<Trade_Pending_Orders_Array> trade_pending_orders_arrays = new ArrayList<>();
    Context context;
    int lastSelectedPosition = -1;
    boolean state = true;
    Trade_Edit_Order_Interface trade_edit_order_interface;

    // RecyclerView recyclerView;
    public Trade_Pending_Orders_Adapter(ArrayList<Trade_Pending_Orders_Array> trade_pending_orders_arrays, Trade_Edit_Order_Interface trade_edit_order_interface, Context context) {
        this.trade_pending_orders_arrays = trade_pending_orders_arrays;
        this.trade_edit_order_interface = trade_edit_order_interface;
        this.context = context;

    }

    @Override
    public Trade_Pending_Orders_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.trade_pending_order_adapter, parent, false);
        Trade_Pending_Orders_Adapter.ViewHolder viewHolder = new Trade_Pending_Orders_Adapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Trade_Pending_Orders_Adapter.ViewHolder holder, int position) {
        final Trade_Pending_Orders_Array myListData = trade_pending_orders_arrays.get(position);
        holder.txt_ticker_name.setText(myListData.getTicker());
        holder.txt_order_type.setText(myListData.getOrderType());
        holder.txt_contract_month.setText(myListData.getContractMonth());
        holder.txt_price.setText("$"+myListData.getPrice());
        holder.txt_quantity.setText(myListData.getQuantity());
        holder.txt_current_price.setText("$"+myListData.getCurrentPrice());
        holder.txt_valid_upto.setText(myListData.getValidUpTo());

        holder.img_delete_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trade_edit_order_interface.editorderdata(myListData.getOrderId());
            }
        });

        holder.img_edit_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent edit_order = new Intent(context, Trade_Edit_Order_Activity.class);
                edit_order.putExtra("order_id", myListData.getOrderId());
                context.startActivity(edit_order);
            }
        });


//        if (lastSelectedPosition != -1 && lastSelectedPosition == position) {
//            holder.img_tick.setVisibility(View.VISIBLE);
//            holder.txt_doc.setTextColor(ContextCompat.getColor(context, R.color.app_background_color));
//            final Typeface typeface = ResourcesCompat.getFont(context, R.font.opensans_semibold);
//            holder.txt_doc.setTypeface(typeface);
//
//            state = false;
//        }else{
//            holder.img_tick.setVisibility(View.GONE);
//            holder.txt_doc.setTextColor(ContextCompat.getColor(context, R.color.bottomsheet_doc_color));
//            final Typeface typeface = ResourcesCompat.getFont(context, R.font.opensans_regular);
//            holder.txt_doc.setTypeface(typeface);
//            state = true;
//
//        }


    }

    @Override
    public int getItemCount() {
        return trade_pending_orders_arrays.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_ticker_name, txt_order_type, txt_contract_month, txt_price, txt_quantity, txt_current_price, txt_valid_upto;
        ImageView img_edit_order, img_delete_order;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txt_ticker_name = itemView.findViewById(R.id.txt_ticker_name);
            this.txt_order_type = itemView.findViewById(R.id.txt_order_type);
            this.txt_contract_month = itemView.findViewById(R.id.txt_contract_month);
            this.txt_price = itemView.findViewById(R.id.txt_price);
            this.txt_quantity = itemView.findViewById(R.id.txt_quantity);
            this.txt_current_price = itemView.findViewById(R.id.txt_current_price);
            this.txt_valid_upto = itemView.findViewById(R.id.txt_valid_upto);
            this.img_edit_order = itemView.findViewById(R.id.img_edit_order);
            this.img_delete_order = itemView.findViewById(R.id.img_delete_order);
        }
    }
}
