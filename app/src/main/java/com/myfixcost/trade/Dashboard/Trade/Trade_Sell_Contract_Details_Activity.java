package com.myfixcost.trade.Dashboard.Trade;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.myfixcost.trade.Dashboard.Trade_Dashboard_Activity;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;

import java.text.DecimalFormat;

public class Trade_Sell_Contract_Details_Activity extends AppCompatActivity {

    TextView txt_ticker_name, txt_order_type, txt_contract_month, txt_quantity, txt_price, txt_credit_recieved, txt_available_funds, txt_contract_details, txt_credit_and_amount;
    String Ticker, Type, ContractMonth, quantity, MarketPrice, CreditReceived, AvailableFunds;
    Button btn_go_to_position;
    DecimalFormat decimalFormat;
    MySharedPreference mySharedPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trade_sell_contract_details_activity);

        Type = getIntent().getStringExtra("Type");
        Ticker = getIntent().getStringExtra("Ticker");
        ContractMonth = getIntent().getStringExtra("ContractMonth");
        quantity = getIntent().getStringExtra("quantity");
        MarketPrice = getIntent().getStringExtra("MarketPrice");
        CreditReceived = getIntent().getStringExtra("CreditReceived");
        AvailableFunds = getIntent().getStringExtra("AvailableFunds");

        mySharedPreference = new MySharedPreference(this);
        initUI();

        btn_go_to_position.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Trade_Sell_Contract_Details_Activity.this, Trade_Dashboard_Activity.class));
                finish();
            }
        });
    }

    private void initUI() {
        txt_ticker_name = findViewById(R.id.txt_ticker_name);
        txt_order_type = findViewById(R.id.txt_order_type);
        txt_contract_month = findViewById(R.id.txt_contract_month);
        txt_quantity = findViewById(R.id.txt_quantity);
        txt_price = findViewById(R.id.txt_price);
        txt_credit_recieved = findViewById(R.id.txt_credit_recieved);
        txt_available_funds = findViewById(R.id.txt_available_funds);
        txt_contract_details = findViewById(R.id.txt_contract_details);
        txt_credit_and_amount = findViewById(R.id.txt_credit_and_amount);
        btn_go_to_position = findViewById(R.id.btn_go_to_position);

        setText();

    }

    public void setText() {
        decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");

        if (Type.equalsIgnoreCase("buy")) {
            txt_credit_and_amount.setText("Amount");

            Double Market_Price = Double.valueOf(MarketPrice);
            txt_credit_recieved.setText("$" + "(" + decimalFormat.format(Market_Price) + ")");

            txt_credit_recieved.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_order_red));
            txt_contract_details.setText("Buy Contract Details");
        } else {
            txt_credit_and_amount.setText("Credit Recieved");

//            Double Credit_Received = Double.valueOf(CreditReceived);
//            txt_credit_recieved.setText("$"+decimalFormat.format(Credit_Received));
            txt_credit_recieved.setText("$" + CreditReceived);
            txt_credit_recieved.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_text_color));
            txt_contract_details.setText("Sell Contract Details");
        }

        txt_ticker_name.setText(Ticker);
        txt_order_type.setText(Type);
        txt_contract_month.setText(ContractMonth);
        txt_quantity.setText(quantity);
        txt_price.setText("$" + MarketPrice);

        Double Available_Funds = Double.valueOf(AvailableFunds);
        txt_available_funds.setText("$" + decimalFormat.format(Available_Funds));
    }
}
