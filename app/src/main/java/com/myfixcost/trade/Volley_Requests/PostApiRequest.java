package com.myfixcost.trade.Volley_Requests;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class PostApiRequest {

    String Auth_key;
    Context mContext;

    public void post_request_Object(final Context context, final FetchDataListner listener, JSONObject params, final String ApiURL) throws JSONException {
        if (listener != null) {
            listener.onFetchStart();
        }
        this.mContext = context;

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ApiURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (listener != null) {
                                listener.onFetchComplete(response);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("coming error is :" + error.toString());
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    if (listener != null) listener.onFetchFailure(message);
                }
                //Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                System.out.println("Error :" + message);
//                if (error instanceof NoConnectionError) {
//                    Toast.makeText(mContext,"Network Connectivity Problem",Toast.LENGTH_SHORT).show();
//                    listener.onFetchFailure("Network Connectivity Problem");
//                } else if (error.networkResponse != null && error.networkResponse.data != null) {
//                    VolleyError volley_error = new VolleyError(new String(error.networkResponse.data));
//                    String errorMessage = "";
//                    try {
//                        JSONObject errorJson = new JSONObject(volley_error.getMessage().toString());
//                        if (errorJson.has("error")) errorMessage = errorJson.getString("error");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    if (errorMessage.isEmpty()) {
//                        errorMessage = volley_error.getMessage();
//                    }
//
//                    if (listener != null) listener.onFetchFailure(errorMessage);
//                } else {
//                    Toast.makeText(mContext,"Something went wrong. Please try again later",Toast.LENGTH_SHORT).show();
//                    listener.onFetchFailure("Something went wrong. Please try again later");
//                }

            }
        });
//        postRequest.setRetryPolicy(new DefaultRetryPolicy(
//                MY_SOCKET_TIMEOUT_MS,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueueService.getInstance(context).addToRequestQueue(postRequest.setShouldCache(false));
    }

    public void post_request_ObjectWithHeader(final Context context, final FetchDataListner listener, JSONObject params, final String ApiURL, String auth_key) throws JSONException {
        if (listener != null) {
            listener.onFetchStart();
        }
        //base server URL
        this.mContext = context;
        Auth_key = auth_key;

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.POST, ApiURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (listener != null) {
                                listener.onFetchComplete(response);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NoConnectionError) {


                } else if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volley_error = new VolleyError(new String(error.networkResponse.data));
                    String errorMessage = "";
                    try {
                        JSONObject errorJson = new JSONObject(volley_error.getMessage().toString());
                        if (errorJson.has("error")) errorMessage = errorJson.getString("error");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (errorMessage.isEmpty()) {
                        errorMessage = volley_error.getMessage();
                    }

                    if (listener != null) listener.onFetchFailure(errorMessage);
                } else {

                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", Auth_key);

                return params;
            }
        };
        RequestQueueService.getInstance(context).addToRequestQueue(postRequest.setShouldCache(false));
    }

    public void get_request_Object(final Context context, final FetchDataListner listener, final String ApiURL) throws JSONException {
        if (listener != null) {
            //call onFetchComplete of the listener
            //to show progress dialog
            //-indicates calling started
            listener.onFetchStart();
        }
        this.mContext = context;

        //add extension api url received from caller
        //and make full api
        String url = ApiURL;
        //JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, url, null,
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (listener != null) {

                                //received response
                                //call onFetchComplete of the listener
                                System.out.println("*@ Language Translation Response :" + response.toString());
                                listener.onFetchComplete(response);
                            } else {
                                //has error in response
                                //call onFetchFailure of the listener
                                listener.onFetchFailure("Failed");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("#@ Erron name : " + error);
                if (error instanceof NoConnectionError) {
                    listener.onFetchFailure("Network Connectivity Problem");
                } else if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volley_error = new VolleyError(new String(error.networkResponse.data));
                    String errorMessage = "";
                    try {
                        JSONObject errorJson = new JSONObject(volley_error.getMessage().toString());
                        if (errorJson.has("error")) errorMessage = errorJson.getString("error");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (errorMessage.isEmpty()) {
                        errorMessage = volley_error.getMessage();
                    }

                    if (listener != null) listener.onFetchFailure(errorMessage);
                } else {
                    Toast.makeText(mContext, "Something went wrong. Please try again later", Toast.LENGTH_SHORT).show();
                    listener.onFetchFailure("Something went wrong. Please try again later");
                }

            }
        });

        RequestQueueService.getInstance(context).addToRequestQueue(getRequest.setShouldCache(false));
    }

    public void get_request_ObjectWithHeader(final Context context, final FetchDataListner listener, final String ApiURL, final String Authkey) throws JSONException {
        if (listener != null) {
            //call onFetchComplete of the listener
            //to show progress dialog
            //-indicates calling started
            listener.onFetchStart();
        }
        this.mContext = context;

        //add extension api url received from caller
        //and make full api
        String url = ApiURL;
        //JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, url, null,
        final JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (listener != null) {

                                //received response
                                //call onFetchComplete of the listener
                                System.out.println("*@ Language Translation Response :" + response.toString());
                                listener.onFetchComplete(response);
                            } else {
                                //has error in response
                                //call onFetchFailure of the listener
                                listener.onFetchFailure("Failed");
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("#@ Erron name : " + error);
                if (error instanceof NoConnectionError) {

                    listener.onFetchFailure(error.toString());
                } else if (error.networkResponse != null && error.networkResponse.data != null) {
                    VolleyError volley_error = new VolleyError(new String(error.networkResponse.data));
                    String errorMessage = "";
                    try {
                        JSONObject errorJson = new JSONObject(volley_error.getMessage().toString());
                        if (errorJson.has("error")) errorMessage = errorJson.getString("error");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if (errorMessage.isEmpty()) {
                        errorMessage = volley_error.getMessage();
                    }

                    if (listener != null) listener.onFetchFailure(errorMessage);
                } else {
                    Toast.makeText(mContext, error.toString(), Toast.LENGTH_SHORT).show();
                    listener.onFetchFailure(error.toString());
                }

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", Authkey);

                return params;
            }
        };

        RequestQueueService.getInstance(context).addToRequestQueue(getRequest.setShouldCache(false));
    }

    public void put_request_Object(final Context context, final FetchDataListner listener, JSONObject params, final String ApiURL) throws JSONException {
        if (listener != null) {
            listener.onFetchStart();
        }
        this.mContext = context;

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.PUT, ApiURL, params,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (listener != null) {
                                listener.onFetchComplete(response);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("coming error is :" + error.toString());
                String message = null;
                if (error instanceof NetworkError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof ServerError) {
                    message = "The server could not be found. Please try again after some time!!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof AuthFailureError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof ParseError) {
                    message = "Parsing error! Please try again after some time!!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof NoConnectionError) {
                    message = "Cannot connect to Internet...Please check your connection!";
                    if (listener != null) listener.onFetchFailure(message);
                } else if (error instanceof TimeoutError) {
                    message = "Connection TimeOut! Please check your internet connection.";
                    if (listener != null) listener.onFetchFailure(message);
                }
                //Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
                System.out.println("Error :" + message);
//                if (error instanceof NoConnectionError) {
//                    Toast.makeText(mContext,"Network Connectivity Problem",Toast.LENGTH_SHORT).show();
//                    listener.onFetchFailure("Network Connectivity Problem");
//                } else if (error.networkResponse != null && error.networkResponse.data != null) {
//                    VolleyError volley_error = new VolleyError(new String(error.networkResponse.data));
//                    String errorMessage = "";
//                    try {
//                        JSONObject errorJson = new JSONObject(volley_error.getMessage().toString());
//                        if (errorJson.has("error")) errorMessage = errorJson.getString("error");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    if (errorMessage.isEmpty()) {
//                        errorMessage = volley_error.getMessage();
//                    }
//
//                    if (listener != null) listener.onFetchFailure(errorMessage);
//                } else {
//                    Toast.makeText(mContext,"Something went wrong. Please try again later",Toast.LENGTH_SHORT).show();
//                    listener.onFetchFailure("Something went wrong. Please try again later");
//                }

            }
        });

        RequestQueueService.getInstance(context).addToRequestQueue(postRequest.setShouldCache(false));
    }
}
