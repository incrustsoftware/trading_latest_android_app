package com.myfixcost.trade.Dashboard.Position_Menu;

public class Trade_Position_Array {

    String Ticker;
    String ContractMonth;
    String MarketPrice;
    String PLOpen;
    String PLToday;
    String BuyPrice;
    String PLPercent;
    String Quantity;
    String TotalPLToday;
    String TotalPLPercent;
    String TotalPLOpen;
    String SQty;
    String BQty;

    public String getSQty() {
        return SQty;
    }

    public void setSQty(String SQty) {
        this.SQty = SQty;
    }

    public String getBQty() {
        return BQty;
    }

    public void setBQty(String BQty) {
        this.BQty = BQty;
    }

    public String getTicker() {
        return Ticker;
    }

    public void setTicker(String ticker) {
        Ticker = ticker;
    }

    public String getContractMonth() {
        return ContractMonth;
    }

    public void setContractMonth(String contractMonth) {
        ContractMonth = contractMonth;
    }

    public String getMarketPrice() {
        return MarketPrice;
    }

    public void setMarketPrice(String marketPrice) {
        MarketPrice = marketPrice;
    }

    public String getPLOpen() {
        return PLOpen;
    }

    public void setPLOpen(String PLOpen) {
        this.PLOpen = PLOpen;
    }

    public String getPLToday() {
        return PLToday;
    }

    public void setPLToday(String PLToday) {
        this.PLToday = PLToday;
    }

    public String getBuyPrice() {
        return BuyPrice;
    }

    public void setBuyPrice(String buyPrice) {
        BuyPrice = buyPrice;
    }

    public String getPLPercent() {
        return PLPercent;
    }

    public void setPLPercent(String PLPercent) {
        this.PLPercent = PLPercent;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getTotalPLToday() {
        return TotalPLToday;
    }

    public void setTotalPLToday(String totalPLToday) {
        TotalPLToday = totalPLToday;
    }

    public String getTotalPLPercent() {
        return TotalPLPercent;
    }

    public void setTotalPLPercent(String totalPLPercent) {
        TotalPLPercent = totalPLPercent;
    }

    public String getTotalPLOpen() {
        return TotalPLOpen;
    }

    public void setTotalPLOpen(String totalPLOpen) {
        TotalPLOpen = totalPLOpen;
    }

    public Trade_Position_Array(String ticker, String contractMonth, String marketPrice, String PLOpen, String PLToday, String buyPrice, String PLPercent, String quantity, String totalPLToday, String totalPLPercent, String totalPLOpen) {
        Ticker = ticker;
        ContractMonth = contractMonth;
        MarketPrice = marketPrice;
        this.PLOpen = PLOpen;
        this.PLToday = PLToday;
        BuyPrice = buyPrice;
        this.PLPercent = PLPercent;
        Quantity = quantity;
        TotalPLToday = totalPLToday;
        TotalPLPercent = totalPLPercent;
        TotalPLOpen = totalPLOpen;
    }


    public Trade_Position_Array() {

    }
}
