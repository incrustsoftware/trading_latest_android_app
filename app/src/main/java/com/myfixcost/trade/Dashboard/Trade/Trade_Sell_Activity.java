package com.myfixcost.trade.Dashboard.Trade;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.Constant.MyGlobalClass;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.HashMap;

public class Trade_Sell_Activity extends AppCompatActivity {

    TextView txt_sell_buy_contract, txt_sell_buy_button;
    String status, orderStatus, stop_loss_data, target_sell_price, ticker_name, order_type;
    TextView txt_stock_name, txt_available_quantity, txt_ticker_desc;
    ImageView img_back, img_minus_button, img_plus_button;
    int counter;
    Dialog dialog;
    EditText ed_contract_month, ed_market_price, ed_estimated_amt, ed_stop_loss, ed_target_sell_price, ed_after_sell, ed_rel_quantity;
    String Ticker, ContractMonth, MarketPrice, AvailableFunds, AfterBuyAvailableFunds, EstAmount, StopLoss, TargetSellPrice, TickerDesc, UserId, Type, BrokerCommission, ExchangeFees, CreditReceived, AvailableQty, Quantity;
    MySharedPreference mySharedPreference;
    DecimalFormat decimalFormat;
    TextView txt_after_buy_sell, txt_avail_quan, target_sell_price_question, stoploss_question;
    TextView txt_info, txt_info_list;
    String state,state_info;
    boolean refreshSellBuyValue = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trade_sell_activity);

        status = getIntent().getStringExtra("status");
        ticker_name = getIntent().getStringExtra("ticker_name");
        mySharedPreference = new MySharedPreference(this);
        initUI();

        txt_sell_buy_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_stop_loss.getText().toString().equalsIgnoreCase("")) {
                    ed_stop_loss.setError("Enter amount");
                } else if (ed_target_sell_price.getText().toString().equalsIgnoreCase("")) {
                    ed_target_sell_price.setError("Enter amount");
                } else if (ed_rel_quantity.getText().toString().equalsIgnoreCase("")){
                    ed_rel_quantity.setError("Quantity should be greater than zero");
                }else {
                    int quan = Integer.parseInt(ed_rel_quantity.getText().toString());
                    if (quan > 0) {
                        ed_rel_quantity.setError(null);
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(Trade_Sell_Activity.this);
                    } else {
                        ed_rel_quantity.setError("Quantity should be greater than zero");
                    }

                }
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_plus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                counter++;
                if (ed_rel_quantity.getText().toString().equalsIgnoreCase("")) {
                    counter = 0;
                } else {
                    int val_cnt = Integer.parseInt(ed_rel_quantity.getText().toString());
                    counter = val_cnt;
                }
                counter = counter + 1;
                ed_rel_quantity.setError(null);
                ed_rel_quantity.setText(String.valueOf(counter));
            }
        });

        img_minus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                counter--;
                if (ed_rel_quantity.getText().toString().equalsIgnoreCase("")) {
                    counter = 0;
                } else {
                    int val_cnt = Integer.parseInt(ed_rel_quantity.getText().toString());
                    counter = val_cnt;
                }
                if (!ed_rel_quantity.getText().toString().equalsIgnoreCase("0")) {
                    counter = counter - 1;
                    ed_rel_quantity.setError(null);
                    ed_rel_quantity.setText(String.valueOf(counter));
                }


            }
        });

        stoploss_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state = "What is StopLoss";
                state_info = "StopLoss";
                Click_Popup alert = new Click_Popup();
                alert.showDialog(Trade_Sell_Activity.this);
            }
        });

        target_sell_price_question.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state = "What is Target Sell Price";
                state_info = "Target Sell Price";
                Click_Popup alert = new Click_Popup();
                alert.showDialog(Trade_Sell_Activity.this);
            }
        });
    }

    private void initUI() {
        txt_sell_buy_contract = findViewById(R.id.txt_sell_buy_contract);
        txt_sell_buy_button = findViewById(R.id.txt_sell_buy_button);
        img_back = findViewById(R.id.img_back);
        img_minus_button = findViewById(R.id.img_minus_button);
        img_plus_button = findViewById(R.id.img_plus_button);

        txt_stock_name = findViewById(R.id.txt_stock_name);
        txt_available_quantity = findViewById(R.id.txt_available_quantity);
        txt_ticker_desc = findViewById(R.id.txt_ticker_desc);

        ed_contract_month = findViewById(R.id.ed_contract_month);
        ed_market_price = findViewById(R.id.ed_market_price);
        ed_estimated_amt = findViewById(R.id.ed_estimated_amt);
        ed_stop_loss = findViewById(R.id.ed_stop_loss);
        ed_target_sell_price = findViewById(R.id.ed_target_sell_price);
        ed_after_sell = findViewById(R.id.ed_after_sell);
        ed_rel_quantity = findViewById(R.id.ed_rel_quantity);
        txt_after_buy_sell = findViewById(R.id.txt_after_buy_sell);
        txt_avail_quan = findViewById(R.id.txt_avail_quan);
        target_sell_price_question = findViewById(R.id.target_sell_price_question);
        stoploss_question = findViewById(R.id.stoploss_question);

        if (status.equalsIgnoreCase("buy")) {
            txt_sell_buy_contract.setText("Buy Contract");
            txt_avail_quan.setText("Available Funds :");
            txt_after_buy_sell.setText("After Buy - Estimated Available Funds");
            txt_sell_buy_button.setBackground(getResources().getDrawable(R.drawable.background_email_login));
            txt_sell_buy_button.setText("BUY");
            order_type = "B";
            orderStatus = "Buy";
        } else {
            txt_sell_buy_contract.setText("Sell Contract");
            txt_avail_quan.setText("Available Quantity :");
            txt_after_buy_sell.setText("After Sell - Estimated Available Funds");
            txt_sell_buy_button.setBackground(getResources().getDrawable(R.drawable.background_sell_button));
            txt_sell_buy_button.setText("SELL");
            order_type = "S";
            orderStatus = "Sell";
        }

        GetTradeFullData();
        editTextStopLoss(ed_stop_loss);
        editTextTargetSellPrice(ed_target_sell_price);
    }

    // get stop loss method...
    public void editTextStopLoss(EditText ed_stop_loss) {
        ed_stop_loss.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    stop_loss_data = s.toString();
                }
            }
        });
    }

    // get target sell price method...
    public void editTextTargetSellPrice(EditText ed_target_sell_price) {
        ed_target_sell_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    target_sell_price = s.toString();
                }
            }
        });
    }

    public class ViewDialog {

        public void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_order_confirmation_popup);

            AppCompatButton btn_submit = dialog.findViewById(R.id.btn_submit);
            AppCompatButton btn_cancel = dialog.findViewById(R.id.btn_cancel);
            ImageView img_close = dialog.findViewById(R.id.img_close);


            TextView txt_buy_or_sell = dialog.findViewById(R.id.txt_buy_or_sell);
            TextView txt_ticker_name = dialog.findViewById(R.id.txt_ticker_name);
            TextView txt_contract_month = dialog.findViewById(R.id.txt_contract_month);
            TextView txt_price = dialog.findViewById(R.id.txt_price);
            TextView txt_quantity = dialog.findViewById(R.id.txt_quantity);
            TextView txt_broker_commission = dialog.findViewById(R.id.txt_broker_commission);
            TextView txt_exchange_fees = dialog.findViewById(R.id.txt_exchange_fees);
            TextView txt_estimated_amt = dialog.findViewById(R.id.txt_estimated_amt);
            TextView txt_stop_loss = dialog.findViewById(R.id.txt_stop_loss);
            TextView txt_target_sell_price = dialog.findViewById(R.id.txt_target_sell_price);
            TextView txt_estimated_avl_funds = dialog.findViewById(R.id.txt_estimated_avl_funds);

            decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");

            if (status.equalsIgnoreCase("buy")) {
                txt_estimated_amt.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_order_red));
                txt_exchange_fees.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_text_color));
                txt_broker_commission.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_text_color));

                decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");
                Double Est_Amount = Double.valueOf(EstAmount);
                txt_estimated_amt.setText("(" + "$" + decimalFormat.format(Est_Amount) + ")");

                Double Broker_Commission = Double.valueOf(BrokerCommission);
                txt_broker_commission.setText("$" + decimalFormat.format(Broker_Commission));

                Double Exchange_Fees = Double.valueOf(ExchangeFees);
                txt_exchange_fees.setText("$" + decimalFormat.format(Exchange_Fees));
            } else {
                txt_exchange_fees.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_order_red));
                txt_broker_commission.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_order_red));
                txt_estimated_amt.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_text_color));

                decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");
                Double Est_Amount = Double.valueOf(EstAmount);
                Double Broker_Commission = Double.valueOf(BrokerCommission);
                Double Exchange_Fees = Double.valueOf(ExchangeFees);

                txt_estimated_amt.setText("$" + decimalFormat.format(Est_Amount));
                txt_broker_commission.setText("(" + "$" + decimalFormat.format(Broker_Commission) + ")");
                txt_exchange_fees.setText("(" + "$" + decimalFormat.format(Exchange_Fees) + ")");
            }

            txt_buy_or_sell.setText(orderStatus);
            txt_ticker_name.setText(TickerDesc);
            txt_contract_month.setText(ContractMonth);

            Double Market_Price = Double.valueOf(MarketPrice);
            txt_price.setText("$" + decimalFormat.format(Market_Price));

            txt_quantity.setText(ed_rel_quantity.getText().toString());

            stop_loss_data = stop_loss_data.replace("$", "");
            Double d_stop_loss_data = Double.valueOf(stop_loss_data);

            target_sell_price = target_sell_price.replace("$", "");
            Double d_target_sell_price = Double.valueOf(target_sell_price);

            txt_stop_loss.setText("$" + decimalFormat.format(d_stop_loss_data));
            txt_target_sell_price.setText("$" + decimalFormat.format(d_target_sell_price));

            Double d_AfterBuyAvailableFunds = Double.valueOf(AfterBuyAvailableFunds);
            txt_estimated_avl_funds.setText("$" + decimalFormat.format(d_AfterBuyAvailableFunds));

            btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    SubmitTradeFullData();
                }
            });

            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        }
    }

    private void GetTradeFullData() {

        String Url = Constant.TRADE_FULL_PAGE_DATA;
        System.out.println("GetTradeFullData url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("orderType", order_type);
        postParams.put("ticker", ticker_name);
        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("accountType", ((MyGlobalClass) this.getApplicationContext()).getAccountType());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(Trade_Sell_Activity.this, getMasterResons2, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons2 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(Trade_Sell_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                refreshSellBuyValue  = true;
                refreshAllContent(5000);
                JSONObject new_data = data.getJSONObject("data");

                Ticker = new_data.getString("Ticker");
                ContractMonth = new_data.getString("ContractMonth");
                MarketPrice = new_data.getString("MarketPrice");
                AvailableFunds = new_data.getString("AvailableFunds");
                AfterBuyAvailableFunds = new_data.getString("AfterBuyAvailableFunds");
                AvailableQty = new_data.getString("AvailableQty");
                Quantity = new_data.getString("Quantity");
                EstAmount = new_data.getString("EstAmount");
                StopLoss = new_data.getString("StopLoss");
                TargetSellPrice = new_data.getString("TargetSellPrice");
                TickerDesc = new_data.getString("TickerDesc");
                UserId = new_data.getString("UserId");
                Type = new_data.getString("OrderType");
                BrokerCommission = new_data.getString("BrokerCommission");
                ExchangeFees = new_data.getString("ExchangeFees");
                CreditReceived = new_data.getString("CreditReceived");

                decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");

                target_sell_price = TargetSellPrice;
                stop_loss_data = StopLoss;

                if (status.equalsIgnoreCase("buy")) {
                    Double est_amt = Double.valueOf(EstAmount);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");
                    ed_estimated_amt.setText("$" + decimalFormat.format(est_amt));
                    ed_estimated_amt.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_order_red));

                    if (AvailableFunds.startsWith("-")) {
                        AvailableFunds = AvailableFunds.replace("-", "");
                        Double Available_Funds = Double.valueOf(AvailableFunds);
                        txt_available_quantity.setText("$" + "(" + decimalFormat.format(Available_Funds) + ")");
                        txt_available_quantity.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_order_red));
                    } else {
                        Double Available_Funds = Double.valueOf(AvailableFunds);
                        txt_available_quantity.setText("$" + decimalFormat.format(Available_Funds));
                        txt_available_quantity.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.app_color));
                    }


                } else {
                    Double est_amt = Double.valueOf(EstAmount);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");
                    ed_estimated_amt.setText("$" + decimalFormat.format(est_amt));
                    ed_estimated_amt.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_text_color));
                    txt_available_quantity.setText(AvailableQty);

                }

                txt_stock_name.setText(Ticker);
                ed_rel_quantity.setText(Quantity);
                txt_ticker_desc.setText(TickerDesc);
                ed_contract_month.setText(ContractMonth);

                Double Market_Price = Double.valueOf(MarketPrice);
                ed_market_price.setText("$" + decimalFormat.format(Market_Price));

                Double Stop_Loss = Double.valueOf(StopLoss);
                ed_stop_loss.setText("$" + decimalFormat.format(Stop_Loss));

                Double Target_Sell_Price = Double.valueOf(TargetSellPrice);
                ed_target_sell_price.setText("$" + decimalFormat.format(Target_Sell_Price));

                Double After_Buy_Available_Funds = Double.valueOf(AfterBuyAvailableFunds);
                ed_after_sell.setText("$" + decimalFormat.format(After_Buy_Available_Funds));

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_Sell_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(Trade_Sell_Activity.this, msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_Sell_Activity.this);
        }
    };


    private void SubmitTradeFullData() {

        String Url = Constant.SUBMIT_TRADE_FULL_PAGE_DATA;
        System.out.println("SubmitTradeFullData url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        StopLoss = StopLoss.replace(",", "");
        TargetSellPrice = TargetSellPrice.replace(",", "");

        postParams.put("ticker", Ticker);
        postParams.put("contractMonth", ContractMonth);
        postParams.put("marketPrice", MarketPrice);
        postParams.put("availableFunds", AvailableFunds);
        postParams.put("afterBuyAvailableFunds", AfterBuyAvailableFunds);
        postParams.put("estAmount", EstAmount);
        postParams.put("availableQty", AvailableQty);
        postParams.put("quantity", ed_rel_quantity.getText().toString());
        postParams.put("stopLoss", StopLoss);
        postParams.put("targetSellPrice", TargetSellPrice);
        postParams.put("tickerDesc", TickerDesc);
        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("orderType", Type);
        postParams.put("brokerCommission", BrokerCommission);
        postParams.put("exchangeFees", ExchangeFees);
        postParams.put("creditReceived", CreditReceived);
        postParams.put("accountType", ((MyGlobalClass) this.getApplication()).getAccountType());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(Trade_Sell_Activity.this, getMasterResons3, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons3 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(Trade_Sell_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                    JSONObject new_data = data.getJSONObject("data");
                refreshSellBuyValue = false;
                    Ticker = new_data.getString("Ticker");
                    ContractMonth = new_data.getString("ContractMonth");
                    MarketPrice = new_data.getString("MarketPrice");
                    AvailableFunds = new_data.getString("AvailableFunds");
                    AfterBuyAvailableFunds = new_data.getString("AfterBuyAvailableFunds");
                    EstAmount = new_data.getString("EstAmount");
                    StopLoss = new_data.getString("StopLoss");
                    AvailableQty = new_data.getString("AvailableQty");
                    Quantity = new_data.getString("Quantity");
                    TargetSellPrice = new_data.getString("TargetSellPrice");
                    TickerDesc = new_data.getString("TickerDesc");
                    UserId = new_data.getString("UserId");
                    Type = new_data.getString("OrderType");
                    BrokerCommission = new_data.getString("BrokerCommission");
                    ExchangeFees = new_data.getString("ExchangeFees");
                    dialog.dismiss();

                    Intent submit_data = new Intent(Trade_Sell_Activity.this, Trade_Sell_Contract_Details_Activity.class);
                    submit_data.putExtra("Type", orderStatus);
                    submit_data.putExtra("Ticker", ticker_name);
                    submit_data.putExtra("ContractMonth", ContractMonth);
                    submit_data.putExtra("quantity", Quantity);
                    submit_data.putExtra("MarketPrice", MarketPrice);
                    submit_data.putExtra("CreditReceived", CreditReceived);
                    submit_data.putExtra("AvailableFunds", AvailableFunds);
                    startActivity(submit_data);


            } else if (data.getString("status").equalsIgnoreCase("501")){
                Toast.makeText(Trade_Sell_Activity.this, data.getString("message"), Toast.LENGTH_LONG).show();
                RequestQueueService.cancelProgressDialog();
                dialog.dismiss();
            } else {
                Toast.makeText(Trade_Sell_Activity.this, data.getString("message"), Toast.LENGTH_LONG).show();
                RequestQueueService.cancelProgressDialog();
                dialog.dismiss();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(Trade_Sell_Activity.this, msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_Sell_Activity.this);
        }
    };

    public class Click_Popup {

        public void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_stoploss_popup);

            AppCompatButton btn_confirm = dialog.findViewById(R.id.btn_confirm);
            txt_info = dialog.findViewById(R.id.txt_info);
            txt_info_list = dialog.findViewById(R.id.txt_info_list);

            txt_info.setText(state);
            txt_info_list.setText(state_info);

            btn_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    private void GetTradeFullDataCycle() {

        String Url = Constant.TRADE_FULL_PAGE_DATA;
        System.out.println("GetTradeFullData url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("orderType", order_type);
        postParams.put("ticker", ticker_name);
        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("accountType", ((MyGlobalClass) this.getApplicationContext()).getAccountType());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(Trade_Sell_Activity.this, getMasterResons5, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons5 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                if (refreshSellBuyValue) {
                    refreshAllContent(5000);
                } else {
                    refreshSellBuyValue = false;
                }
                JSONObject new_data = data.getJSONObject("data");

                Ticker = new_data.getString("Ticker");
                ContractMonth = new_data.getString("ContractMonth");
                MarketPrice = new_data.getString("MarketPrice");
                AvailableFunds = new_data.getString("AvailableFunds");
                AfterBuyAvailableFunds = new_data.getString("AfterBuyAvailableFunds");
                AvailableQty = new_data.getString("AvailableQty");
                Quantity = new_data.getString("Quantity");
                EstAmount = new_data.getString("EstAmount");
                StopLoss = new_data.getString("StopLoss");
                TargetSellPrice = new_data.getString("TargetSellPrice");
                TickerDesc = new_data.getString("TickerDesc");
                UserId = new_data.getString("UserId");
                Type = new_data.getString("OrderType");
                BrokerCommission = new_data.getString("BrokerCommission");
                ExchangeFees = new_data.getString("ExchangeFees");
                CreditReceived = new_data.getString("CreditReceived");

                decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");

//                target_sell_price = TargetSellPrice;
//                stop_loss_data = StopLoss;

                if (status.equalsIgnoreCase("buy")) {
                    Double est_amt = Double.valueOf(EstAmount);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");
                    ed_estimated_amt.setText("$" + decimalFormat.format(est_amt));
                    ed_estimated_amt.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_order_red));

                    if (AvailableFunds.startsWith("-")) {
                        AvailableFunds = AvailableFunds.replace("-", "");
                        Double Available_Funds = Double.valueOf(AvailableFunds);
                        txt_available_quantity.setText("$" + "(" + decimalFormat.format(Available_Funds) + ")");
                        txt_available_quantity.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.color_order_red));
                    } else {
                        Double Available_Funds = Double.valueOf(AvailableFunds);
                        txt_available_quantity.setText("$" + decimalFormat.format(Available_Funds));
                        txt_available_quantity.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.app_color));
                    }


                } else {
                    Double est_amt = Double.valueOf(EstAmount);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getApplicationContext(), "");
                    ed_estimated_amt.setText("$" + decimalFormat.format(est_amt));
                    ed_estimated_amt.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.login_text_color));
                    txt_available_quantity.setText(AvailableQty);

                }

                txt_stock_name.setText(Ticker);
                //ed_rel_quantity.setText(Quantity);
                txt_ticker_desc.setText(TickerDesc);
                ed_contract_month.setText(ContractMonth);

                Double Market_Price = Double.valueOf(MarketPrice);
                ed_market_price.setText("$" + decimalFormat.format(Market_Price));

//                Double Stop_Loss = Double.valueOf(StopLoss);
//                ed_stop_loss.setText("$" + decimalFormat.format(Stop_Loss));
//
//                Double Target_Sell_Price = Double.valueOf(TargetSellPrice);
//                ed_target_sell_price.setText("$" + decimalFormat.format(Target_Sell_Price));
//
//                Double After_Buy_Available_Funds = Double.valueOf(AfterBuyAvailableFunds);
//                ed_after_sell.setText("$" + decimalFormat.format(After_Buy_Available_Funds));

            } else {
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(Trade_Sell_Activity.this, msg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
        }
    };

    public void refreshAllContent(final long timetoupdate) {
        new CountDownTimer(timetoupdate, 3000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                if (refreshSellBuyValue) {
                    System.out.println("UPDATE SELLBUY CONTENT HERE");
                    GetTradeFullDataCycle();
                }

            }
        }.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        refreshSellBuyValue = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        refreshSellBuyValue = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        refreshSellBuyValue = false;
    }

}
