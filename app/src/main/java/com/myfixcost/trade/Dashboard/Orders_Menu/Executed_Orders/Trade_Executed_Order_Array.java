package com.myfixcost.trade.Dashboard.Orders_Menu.Executed_Orders;

public class Trade_Executed_Order_Array {

    String Ticker, OrderId, ContractMonth, CurrentPrice, OrderType, Price, ValidUpTo, OrderdATE, Quantity;


    public Trade_Executed_Order_Array(String ticker, String orderId, String contractMonth, String currentPrice, String orderType, String price, String validUpTo, String orderdATE, String quantity) {
        Ticker = ticker;
        OrderId = orderId;
        ContractMonth = contractMonth;
        CurrentPrice = currentPrice;
        OrderType = orderType;
        Price = price;
        ValidUpTo = validUpTo;
        OrderdATE = orderdATE;
        Quantity = quantity;
    }

    public Trade_Executed_Order_Array() {

    }


    public String getTicker() {
        return Ticker;
    }

    public void setTicker(String ticker) {
        Ticker = ticker;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getContractMonth() {
        return ContractMonth;
    }

    public void setContractMonth(String contractMonth) {
        ContractMonth = contractMonth;
    }

    public String getCurrentPrice() {
        return CurrentPrice;
    }

    public void setCurrentPrice(String currentPrice) {
        CurrentPrice = currentPrice;
    }

    public String getOrderType() {
        return OrderType;
    }

    public void setOrderType(String orderType) {
        OrderType = orderType;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getValidUpTo() {
        return ValidUpTo;
    }

    public void setValidUpTo(String validUpTo) {
        ValidUpTo = validUpTo;
    }

    public String getOrderdATE() {
        return OrderdATE;
    }

    public void setOrderdATE(String orderdATE) {
        OrderdATE = orderdATE;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }
}
