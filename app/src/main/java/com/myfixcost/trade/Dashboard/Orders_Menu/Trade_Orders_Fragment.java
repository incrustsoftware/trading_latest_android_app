package com.myfixcost.trade.Dashboard.Orders_Menu;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.Constant.MyGlobalClass;
import com.myfixcost.trade.Dashboard.Orders_Menu.Executed_Orders.Trade_Executed_Order_Array;
import com.myfixcost.trade.Dashboard.Orders_Menu.Executed_Orders.Trade_Executed_Orders_Adapter;
import com.myfixcost.trade.Dashboard.Orders_Menu.Pending_Orders.Trade_Edit_Order_Interface;
import com.myfixcost.trade.Dashboard.Orders_Menu.Pending_Orders.Trade_Pending_Orders_Adapter;
import com.myfixcost.trade.Dashboard.Orders_Menu.Pending_Orders.Trade_Pending_Orders_Array;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Trade_Orders_Fragment extends Fragment implements Trade_Edit_Order_Interface {

    LinearLayout lin_pending_orders, lin_executed_click, lin_pending_click;
    RelativeLayout lin_executed_orders, calendar_from, calendar_to;
    RecyclerView rec_pending_data, rec_executed_orders;
    TextView txt_pending, txt_executed_orders, txt_to_calendar, txt_from_calendar;
    Button btn_email_report;
    ArrayList<Trade_Pending_Orders_Array> trade_pending_orders_arrays = new ArrayList<>();
    public ArrayList<Trade_Executed_Order_Array> trade_executed_order_arrays = new ArrayList<>();
    Trade_Pending_Orders_Adapter trade_pending_orders_adapter;
    Trade_Executed_Orders_Adapter trade_executed_orders_adapter;
    Dialog dialog;
    Trade_Edit_Order_Interface trade_edit_order_interface;
    String Order_Id, toDate = "", fromDate = "";
    MySharedPreference mySharedPreference;
    boolean pending_click = true;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trade_edit_order_interface = this;
        mySharedPreference = new MySharedPreference(getContext());

        Date todayDate = Calendar.getInstance().getTime();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String todayString = formatter.format(todayDate);
        fromDate = todayString;
        toDate = todayString;


    }

    // method to create a view on page...
    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.trade_orders_fragment, container, false);

        initUI(root);

        txt_to_calendar.setText(toDate);
        txt_from_calendar.setText(fromDate);

        lin_executed_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetExecutedOrders();
                pending_click = false;
                lin_executed_orders.setVisibility(View.VISIBLE);
                lin_pending_orders.setVisibility(View.GONE);
                lin_executed_click.setBackgroundColor(getResources().getColor(R.color.app_color));
                lin_pending_click.setBackgroundColor(getResources().getColor(R.color.order_bg));
                txt_executed_orders.setTextColor(getResources().getColor(R.color.white));
                txt_pending.setTextColor(getResources().getColor(R.color.color_news));
            }
        });

        lin_pending_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GetPendingOrders();
                pending_click = true;
                lin_executed_orders.setVisibility(View.GONE);
                lin_pending_orders.setVisibility(View.VISIBLE);
                lin_executed_click.setBackgroundColor(getResources().getColor(R.color.order_bg));
                lin_pending_click.setBackgroundColor(getResources().getColor(R.color.app_color));
                txt_executed_orders.setTextColor(getResources().getColor(R.color.color_news));
                txt_pending.setTextColor(getResources().getColor(R.color.white));
            }
        });

        btn_email_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        calendar_to.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openToCalendar();
            }
        });

        calendar_from.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFromCalendar();
            }
        });

//        GetPendingOrders();

        return root;
    }

    // method to initialize UI on page...
    private void initUI(View root) {
        lin_pending_orders = root.findViewById(R.id.lin_pending_orders);
        lin_executed_orders = root.findViewById(R.id.lin_executed_orders);
        rec_pending_data = root.findViewById(R.id.rec_pending_data);
        rec_executed_orders = root.findViewById(R.id.rec_executed_orders);
        lin_executed_click = root.findViewById(R.id.lin_executed_click);
        lin_pending_click = root.findViewById(R.id.lin_pending_click);
        txt_pending = root.findViewById(R.id.txt_pending);
        txt_executed_orders = root.findViewById(R.id.txt_executed_orders);
        btn_email_report = root.findViewById(R.id.btn_email_report);
        calendar_from = root.findViewById(R.id.calendar_from);
        calendar_to = root.findViewById(R.id.calendar_to);
        txt_to_calendar = root.findViewById(R.id.txt_to_calendar);
        txt_from_calendar = root.findViewById(R.id.txt_from_calendar);
    }

    // method to get pending orders from api...
    private void GetPendingOrders() {

        String Url = Constant.PENDING_ORDERS_DATA;
        System.out.println("GetPendingOrders url : " + Url);
        trade_pending_orders_arrays.clear();
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("startDate", "");
        postParams.put("endDate", "");
        postParams.put("accountType", ((MyGlobalClass) getContext().getApplicationContext()).getAccountType());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons1, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons1 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();

                JSONArray service = data.getJSONArray("data");

                for (int i = 0; i < service.length(); i++) {
                    JSONObject history = service.getJSONObject(i);
                    Trade_Pending_Orders_Array getService = new Trade_Pending_Orders_Array();

                    getService.setTicker(history.getString("Ticker"));
                    getService.setOrderId(history.getString("OrderId"));
                    getService.setContractMonth(history.getString("ContractMonth"));
                    getService.setCurrentPrice(history.getString("CurrentPrice"));
                    getService.setOrderType(history.getString("OrderType"));
                    getService.setPrice(history.getString("Price"));
                    getService.setValidUpTo(history.getString("ValidUpTo"));
                    getService.setOrderdATE(history.getString("OrderdATE"));
                    getService.setQuantity(history.getString("Quantity"));

                    trade_pending_orders_arrays.add(getService);
                }
                trade_pending_orders_adapter = new Trade_Pending_Orders_Adapter(trade_pending_orders_arrays, trade_edit_order_interface, getContext());
                rec_pending_data.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                rec_pending_data.setAdapter(trade_pending_orders_adapter);
                trade_pending_orders_adapter.notifyDataSetChanged();


            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    // method to get executed orders from api...
    private void GetExecutedOrders() {

        String Url = Constant.EXECUTED_ORDERS_DATA;
        System.out.println("GetExecutedOrders url : " + Url);
        trade_executed_order_arrays.clear();
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("startDate", fromDate);
        postParams.put("endDate", toDate);
        postParams.put("accountType", ((MyGlobalClass) getContext().getApplicationContext()).getAccountType());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons2, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons2 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();

                JSONArray service = data.getJSONArray("data");

                for (int i = 0; i < service.length(); i++) {
                    JSONObject history = service.getJSONObject(i);
                    Trade_Executed_Order_Array getService = new Trade_Executed_Order_Array();

                    getService.setTicker(history.getString("Ticker"));
                    getService.setOrderId(history.getString("OrderId"));
                    getService.setContractMonth(history.getString("ContractMonth"));
                    getService.setCurrentPrice(history.getString("CurrentPrice"));
                    getService.setOrderType(history.getString("OrderType"));
                    getService.setPrice(history.getString("Price"));
                    getService.setValidUpTo(history.getString("ValidUpTo"));
                    getService.setOrderdATE(history.getString("TradeDate"));
                    getService.setQuantity(history.getString("Quantity"));

                    trade_executed_order_arrays.add(getService);
                }
                trade_executed_orders_adapter = new Trade_Executed_Orders_Adapter(trade_executed_order_arrays, getContext());
                rec_executed_orders.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                rec_executed_orders.setAdapter(trade_executed_orders_adapter);
                trade_executed_orders_adapter.notifyDataSetChanged();


            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    // method to delete order on orders page...
    private void DeleteWatchlistData() {

        String Url = Constant.DELETE_ORDERS_DATA;
        System.out.println("DeleteWatchlistData url : " + Url);
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("orderId", Order_Id);
        postParams.put("accountType", ((MyGlobalClass) getContext().getApplicationContext()).getAccountType());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons3, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons3 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                dialog.dismiss();
                GetPendingOrders();

            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    @Override
    public void editorderdata(String order_id) {
        Order_Id = order_id;
        ViewDialog alert = new ViewDialog();
        alert.showDialog(getActivity());
    }

    // method to show delete order popup...
    public class ViewDialog {

        public void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_order_delete_popup);

            AppCompatButton btn_cancel = dialog.findViewById(R.id.btn_cancel);
            AppCompatButton btn_confirm = dialog.findViewById(R.id.btn_confirm);
            TextView txt_watchlist_name = dialog.findViewById(R.id.txt_watchlist_name);
            TextView txt_watch_name = dialog.findViewById(R.id.txt_watch_name);
            ImageView img_close = dialog.findViewById(R.id.img_close);


            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            btn_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeleteWatchlistData();
                }
            });

            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            dialog.show();
        }
    }

    // method to open fram calendar on page...
    public void openFromCalendar() {
        final Calendar c = Calendar.getInstance();
        int mYear, mMonth, mDay;
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        fromDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                        SimpleDateFormat curFormater = new SimpleDateFormat("dd/M/yyyy");
                        Date dateObj = null;
                        try {
                            dateObj = curFormater.parse(fromDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat postFormater = new SimpleDateFormat("dd/MM/yyyy");

                        String newDateStr = postFormater.format(dateObj);
                        fromDate = newDateStr;
                        txt_from_calendar.setText(newDateStr);
                        GetExecutedOrders();
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    // method to open to calendar on page...
    public void openToCalendar() {
        final Calendar c = Calendar.getInstance();
        int mYear, mMonth, mDay;
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        toDate = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                        SimpleDateFormat curFormater = new SimpleDateFormat("dd/M/yyyy");
                        Date dateObj = null;
                        try {
                            dateObj = curFormater.parse(toDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        SimpleDateFormat postFormater = new SimpleDateFormat("dd/MM/yyyy");

                        String newDateStr = postFormater.format(dateObj);
                        toDate = newDateStr;
                        txt_to_calendar.setText(newDateStr);
                        GetExecutedOrders();
                    }
                }, mYear, mMonth, mDay);

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    // method to send report of executed orders to email...
    private void SendEmailReport() {

        String Url = Constant.EXECUTED_ORDERS_DATA;
        System.out.println("SendEmailReport url : " + Url);
        trade_executed_order_arrays.clear();
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("startDate", fromDate);
        postParams.put("endDate", toDate);
        postParams.put("accountType", ((MyGlobalClass) getContext().getApplicationContext()).getAccountType());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons5, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons5 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();

            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        if (pending_click) {
            GetPendingOrders();
        }
    }
}
