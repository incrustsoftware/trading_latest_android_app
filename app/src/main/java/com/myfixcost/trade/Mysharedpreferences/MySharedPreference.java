package com.myfixcost.trade.Mysharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class MySharedPreference {
    Context context;

    public static SharedPreferences loginValue;
    public static SharedPreferences.Editor myLoginvalue;


    public MySharedPreference(Context context) {

        String isCustomerId = "customerId";
        loginValue = context.getSharedPreferences(isCustomerId, Context.MODE_PRIVATE);
        myLoginvalue = loginValue.edit();

        String isSwitchEnable = "isSwitchEnable";
        loginValue = context.getSharedPreferences(isSwitchEnable, Context.MODE_PRIVATE);
        myLoginvalue = loginValue.edit();

        String isRithmicId = "rithmicId";
        loginValue = context.getSharedPreferences(isRithmicId, Context.MODE_PRIVATE);
        myLoginvalue = loginValue.edit();

    }

    public void setisCustomerId(String flag) {
        myLoginvalue.putString("customerId", flag).apply();
    }

    public String getisCustomerId() {
        return loginValue.getString("customerId", "");
    }

    public void setisRithmicId(String flag) {
        myLoginvalue.putString("rithmicId", flag).apply();
    }

    public String getisRithmicId() {
        return loginValue.getString("rithmicId", "");
    }

    public void setisSwitchEnable(String flag) {
        myLoginvalue.putString("isSwitchEnable", flag).apply();
    }

    public String getisSwitchEnable() {
        return loginValue.getString("isSwitchEnable", "");
    }


    public String dateFormatter(Context context, String selectedCountry, String comingDate) {
        String convertedDate = new String();
        String inputFormat;
        String OutPutFormat;
        switch (selectedCountry) {
            case "Canada":
                inputFormat = "dd MMM yyyy";
                OutPutFormat = "MMM, yy";
                convertedDate = formatDate(comingDate, inputFormat, OutPutFormat);
                break;
            case "execute":
                inputFormat = "dd-MMM-yy";
                OutPutFormat = "MMM, yy";
                convertedDate = formatDate(comingDate, inputFormat, OutPutFormat);
                break;
            default:
                inputFormat = "dd-MM-yyyy";
                OutPutFormat = "MMM, yy";
                convertedDate = formatDate(comingDate, inputFormat, OutPutFormat);
                break;

        }

        return convertedDate;
    }

    // method to set formatted date...
    public String formatDate(String dateToFormat, String inputFormat, String outputFormat) {
        try {
            String convertedDate = new SimpleDateFormat(outputFormat)
                    .format(new SimpleDateFormat(inputFormat)
                            .parse(dateToFormat));

            //Update Date
            return convertedDate;

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public DecimalFormat NumberDecimalFormatter(Context context, String currencyCode) {
        Locale locale;
        DecimalFormat decimalFormat;
        DecimalFormatSymbols dfs;
        switch (currencyCode) {
            case "INR":
                locale = new Locale("en", "IN");
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
                decimalFormat.setMaximumFractionDigits(2);
                decimalFormat.setMinimumFractionDigits(2);
                dfs = DecimalFormatSymbols.getInstance(locale);
                dfs.setCurrencySymbol("₹ ");
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "USD":
                locale = new Locale("en", "US");
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
                decimalFormat.setMaximumFractionDigits(2);
                decimalFormat.setMinimumFractionDigits(2);
                decimalFormat.setGroupingSize(3);
                dfs = DecimalFormatSymbols.getInstance(locale);
                dfs.setCurrencySymbol("$ ");
                dfs.setGroupingSeparator(',');
                dfs.setDecimalSeparator('.');
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "SGD":
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();
                decimalFormat.setMaximumFractionDigits(2);
                decimalFormat.setMinimumFractionDigits(2);
                decimalFormat.setGroupingSize(3);
                dfs = DecimalFormatSymbols.getInstance();
                dfs.setCurrencySymbol("$ ");
                dfs.setGroupingSeparator(',');
                dfs.setDecimalSeparator('.');
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "AUD":
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();
                decimalFormat.setMaximumFractionDigits(2);
                decimalFormat.setMinimumFractionDigits(2);
                decimalFormat.setGroupingSize(3);
                dfs = DecimalFormatSymbols.getInstance();
                dfs.setCurrencySymbol("$ ");
                dfs.setGroupingSeparator(',');
                dfs.setDecimalSeparator('.');
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "CNY":
                locale = new Locale("en", "CA");
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
                dfs = DecimalFormatSymbols.getInstance(locale);
                dfs.setCurrencySymbol("¥ ");
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "GBP":
                locale = new Locale("en", "GB");
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
                dfs = DecimalFormatSymbols.getInstance(locale);
                decimalFormat.setMaximumFractionDigits(2);
                decimalFormat.setMinimumFractionDigits(2);
                dfs.setCurrencySymbol("£ ");
                decimalFormat.setGroupingSize(3);
                dfs.setGroupingSeparator(',');
                dfs.setDecimalSeparator('.');
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "YEN":
                locale = new Locale("ja", "JP");
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
                dfs = DecimalFormatSymbols.getInstance(locale);
                dfs.setCurrencySymbol("¥ ");
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "JPY":
                locale = new Locale("ja", "JP");
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
                dfs = DecimalFormatSymbols.getInstance(locale);
                dfs.setCurrencySymbol("¥ ");
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "AFN":
                //locale = new Locale("ja","JP");
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();
                decimalFormat.setMaximumFractionDigits(2);
                decimalFormat.setMinimumFractionDigits(2);
                dfs = DecimalFormatSymbols.getInstance();
                dfs.setCurrencySymbol(" ؋");
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "EUR":
                //locale = new Locale("fr","");
                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance();
                decimalFormat.setMaximumFractionDigits(2);
                decimalFormat.setMinimumFractionDigits(2);
                decimalFormat.setGroupingSize(3);
                dfs = DecimalFormatSymbols.getInstance();
//                dfs.setCurrencySymbol("€ ");
                dfs.setCurrencySymbol("\u20ac ");
                dfs.setGroupingSeparator(',');
                dfs.setDecimalSeparator('.');
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            case "TRY":
                decimalFormat = (DecimalFormat) DecimalFormat.getInstance();
                decimalFormat.setMaximumFractionDigits(2);
                decimalFormat.setMinimumFractionDigits(2);
                decimalFormat.setGroupingSize(3);
//                dfs = DecimalFormatSymbols.getInstance(Locale.US);
                dfs = new DecimalFormatSymbols();
                dfs.setCurrencySymbol("TRY ");
                dfs.setGroupingSeparator('.');
                dfs.setDecimalSeparator(',');
                decimalFormat.setDecimalFormatSymbols(dfs);
                break;

            default: // This state is default & is set for INR for now as p
//                locale = new Locale("en", "US");
//                decimalFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(locale);
//                decimalFormat.setMaximumFractionDigits(2);
//                decimalFormat.setMinimumFractionDigits(2);
//                decimalFormat.setGroupingSize(3);
//                dfs = DecimalFormatSymbols.getInstance(locale);
//                dfs.setGroupingSeparator(',');
//                dfs.setDecimalSeparator('.');
//                decimalFormat.setDecimalFormatSymbols(dfs);

                decimalFormat = (DecimalFormat) NumberFormat.getInstance(Locale.US);
                decimalFormat.setMaximumFractionDigits(2);
                decimalFormat.setMinimumFractionDigits(2);
                break;
        }


        return decimalFormat;
    }


    // method to create custom toast...
//    public void customToast(Context context, String message, int Type) {
//        LayoutInflater inflater = LayoutInflater.from(context);
//        View layout = inflater.inflate(R.layout.toast, null);
//        TextView text = (TextView) layout.findViewById(R.id.text);
//        text.setText(message);
//      /*  1=erro
//        2==sucess
//        3= validation*/
//        if (Type == 1) {
//            text.setBackgroundResource(R.drawable.red1);
//        } else if (Type == 2) {
//            text.setBackgroundResource(R.drawable.green1);
//        } else {
//            text.setBackgroundResource(R.drawable.yello1);
//        }
//        Toast toast = new Toast(context);
//        toast.setDuration(Toast.LENGTH_SHORT);
//        toast.setView(layout);
//        toast.show();
//    }

}


