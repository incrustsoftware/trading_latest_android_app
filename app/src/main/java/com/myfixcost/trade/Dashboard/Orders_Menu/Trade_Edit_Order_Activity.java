package com.myfixcost.trade.Dashboard.Orders_Menu;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.Constant.MyGlobalClass;
import com.myfixcost.trade.Dashboard.Trade.Trade_Sell_Activity;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Trade_Edit_Order_Activity extends AppCompatActivity {

    TextView txt_stock_name;
    TextView txt_quantity;
    EditText ed_contract_month, ed_order_type, ed_set_price, ed_quantity;
    Button btn_update_order;
    AppCompatButton btn_cancel_changes;
    ImageView img_back, img_minus_button, img_plus_button;
    String Ticker, OrderId, ContractMonth, CurrentPrice, OrderType, Price, ValidUpTo, OrderdATE, Quantity;
    String order_id, order_validity;
    int counter;
    Dialog dialog;
    MySharedPreference mySharedPreference;
    Spinner spinner_order_validity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trade_edit_order_activity);

        mySharedPreference = new MySharedPreference(getApplicationContext());
        order_id = getIntent().getStringExtra("order_id");

        initUI();

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        img_plus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_quantity.getText().toString().equalsIgnoreCase("")) {
                    counter = 0;
                } else {
                    int val_cnt = Integer.parseInt(ed_quantity.getText().toString());
                    counter = val_cnt;
                }
                counter = counter + 1;
                ed_quantity.setText(String.valueOf(counter));
            }
        });

        img_minus_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_quantity.getText().toString().equalsIgnoreCase("")) {
                    counter = 0;
                } else {
                    int val_cnt = Integer.parseInt(ed_quantity.getText().toString());
                    counter = val_cnt;
                }
                if (!ed_quantity.getText().toString().equalsIgnoreCase("0")) {
                    counter = counter - 1;
                    ed_quantity.setError(null);
                    ed_quantity.setText(String.valueOf(counter));
                }
            }
        });

        btn_cancel_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_update_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ed_contract_month.getText().toString().equalsIgnoreCase("")) {
                    ed_contract_month.setError("Enter Month");
                } else if (ed_order_type.getText().toString().equalsIgnoreCase("")) {
                    ed_order_type.setError("Enter Order Type");
                } else if (ed_set_price.getText().toString().equalsIgnoreCase("")) {
                    ed_set_price.setError("Enter Price");
                } else if (ed_quantity.getText().toString().equalsIgnoreCase("")) {
                    ed_quantity.setError("Quantity should be greater than zero");
                } else {

                    int quan = Integer.parseInt(ed_quantity.getText().toString());
                    if (quan > 0) {
                        ed_quantity.setError(null);
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(Trade_Edit_Order_Activity.this);
                    } else {
                        ed_quantity.setError("Quantity should be greater than zero");
                    }

                }
            }
        });


        GetEditOrderDetails();
    }

    // method to initialize UI on page...
    private void initUI() {
        txt_stock_name = findViewById(R.id.txt_stock_name);
        ed_contract_month = findViewById(R.id.ed_contract_month);
        ed_order_type = findViewById(R.id.ed_order_type);
        ed_set_price = findViewById(R.id.ed_set_price);
        ed_quantity = findViewById(R.id.ed_quantity);
        btn_update_order = findViewById(R.id.btn_update_order);
        btn_cancel_changes = findViewById(R.id.btn_cancel_changes);
        img_back = findViewById(R.id.img_back);
        img_minus_button = findViewById(R.id.img_minus_button);
        img_plus_button = findViewById(R.id.img_plus_button);
        spinner_order_validity = findViewById(R.id.spinner_order_validity);

        GetOrderValidity();
    }

    // method to get edit orders details from api...
    private void GetEditOrderDetails() {

        String Url = Constant.EDIT_ORDERS_DATA;
        System.out.println("GetEditOrderDetails url : " + Url);
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("OrderId", order_id);
        postParams.put("AccountType", ((MyGlobalClass) this.getApplicationContext()).getAccountType());

        System.out.println("jsonobject :" + postParams.toString());
        try {
            new PostApiRequest().post_request_Object(Trade_Edit_Order_Activity.this, getMasterResons2, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons2 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(Trade_Edit_Order_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();

                JSONObject service = data.getJSONObject("data");

                Ticker = service.getString("Ticker");
                OrderId = service.getString("OrderId");
                ContractMonth = service.getString("ContractMonth");
                CurrentPrice = service.getString("CurrentPrice");
                OrderType = service.getString("OrderType");
                Price = service.getString("Price");
                ValidUpTo = service.getString("ValidUpTo");
                OrderdATE = service.getString("OrderdATE");
                Quantity = service.getString("Quantity");

                txt_stock_name.setText("Ticker:" + Ticker);
                ed_contract_month.setText(ContractMonth);
                ed_order_type.setText(OrderType);
                ed_set_price.setText(Price);
                ed_quantity.setText(Quantity);

            } else {
                Toast.makeText(Trade_Edit_Order_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(Trade_Edit_Order_Activity.this, msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_Edit_Order_Activity.this);
        }
    };

    // method to update edit orders to api...
    private void UpdateOrderDetails() {

        String Url = Constant.UPDATE_ORDERS_DATA;
        System.out.println("UpdateOrderDetails url : " + Url);
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("userid", mySharedPreference.getisCustomerId());
        postParams.put("ticker", Ticker);
        postParams.put("orderId", order_id);
        postParams.put("contractMonth", ContractMonth);
        postParams.put("quantity", txt_quantity.getText().toString());
        postParams.put("orderType", OrderType);
        postParams.put("price", Price);
        postParams.put("accountType", ((MyGlobalClass) this.getApplicationContext()).getAccountType());
        postParams.put("orderdDuration", order_validity);


        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(Trade_Edit_Order_Activity.this, getMasterResons3, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons3 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(Trade_Edit_Order_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                dialog.dismiss();
                finish();

            } else {
                RequestQueueService.cancelProgressDialog();
                dialog.dismiss();
                Toast.makeText(Trade_Edit_Order_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(Trade_Edit_Order_Activity.this, msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_Edit_Order_Activity.this);
        }
    };

    // method to show dialog for update orders...
    public class ViewDialog {

        public void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_update_order_confirmation_popup);

            AppCompatButton btn_confirm = dialog.findViewById(R.id.btn_confirm);
            AppCompatButton btn_cancel = dialog.findViewById(R.id.btn_cancel);

            TextView txt_contract_month = dialog.findViewById(R.id.txt_contract_month);
            TextView txt_order_type = dialog.findViewById(R.id.txt_order_type);
            TextView txt_set_price = dialog.findViewById(R.id.txt_set_price);
            txt_quantity = dialog.findViewById(R.id.txt_quantity);
            TextView txt_order_validity = dialog.findViewById(R.id.txt_order_validity);


            txt_contract_month.setText(ContractMonth);
            txt_order_type.setText(OrderType);
            txt_set_price.setText("$ " + ed_set_price.getText().toString());
            Price = ed_set_price.getText().toString();
            txt_quantity.setText(ed_quantity.getText().toString());
            txt_order_validity.setText(order_validity);

            btn_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    UpdateOrderDetails();

                }
            });

            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        }
    }

    // method to show spinner to update validity of order...
    public void GetOrderValidity() {
        spinner_order_validity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                order_validity = parent.getItemAtPosition(position).toString();
                System.out.println("order_validity :" + order_validity);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }
}
