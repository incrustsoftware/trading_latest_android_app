package com.myfixcost.trade.Constant;

public class Constant {

//    public static final String BASE_URL = "http://35.175.221.200:7024/";
//    public static final String BASE_URL = "http://34.224.7.35:7024/";
    public static final String BASE_URL = "http://107.22.3.157:7024/";
    public static final String API_PERSONAL_INFO = BASE_URL + "Register/RegisterUser";
    public static final String LOGIN_USER = BASE_URL + "login/loginuser";
    public static final String SEND_OTP = BASE_URL + "login/SendOtp";
    public static final String VERIFY_OTP = BASE_URL + "login/VerifyOtp";
    public static final String ADD_RITHMIC_ID = BASE_URL + "Login/AddRithimic";
    public static final String GET_SUBSCRIPTION_TYPE = BASE_URL + "AppSetting/GetAllSubscription";
    public static final String GET_USER_BY_EMAIL = BASE_URL + "login/GetUserByEmail";
    public static final String POST_RESET_PASSWORD_DATA = BASE_URL + "login/ResetPassword";
    public static final String APPLY_PROMO_CODE = BASE_URL + "Promotion/ApplyPromoCode";
    public static final String POSITION_PAGE = BASE_URL + "Position/ReplayPnl?";
    public static final String TRADE_PAGE = BASE_URL + "Position/GetRefDataTicker";
    public static final String WATCHLIST_CLICK_TRADE_PAGE = BASE_URL + "Position/TradePage";
    public static final String TRADE_FULL_PAGE_DATA = BASE_URL + "Position/BuySellContract";
    public static final String SUBMIT_TRADE_FULL_PAGE_DATA = BASE_URL + "Position/PostBuySellOrder";
    public static final String GET_WATCHLIST = BASE_URL + "WatchList/GetAllMasterWatchList";
    public static final String ADD_WATCHLIST = BASE_URL + "WatchList/AddMasterWatchList";
    public static final String SAVE_WATCHLIST = BASE_URL + "WatchList/AddWatchListDetail";
    public static final String GET_WATCHLIST_TICKERS = BASE_URL + "WatchList/GetAllTickers";
    public static final String GET_ALL_WATCHLIST_DATA = BASE_URL + "WatchList/GetAllWatchListDetail";
    public static final String DELETE_WATCHLIST_DATA = BASE_URL + "WatchList/DeleteMasterWatchlist";
    public static final String PENDING_ORDERS_DATA = BASE_URL + "Order/PendingOrders";
    public static final String EXECUTED_ORDERS_DATA = BASE_URL + "Order/ExecutedOrders";
    public static final String EDIT_ORDERS_DATA = BASE_URL + "Order/GetOrderDetail";
    public static final String UPDATE_ORDERS_DATA = BASE_URL + "Order/UpdateOrder";
    public static final String DELETE_ORDERS_DATA = BASE_URL + "Order/CancelOrder";
    public static final String SUBSCRIBE_USER = BASE_URL + "Register/SubscribeUser";
    public static final String GET_USER_PERSONAL_INFO = BASE_URL + "Register/GetUserByUserID";
    public static final String UPDATE_USER_PERSONAL_INFO = BASE_URL + "Register/AccountSetting";
    public static final String UPDATE_SUBSCRIPTION_INFO = BASE_URL + "Register/SubcriptionSetting";
    public static final String GET_APP_DATA = BASE_URL + "AppSetting/GetAllAppSetting";
    public static final String GET_CHART_DATA = BASE_URL + "Position/ReplayBars";
}
