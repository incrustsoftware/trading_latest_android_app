package com.myfixcost.trade.Trade_Login_Views;

public class Trade_Sub_Info_Array {

    String A_id, Key, Type, Values;

    public Trade_Sub_Info_Array(String a_id, String key, String type, String values) {
        A_id = a_id;
        Key = key;
        Type = type;
        Values = values;
    }

    public Trade_Sub_Info_Array() {

    }

    public String getA_id() {
        return A_id;
    }

    public void setA_id(String a_id) {
        A_id = a_id;
    }

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getValues() {
        return Values;
    }

    public void setValues(String values) {
        Values = values;
    }


}
