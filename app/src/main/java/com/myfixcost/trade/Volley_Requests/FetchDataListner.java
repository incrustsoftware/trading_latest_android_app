package com.myfixcost.trade.Volley_Requests;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public interface FetchDataListner {

    void onFetchComplete(JSONArray data);

    void onFetchComplete(JSONObject data) throws JSONException;

    void onFetchFailure(String msg);

    void onFetchStart();
}
