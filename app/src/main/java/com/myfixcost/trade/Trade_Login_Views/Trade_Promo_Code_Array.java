package com.myfixcost.trade.Trade_Login_Views;

public class Trade_Promo_Code_Array {

    public Trade_Promo_Code_Array(String id, String rateType, String calculatedamt, String subscription_type_id) {
        this.id = id;
        RateType = rateType;
        Calculatedamt = calculatedamt;
        Subscription_type_id = subscription_type_id;
    }

    public Trade_Promo_Code_Array() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRateType() {
        return RateType;
    }

    public void setRateType(String rateType) {
        RateType = rateType;
    }

    public String getCalculatedamt() {
        return Calculatedamt;
    }

    public void setCalculatedamt(String calculatedamt) {
        Calculatedamt = calculatedamt;
    }

    public String getSubscription_type_id() {
        return Subscription_type_id;
    }

    public void setSubscription_type_id(String subscription_type_id) {
        Subscription_type_id = subscription_type_id;
    }

    String id, RateType, Calculatedamt, Subscription_type_id;
}
