package com.myfixcost.trade.Constant;

import android.app.Application;

public class MyGlobalClass extends Application {

    private String accountType;
    String trading_url;
    String agreement_text;
    String ticker_name;
    String click_position;
    String ticker_quantity;
    String Guest_User;
    String rithmic_id = "";

    public String getRithmic_id() {
        return rithmic_id;
    }

    public void setRithmic_id(String rithmic_id) {
        this.rithmic_id = rithmic_id;
    }

    public String getGuest_User() {
        return Guest_User;
    }

    public void setGuest_User(String guest_User) {
        Guest_User = guest_User;
    }

    public String getClick_position() {
        return click_position;
    }

    public void setClick_position(String click_position) {
        this.click_position = click_position;
    }

    public String getTicker_quantity() {
        return ticker_quantity;
    }

    public void setTicker_quantity(String ticker_quantity) {
        this.ticker_quantity = ticker_quantity;
    }

    public String getTicker_name() {
        return ticker_name;
    }

    public void setTicker_name(String ticker_name) {
        this.ticker_name = ticker_name;
    }

    public String getAgreement_text() {
        return agreement_text;
    }

    public void setAgreement_text(String agreement_text) {
        this.agreement_text = agreement_text;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getTrading_url() {
        return trading_url;
    }

    public void setTrading_url(String trading_url) {
        this.trading_url = trading_url;
    }


}
