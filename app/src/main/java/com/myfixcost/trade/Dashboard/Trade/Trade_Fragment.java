package com.myfixcost.trade.Dashboard.Trade;

import static android.os.Build.VERSION_CODES.N;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.Constant.MyGlobalClass;
import com.myfixcost.trade.Dashboard.Position_Menu.Trade_Position_Array;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;
import com.myfixcost.trade.Trade_Login_Views.Trade_SIgn_Up_Activity;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class Trade_Fragment extends Fragment {

    TextView txt_buy, txt_sell, txt_one_day, txt_one_week, txt_one_month, txt_one_year, txt_five_year, txt_all;
    TextView txt_stock_name, txt_month_name, txt_stock_price, txt_ticker_desc, txt_ticker_quantity, txt_pl_open, txt_pl_today, txt_todays_high, txt_todays_low, txt_todays_change;
    Context context;
    ImageView img_create_watchlist;
    String ticker_name, Ticker, add_watchlist = "", watchlist_id, convertedDate;
    Spinner spinner_get_watchlist;
    ArrayList<Trade_get_watchlist_array> trade_get_watchlist_arrays = new ArrayList<>();
    ArrayList<String> spinnerSubArray = new ArrayList<>();
    ArrayAdapter<String> adapter;
    Dialog dialog;
    LinearLayout lin_create_watchlist;
    MySharedPreference mySharedPreference;
    LineChart img_chart;
    DecimalFormat decimalFormat;
    String Url = "",bar_type = "day";
    boolean rithmic_status = false;
    boolean refreshtradeValue = false;
    ArrayList<Entry> chartArrayList;
    LineDataSet lineDataSet;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mySharedPreference = new MySharedPreference(getContext());
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.trade_fragment, container, false);

        init(root);


        txt_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshtradeValue = false;
                if (((MyGlobalClass) getContext().getApplicationContext()).getGuest_User().equalsIgnoreCase("1")) {
                    GuestUser_Popup alert = new GuestUser_Popup();
                    alert.guestDialog(getActivity());
                } else {

                    if (((MyGlobalClass) getContext().getApplicationContext()).getRithmic_id().equalsIgnoreCase("0")) {
                        Rithmic_Popup alert = new Rithmic_Popup();
                        alert.rithmicDialog(getActivity());
                    } else {
                        Intent buy = new Intent(getContext(), Trade_Sell_Activity.class);
                        buy.putExtra("status", "buy");
                        buy.putExtra("ticker_name", Ticker);
                        getActivity().startActivity(buy);
                    }

                }
            }
        });

        txt_sell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                refreshtradeValue = false;
                if (((MyGlobalClass) getContext().getApplicationContext()).getGuest_User().equalsIgnoreCase("1")) {
                    GuestUser_Popup alert = new GuestUser_Popup();
                    alert.guestDialog(getActivity());
                } else {

                    if (((MyGlobalClass) getContext().getApplicationContext()).getRithmic_id().equalsIgnoreCase("0")) {
                        Rithmic_Popup alert = new Rithmic_Popup();
                        alert.rithmicDialog(getActivity());
                    } else {
                        Intent buy = new Intent(getContext(), Trade_Sell_Activity.class);
                        buy.putExtra("status", "sell");
                        buy.putExtra("ticker_name", Ticker);
                        getActivity().startActivity(buy);
                    }

                }


            }
        });

        img_create_watchlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshtradeValue = false;
                ViewDialog alert = new ViewDialog();
                alert.showDialog(getActivity());
                GetWatchlistData();
            }
        });

        txt_one_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar_type = "day";
                txt_one_day.setBackground(getResources().getDrawable(R.drawable.background_email_login));
                txt_one_day.setTextColor(getResources().getColor(R.color.white));
                txt_one_week.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));
                txt_one_month.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));
                txt_one_year.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));
                txt_five_year.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));
                txt_all.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));

                txt_one_week.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_month.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_year.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_five_year.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_all.setTextColor(getResources().getColor(R.color.login_text_color));

                GetLineChartData();
            }
        });

        txt_one_week.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar_type = "week";
                txt_one_week.setBackground(getResources().getDrawable(R.drawable.background_email_login));
                txt_one_week.setTextColor(getResources().getColor(R.color.white));
                txt_one_day.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));
                txt_one_month.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));
                txt_one_year.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));
                txt_five_year.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));
                txt_all.setBackground(getResources().getDrawable(R.drawable.background_graph_buttons));

                txt_one_day.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_month.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_year.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_five_year.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_all.setTextColor(getResources().getColor(R.color.login_text_color));

                GetLineChartData();
            }
        });

        txt_one_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar_type = "month";
                txt_one_month.setBackground(getResources().getDrawable(R.drawable.background_email_login));
                txt_one_month.setTextColor(getResources().getColor(R.color.white));
                txt_one_day.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_week.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_year.setBackgroundColor(getResources().getColor(R.color.white));
                txt_five_year.setBackgroundColor(getResources().getColor(R.color.white));
                txt_all.setBackgroundColor(getResources().getColor(R.color.white));

                txt_one_day.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_week.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_year.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_five_year.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_all.setTextColor(getResources().getColor(R.color.login_text_color));

                GetLineChartData();
            }
        });

        txt_one_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar_type = "year";
                txt_one_year.setBackground(getResources().getDrawable(R.drawable.background_email_login));
                txt_one_year.setTextColor(getResources().getColor(R.color.white));
                txt_one_day.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_week.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_month.setBackgroundColor(getResources().getColor(R.color.white));
                txt_five_year.setBackgroundColor(getResources().getColor(R.color.white));
                txt_all.setBackgroundColor(getResources().getColor(R.color.white));

                txt_one_day.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_week.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_month.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_five_year.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_all.setTextColor(getResources().getColor(R.color.login_text_color));

                GetLineChartData();
            }
        });

        txt_five_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar_type = "fiveyear";
                txt_five_year.setBackground(getResources().getDrawable(R.drawable.background_email_login));
                txt_five_year.setTextColor(getResources().getColor(R.color.white));
                txt_one_day.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_week.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_month.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_year.setBackgroundColor(getResources().getColor(R.color.white));
                txt_all.setBackgroundColor(getResources().getColor(R.color.white));

                txt_one_day.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_week.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_month.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_year.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_all.setTextColor(getResources().getColor(R.color.login_text_color));

                GetLineChartData();
            }
        });

        txt_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bar_type = "all";
                txt_all.setBackground(getResources().getDrawable(R.drawable.background_email_login));
                txt_all.setTextColor(getResources().getColor(R.color.white));
                txt_one_day.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_week.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_month.setBackgroundColor(getResources().getColor(R.color.white));
                txt_one_year.setBackgroundColor(getResources().getColor(R.color.white));
                txt_five_year.setBackgroundColor(getResources().getColor(R.color.white));

                txt_one_day.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_week.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_month.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_one_year.setTextColor(getResources().getColor(R.color.login_text_color));
                txt_five_year.setTextColor(getResources().getColor(R.color.login_text_color));

                GetLineChartData();
            }
        });

        return root;
    }

    // initialize UI...
    private void init(View root) {
        txt_sell = root.findViewById(R.id.txt_sell);
        txt_buy = root.findViewById(R.id.txt_buy);
        img_create_watchlist = root.findViewById(R.id.img_create_watchlist);

        txt_stock_name = root.findViewById(R.id.txt_stock_name);
        txt_month_name = root.findViewById(R.id.txt_month_name);
        txt_stock_price = root.findViewById(R.id.txt_stock_price);
        txt_ticker_desc = root.findViewById(R.id.txt_ticker_desc);
        txt_ticker_quantity = root.findViewById(R.id.txt_ticker_quantity);
        txt_pl_open = root.findViewById(R.id.txt_pl_open);
        txt_pl_today = root.findViewById(R.id.txt_pl_today);
        txt_todays_high = root.findViewById(R.id.txt_todays_high);
        txt_todays_low = root.findViewById(R.id.txt_todays_low);
        txt_todays_change = root.findViewById(R.id.txt_todays_change);

        txt_one_day = root.findViewById(R.id.txt_one_day);
        txt_one_week = root.findViewById(R.id.txt_one_week);
        txt_one_month = root.findViewById(R.id.txt_one_month);
        txt_one_year = root.findViewById(R.id.txt_one_year);
        txt_five_year = root.findViewById(R.id.txt_five_year);
        txt_all = root.findViewById(R.id.txt_all);
        img_chart = root.findViewById(R.id.img_chart);

//        if (((MyGlobalClass) getContext().getApplicationContext()).getGuest_User().equalsIgnoreCase("1")) {
//            GuestUser_Popup alert = new GuestUser_Popup();
//            alert.guestDialog(getActivity());
//        } else {
//
//            if (((MyGlobalClass) getContext().getApplicationContext()).getRithmic_id().equalsIgnoreCase("0")) {
//                Rithmic_Popup alert = new Rithmic_Popup();
//                alert.rithmicDialog(getActivity());
//            } else {
//                if (((MyGlobalClass) getContext().getApplicationContext()).getClick_position().equalsIgnoreCase("1")) {
//                    Url = Constant.TRADE_PAGE;
//                    GetTradeData();
//                } else if (((MyGlobalClass) getContext().getApplicationContext()).getClick_position().equalsIgnoreCase("2")) {
//                    Url = Constant.WATCHLIST_CLICK_TRADE_PAGE;
//                    GetTradeData();
//                } else {
//                    Click_Popup alert = new Click_Popup();
//                    alert.showDialog(getActivity());
//                }
//            }
//
//        }

        if (((MyGlobalClass) getContext().getApplicationContext()).getClick_position().equalsIgnoreCase("1")) {
            Url = Constant.TRADE_PAGE;
            GetTradeData();
        } else if (((MyGlobalClass) getContext().getApplicationContext()).getClick_position().equalsIgnoreCase("2")) {
            Url = Constant.WATCHLIST_CLICK_TRADE_PAGE;
            GetTradeData();
        } else {
            Click_Popup alert = new Click_Popup();
            alert.showDialog(getActivity());
        }

        GetLineChartData();

    }

    // method to open dialog of add watchlist...
    public class ViewDialog {

        public void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_add_watchlist_popup);

            AppCompatButton btn_create_new = dialog.findViewById(R.id.btn_create_new);
            AppCompatButton btn_save = dialog.findViewById(R.id.btn_save);
            AppCompatButton btn_save_watchlist = dialog.findViewById(R.id.btn_save_watchlist);
            AppCompatButton btn_cancel = dialog.findViewById(R.id.btn_cancel);
            ImageView img_close = dialog.findViewById(R.id.img_close);
            lin_create_watchlist = dialog.findViewById(R.id.lin_create_watchlist);
            spinner_get_watchlist = dialog.findViewById(R.id.spinner_get_watchlist);
            EditText ed_create_watchlist = dialog.findViewById(R.id.ed_create_watchlist);

            // add watchlist method...
            ed_create_watchlist.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                    } else {
                        add_watchlist = s.toString();
                    }
                }
            });


            btn_save.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SaveWatchlistData();
                    refreshtradeValue = true;
                }
            });

            btn_save_watchlist.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (add_watchlist.equalsIgnoreCase("")) {
                        ed_create_watchlist.setError("Add Watchlist Name");
                    } else {
                        ed_create_watchlist.getText().clear();
                        AddWatchlistData();
                    }

                }
            });

            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    refreshtradeValue = true;
                }
            });

            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ed_create_watchlist.getText().clear();
                    lin_create_watchlist.setVisibility(View.GONE);
                }
            });

            btn_create_new.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lin_create_watchlist.setVisibility(View.VISIBLE);
                    ed_create_watchlist.setError(null);
                }
            });

            getWatchlistD();

            dialog.show();

        }
    }

    // method to open dialog of trade alert...
    public class Click_Popup {

        public void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_click_popup);

            AppCompatButton btn_confirm = dialog.findViewById(R.id.btn_confirm);

            btn_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BottomNavigationView) getActivity().findViewById(R.id.navigation)).setSelectedItemId(R.id.navigation_watchlist);
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    // method to open rithmic id missing dialog...
    public class Rithmic_Popup {

        public void rithmicDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_rithmic_popup);

            AppCompatButton btn_confirm = dialog.findViewById(R.id.btn_confirm);
            ImageView img_close = dialog.findViewById(R.id.img_close);

            btn_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((BottomNavigationView) getActivity().findViewById(R.id.navigation)).setSelectedItemId(R.id.navigation_settings);
                    dialog.dismiss();
                }
            });

            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    // method to open guest user popup...
    public class GuestUser_Popup {

        public void guestDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_guest_user_popup);

            AppCompatButton btn_confirm = dialog.findViewById(R.id.btn_confirm);
            ImageView img_close = dialog.findViewById(R.id.img_close);

            btn_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getContext(), Trade_SIgn_Up_Activity.class));
                    requireActivity().finish();
                    dialog.dismiss();
                }
            });

            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    // method to get trade data by ticker name and user id...
    private void GetTradeData() {


        System.out.println("GetTradeData url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        String ticker_name = ((MyGlobalClass) getContext().getApplicationContext()).getTicker_name();


        postParams.put("userid", mySharedPreference.getisCustomerId());
        postParams.put("ticker_name", ticker_name);
        postParams.put("accountType", ((MyGlobalClass) getContext().getApplicationContext()).getAccountType());
        postParams.put("bQuantity", ((MyGlobalClass) getContext().getApplicationContext()).getTicker_quantity());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons1, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons1 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(context, data.getString("message"), Toast.LENGTH_SHORT).show();


                JSONObject new_data = data.getJSONObject("data");

                Ticker = new_data.getString("Ticker");
                String ContractMonth = new_data.getString("ContractMonth");
                String MarketPrice = new_data.getString("MarketPrice");
                String PLOpen = new_data.getString("PLOpen");
                String PLToday = new_data.getString("PLToday");
                String TodayHigh = new_data.getString("TodayHigh");
                String TodayLow = new_data.getString("TodayLow");
                String Quantity = new_data.getString("Quantity");
                String AvailableQty = new_data.getString("AvailableQty");
                String TickerDesc = new_data.getString("TickerDesc");
                String TodayChange = new_data.getString("TodayChange");

                if (PLOpen.startsWith("-")) {
                    String pl_open_total = PLOpen.replace("-", "");
                    txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                    Double d_pl_open = Double.valueOf(pl_open_total);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_open.setText("$" + "(" + decimalFormat.format((d_pl_open)) + ")");
                } else {
                    txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                    Double d_pl_open = Double.valueOf(PLOpen);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_open.setText("$" + decimalFormat.format(d_pl_open));
                }

                if (PLToday.startsWith("-")) {
                    PLToday = PLToday.replace("-", "");
                    txt_pl_today.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                    Double d_pl_today = Double.valueOf(PLToday);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_today.setText("$" + "(" + decimalFormat.format((d_pl_today)) + ")");
                } else {
                    Double d_pl_today = Double.valueOf(PLToday);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_today.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                    txt_pl_today.setText("$" + decimalFormat.format(d_pl_today));
                }

                txt_stock_name.setText(Ticker);
                //convertedDate = mySharedPreference.dateFormatter(context, "",ContractMonth);
                txt_month_name.setText(ContractMonth);
                txt_stock_price.setText("$" + MarketPrice);
                txt_ticker_desc.setText(TickerDesc);
                txt_ticker_quantity.setText(Quantity);
                txt_todays_high.setText("$" + TodayHigh);
                txt_todays_low.setText("$" + TodayLow);


                if (TodayChange.startsWith("-")) {
                    TodayChange = TodayChange.replace("-", "");
                    txt_todays_change.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                    Double Today_Change = Double.valueOf(TodayChange);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_todays_change.setText("$" + "(" + decimalFormat.format((Today_Change)) + ")");
                } else {
                    Double Today_Change = Double.valueOf(TodayChange);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_todays_change.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                    txt_todays_change.setText("$" + decimalFormat.format(Today_Change));
                }
                RequestQueueService.cancelProgressDialog();
                refreshtradeValue = true;
                refreshAllContent(5000);
            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    // method to add watchlist data...
    public void AddWatchlistData() {

        String Url = Constant.ADD_WATCHLIST;
        System.out.println("AddWatchlistData url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("user_id", mySharedPreference.getisCustomerId());
        postParams.put("watchName", add_watchlist);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons5, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons5 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                lin_create_watchlist.setVisibility(View.GONE);
//                dialog.dismiss();
//                JSONObject new_data = data.getJSONObject("data");
                GetWatchlistData();

            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    // method to get watchlist data...
    private void GetWatchlistData() {

        String Url = Constant.GET_WATCHLIST;
        System.out.println("GetWatchlistData url : " + Url);
        trade_get_watchlist_arrays.clear();

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("user_id", mySharedPreference.getisCustomerId());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons4, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons4 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                JSONArray service = data.getJSONArray("data");

                if (service.length() > 0 && service != null) {
                    for (int i = 0; i < service.length(); i++) {
                        JSONObject history = service.getJSONObject(i);
                        Trade_get_watchlist_array getService = new Trade_get_watchlist_array();

                        getService.setM_id(history.getString("M_id"));
                        getService.setUser_id(history.getString("User_id"));
                        getService.setWatchName(history.getString("WatchName"));

                        trade_get_watchlist_arrays.add(getService);
                    }
                    spinnerSubArray.clear();
                    for (int i = 0; i < trade_get_watchlist_arrays.size(); i++) {
                        String c_name = trade_get_watchlist_arrays.get(i).getWatchName();
                        System.out.println("c_names :" + c_name);

                        spinnerSubArray.add(c_name);
                    }

                    System.out.println("country array :" + spinnerSubArray.size());
                    adapter = new ArrayAdapter<String>(
                            getContext(),
                            R.layout.trade_subscription_type_view,
                            spinnerSubArray
                    );

                    adapter.setDropDownViewResource(R.layout.trade_subscription_type_view);
                    spinner_get_watchlist.setAdapter(adapter);
                } else {
                    Toast.makeText(getContext(), "No Watchlist Present", Toast.LENGTH_SHORT).show();

                }

            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };


    // method to save watchlist data...
    public void SaveWatchlistData() {

        String Url = Constant.SAVE_WATCHLIST;
        System.out.println("SaveWatchlistData url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("ticker", Ticker);
        postParams.put("user_id", mySharedPreference.getisCustomerId());
        postParams.put("watch_list_id", watchlist_id);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons6, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons6 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                dialog.dismiss();
//                JSONObject new_data = data.getJSONObject("data");

            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    // method to get selected watchlist name from dropdown...
    public void getWatchlistD() {
        spinner_get_watchlist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String watchlist_name = parent.getItemAtPosition(position).toString();


                System.out.println("watchlist_id :" + watchlist_name);
                for (int i = 0; i < trade_get_watchlist_arrays.size(); i++) {
                    if (watchlist_name.equalsIgnoreCase(trade_get_watchlist_arrays.get(i).getWatchName())) {
                        watchlist_id = trade_get_watchlist_arrays.get(i).getM_id();
                        System.out.println("watchlist_id :" + watchlist_id);

                        break;
                    }
                }


            } // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    // method to get line chart data...
    public void GetLineChartData() {

        String Url = Constant.GET_CHART_DATA;
        System.out.println("GetLineChartData url : " + Url);
        chartArrayList = new ArrayList<>();
        chartArrayList.clear();
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("barType", bar_type);
        postParams.put("ticker", ((MyGlobalClass) getContext().getApplicationContext()).getTicker_name());
        postParams.put("userid", mySharedPreference.getisCustomerId());
        postParams.put("accountType", ((MyGlobalClass) getContext().getApplicationContext()).getAccountType());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons10, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons10 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                //Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                JSONObject new_data = data.getJSONObject("data");
                JSONArray graph = new_data.getJSONArray("Graph");
                if (graph.length() != 0) {
                    for (int i = 0; i < graph.length(); i++) {
                        JSONObject history = graph.getJSONObject(i);
                        Trade_Chart_Data_Array getService = new Trade_Chart_Data_Array();

                        getService.setDate(history.getString("Date"));
                        getService.setNew_price(Float.parseFloat(history.getString("Price")));


                        chartArrayList.add(new Entry(i, Float.parseFloat(history.getString("Price")), i));
                    }
                    initLineChartDownFill();
                } else {
                    Toast.makeText(context, "No Graph Data Available", Toast.LENGTH_SHORT).show();
                }

            } else {
                Toast.makeText(context, "No Graph Data Available", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(context, "No Graph Data Available", Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    private void initLineChartDownFill() {


        img_chart.setTouchEnabled(false);
        img_chart.setDragEnabled(true);
        img_chart.setScaleEnabled(false);
        img_chart.setPinchZoom(false);
        img_chart.setDrawGridBackground(false);
        img_chart.setMaxHighlightDistance(200);
        img_chart.getAxisLeft().setDrawGridLines(false);
        img_chart.getXAxis().setDrawGridLines(false);
        img_chart.getXAxis().setEnabled(false);
        img_chart.getXAxis().setDrawAxisLine(false);
        img_chart.getAxisRight().setDrawGridLines(false);
        img_chart.fitScreen();
        lineChartDownFillWithData();

    }

    // method for line chart...
    private void lineChartDownFillWithData() {

        Description description = new Description();
        description.setText("Trade Data");

        img_chart.setDescription(description);

        //LineDataSet is the line on the graph
        lineDataSet = new LineDataSet(chartArrayList, "This is y bill");
        lineDataSet.setLineWidth(1f);
        lineDataSet.setColor(Color.BLUE);
        lineDataSet.setCircleHoleColor(Color.BLUE);
        lineDataSet.setCircleColor(R.color.app_color);
        lineDataSet.setHighLightColor(Color.RED);
        lineDataSet.setDrawValues(true);
        lineDataSet.setCircleRadius(10f);
        lineDataSet.setCircleColor(Color.BLUE);

        XAxis xAxis = img_chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTH_SIDED);
        xAxis.setGranularity(0.5f);
        xAxis.setLabelCount(N, true);

        YAxis leftAxis = img_chart.getAxisLeft();

        //to make the smooth line as the graph is adrapt change so smooth curve
        lineDataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        //to enable the cubic density : if 1 then it will be sharp curve
        lineDataSet.setCubicIntensity(0.1f);

        //to fill the below of smooth line in graph
        lineDataSet.setDrawFilled(true);
        lineDataSet.setFillColor(ContextCompat.getColor(context, R.color.graph_line_color));
        //set the transparency
        lineDataSet.setFillAlpha(80);

        //set the gradiant then the above draw fill color will be replace
        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.gradient_graph);
        lineDataSet.setFillDrawable(drawable);

        //set legend disable or enable to hide {the left down corner name of graph}
        Legend legend = img_chart.getLegend();
        legend.setEnabled(false);

        //to remove the cricle from the graph
        lineDataSet.setDrawCircles(true);

        //lineDataSet.setColor(ColorTemplate.COLORFUL_COLORS);


        ArrayList<ILineDataSet> iLineDataSetArrayList = new ArrayList<>();
        iLineDataSetArrayList.add(lineDataSet);

        //LineData is the data accord
        LineData lineData = new LineData(iLineDataSetArrayList);
        lineData.setValueTextSize(10f);
        lineData.setValueTextColor(Color.BLACK);


        img_chart.setData(lineData);
        img_chart.invalidate();


    }

    private void GetTradeDataCycle() {


        System.out.println("GetTradeDataCycle url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        String ticker_name = ((MyGlobalClass) getContext().getApplicationContext()).getTicker_name();


        postParams.put("userid", mySharedPreference.getisCustomerId());
        postParams.put("ticker_name", ticker_name);
        postParams.put("accountType", ((MyGlobalClass) getContext().getApplicationContext()).getAccountType());
        postParams.put("bQuantity", ((MyGlobalClass) getContext().getApplicationContext()).getTicker_quantity());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons8, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons8 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                //Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                if (refreshtradeValue) {
                    refreshAllContent(5000);
                } else {
                    refreshtradeValue = false;
                }
                JSONObject new_data = data.getJSONObject("data");

                Ticker = new_data.getString("Ticker");
                String ContractMonth = new_data.getString("ContractMonth");
                String MarketPrice = new_data.getString("MarketPrice");
                String PLOpen = new_data.getString("PLOpen");
                String PLToday = new_data.getString("PLToday");
                String TodayHigh = new_data.getString("TodayHigh");
                String TodayLow = new_data.getString("TodayLow");
                String Quantity = new_data.getString("Quantity");
                String AvailableQty = new_data.getString("AvailableQty");
                String TickerDesc = new_data.getString("TickerDesc");
                String TodayChange = new_data.getString("TodayChange");

                if (PLOpen.startsWith("-")) {
                    String pl_open_total = PLOpen.replace("-", "");
                    txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                    Double d_pl_open = Double.valueOf(pl_open_total);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_open.setText("$" + "(" + decimalFormat.format((d_pl_open)) + ")");
                } else {
                    txt_pl_open.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                    Double d_pl_open = Double.valueOf(PLOpen);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_open.setText("$" + decimalFormat.format(d_pl_open));
                }

                if (PLToday.startsWith("-")) {
                    PLToday = PLToday.replace("-", "");
                    txt_pl_today.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                    Double d_pl_today = Double.valueOf(PLToday);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_today.setText("$" + "(" + decimalFormat.format((d_pl_today)) + ")");
                } else {
                    Double d_pl_today = Double.valueOf(PLToday);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_pl_today.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                    txt_pl_today.setText("$" + decimalFormat.format(d_pl_today));
                }

                txt_stock_name.setText(Ticker);
                //convertedDate = mySharedPreference.dateFormatter(context, "",ContractMonth);
                txt_month_name.setText(ContractMonth);
                txt_stock_price.setText("$" + MarketPrice);
                txt_ticker_desc.setText(TickerDesc);
                txt_ticker_quantity.setText(Quantity);
                txt_todays_high.setText("$" + TodayHigh);
                txt_todays_low.setText("$" + TodayLow);


                if (TodayChange.startsWith("-")) {
                    TodayChange = TodayChange.replace("-", "");
                    txt_todays_change.setTextColor(ContextCompat.getColor(context, R.color.color_order_red));
                    Double Today_Change = Double.valueOf(TodayChange);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_todays_change.setText("$" + "(" + decimalFormat.format((Today_Change)) + ")");
                } else {
                    Double Today_Change = Double.valueOf(TodayChange);
                    decimalFormat = mySharedPreference.NumberDecimalFormatter(getContext(), "");
                    txt_todays_change.setTextColor(ContextCompat.getColor(context, R.color.app_color));
                    txt_todays_change.setText("$" + decimalFormat.format(Today_Change));
                }

            } else {
                //Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            //Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
        }
    };

    public void refreshAllContent(final long timetoupdate) {
        new CountDownTimer(timetoupdate, 3000) {
            public void onTick(long millisUntilFinished) {
            }
            public void onFinish() {
                if (refreshtradeValue) {
                    System.out.println("UPDATE TRADE CONTENT HERE");
                    GetTradeDataCycle();
                }

            }
        }.start();
    }

    @Override
    public void onStop() {
        super.onStop();
        refreshtradeValue = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        refreshtradeValue = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        refreshtradeValue = false;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        refreshtradeValue = false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        refreshtradeValue = false;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        refreshtradeValue = false;
    }
}
