package com.myfixcost.trade.Dashboard.Watchlist_Menu;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.Constant.MyGlobalClass;
import com.myfixcost.trade.Dashboard.Trade.Trade_get_watchlist_array;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;
import com.myfixcost.trade.Trade_Login_Views.Trade_Promo_Code_Array;
import com.myfixcost.trade.Trade_Login_Views.Trade_SIgn_Up_Activity;
import com.myfixcost.trade.UIViews.RecyclerItemClickListener;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Trade_Watchlist_Fragment extends Fragment implements Trade_watchlist_click_interface {

    RecyclerView rec_watchlist_data, rec_show_watchlist;
    Trade_Show_Watchlist_Adapter trade_show_watchlist_adapter;
    Trade_Get_Watchlist_All_Data_Adapter trade_get_watchlist_all_data_adapter;
    public ArrayList<Trade_show_watchlist_array> trade_show_watchlist_arrays = new ArrayList<>();
    ArrayList<Trade_get_watchlist_array> trade_get_watchlist_arrays = new ArrayList<>();
    Trade_watchlist_click_interface trade_watchlist_click_interface;
    String watch_id = "", watchlist_name,Ticker_Name,Ticker_Id;
    Dialog dialog;
    MySharedPreference mySharedPreference;
    EditText ed_search_and_add;
    AutoCompleteTextView spinner_watchlist_tickers;
    ArrayList<Trade_Show_Ticker_Array> trade_show_ticker_arrays = new ArrayList<>();
    ArrayList<String> spinnerSubArray = new ArrayList<>();
    ArrayAdapter<String> adapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trade_watchlist_click_interface = this;
        mySharedPreference = new MySharedPreference(getContext());
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.trade_watchlist_fragment, container, false);

        initUI(root);
        getWatchlistTickerName();

        rec_watchlist_data.addOnItemTouchListener(new
                RecyclerItemClickListener(getContext(), rec_watchlist_data, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ((MyGlobalClass) getContext().getApplicationContext()).setTicker_name(trade_show_watchlist_arrays.get(position).getTicker());
                ((MyGlobalClass) getContext().getApplicationContext()).setTicker_quantity("0");
                ((BottomNavigationView) getActivity().findViewById(R.id.navigation)).setSelectedItemId(R.id.navigation_trade);
                ((MyGlobalClass) getContext().getApplicationContext()).setClick_position("2");
            }

            @Override
            public void onLongItemClick(View view, int position) {
            }
        }));

        return root;
    }

    private void initUI(View root) {
        rec_watchlist_data = root.findViewById(R.id.rec_watchlist_data);
        rec_show_watchlist = root.findViewById(R.id.rec_show_watchlist);
        ed_search_and_add = root.findViewById(R.id.ed_search_and_add);
        spinner_watchlist_tickers = root.findViewById(R.id.spinner_watchlist_tickers);
        GetWatchlistData();
        GetAllTicker();

        ed_search_and_add.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

    }

    public class ViewDialog {

        public void showDialog(Activity activity) {
            dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_watchlist_delete_popup);

            AppCompatButton btn_cancel = dialog.findViewById(R.id.btn_cancel);
            AppCompatButton btn_confirm = dialog.findViewById(R.id.btn_confirm);
            TextView txt_watchlist_name = dialog.findViewById(R.id.txt_watchlist_name);
            TextView txt_watch_name = dialog.findViewById(R.id.txt_watch_name);
            ImageView img_close = dialog.findViewById(R.id.img_close);

            txt_watch_name.setText("Are you sure, you want to delete " + watchlist_name);
            txt_watchlist_name.setText(watchlist_name);

            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            btn_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DeleteWatchlistData();
                }
            });

            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });


            dialog.show();
        }
    }

    private void GetWatchlistDataById() {

        String Url = Constant.GET_ALL_WATCHLIST_DATA;
        System.out.println("GetWatchlistDataById url : " + Url);
        trade_show_watchlist_arrays.clear();
        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("user_id", mySharedPreference.getisCustomerId());
        postParams.put("watch_list_id", watch_id);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons1, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons1 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();

                JSONArray service = data.getJSONArray("data");

                if (service.length() > 0 && service != null) {
                    for (int i = 0; i < service.length(); i++) {
                        JSONObject history = service.getJSONObject(i);
                        Trade_show_watchlist_array getService = new Trade_show_watchlist_array();

                        getService.setId(history.getString("Id"));
                        getService.setTicker(history.getString("Ticker"));
                        getService.setContractMonth(history.getString("ContractMonth"));
                        getService.setPriceChange(history.getString("PriceChange"));
                        getService.setTodayChange(history.getString("TodayChange"));
                        getService.setWatch_list_id(history.getString("Watch_list_id"));

                        trade_show_watchlist_arrays.add(getService);
                    }
                    trade_get_watchlist_all_data_adapter = new Trade_Get_Watchlist_All_Data_Adapter(trade_show_watchlist_arrays, getContext());
                    rec_watchlist_data.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    rec_watchlist_data.setAdapter(trade_get_watchlist_all_data_adapter);
                    trade_get_watchlist_all_data_adapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "No Data", Toast.LENGTH_SHORT).show();
                    trade_get_watchlist_all_data_adapter = new Trade_Get_Watchlist_All_Data_Adapter(trade_show_watchlist_arrays, getContext());
                    rec_watchlist_data.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
                    rec_watchlist_data.setAdapter(trade_get_watchlist_all_data_adapter);
                    trade_get_watchlist_all_data_adapter.notifyDataSetChanged();
                }


            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    private void GetWatchlistData() {

        String Url = Constant.GET_WATCHLIST;
        System.out.println("GetWatchlistData url : " + Url);
        trade_get_watchlist_arrays.clear();

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("user_id", mySharedPreference.getisCustomerId());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons4, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons4 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                JSONArray service = data.getJSONArray("data");

                for (int i = 0; i < service.length(); i++) {
                    JSONObject history = service.getJSONObject(i);
                    Trade_get_watchlist_array getService = new Trade_get_watchlist_array();

                    getService.setM_id(history.getString("M_id"));
                    getService.setUser_id(history.getString("User_id"));
                    getService.setWatchName(history.getString("WatchName"));

                    trade_get_watchlist_arrays.add(getService);
                }
                if (trade_get_watchlist_arrays.size() != 0) {

                    for (int i = 0; i < trade_get_watchlist_arrays.size(); i++) {
                        watch_id = trade_get_watchlist_arrays.get(0).getM_id();
                        break;
                    }
                    GetWatchlistDataById();
                } else {
                    watch_id = "1";
                    GetWatchlistDataById();
                }

                trade_show_watchlist_adapter = new Trade_Show_Watchlist_Adapter(trade_get_watchlist_arrays, trade_watchlist_click_interface, getContext());
                rec_show_watchlist.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
                rec_show_watchlist.setAdapter(trade_show_watchlist_adapter);
                trade_show_watchlist_adapter.notifyDataSetChanged();

            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };


    private void DeleteWatchlistData() {

        String Url = Constant.DELETE_WATCHLIST_DATA;
        System.out.println("DeleteWatchlistData url : " + Url);
        trade_get_watchlist_arrays.clear();

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("m_id", watch_id);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons6, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons6 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                JSONObject service = data.getJSONObject("data");
                dialog.dismiss();
                GetWatchlistData();

            } else {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    @Override
    public void getwatchlistdata(String watchlist_id, String watch_flag, String watch_name) {


        if (watch_flag.equalsIgnoreCase("0")) {
            watch_id = watchlist_id;
            trade_show_watchlist_arrays.clear();
            GetWatchlistDataById();
            System.out.println("watch_id :" + watch_id);
        } else {
            watch_id = watchlist_id;
            watchlist_name = watch_name;
            ViewDialog alert = new ViewDialog();
            alert.showDialog(getActivity());
        }
    }

    private void filter(String text) {
        //new array list that will hold the filtered data
        ArrayList<Trade_show_watchlist_array> temp = new ArrayList();


        //looping through existing elements
        for (Trade_show_watchlist_array s : trade_show_watchlist_arrays) {
            //if the existing elements contains the search input
            if (s.getTicker().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                temp.add(s);
            }
            trade_get_watchlist_all_data_adapter.filterList(temp);
        }
    }

    // method to get subscription type list...
    private void GetAllTicker() {
        String Url = Constant.GET_WATCHLIST_TICKERS;
        System.out.println("GetAllTicker url : " + Url);
        trade_show_ticker_arrays.clear();
        try {
            new PostApiRequest().get_request_Object(getContext(), subscriptionType, Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner subscriptionType = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getActivity(), data.getString("message"), Toast.LENGTH_SHORT).show();

                JSONArray service = data.getJSONArray("data");
                for (int i = 0; i < service.length(); i++) {

                    JSONObject history = service.getJSONObject(i);
                    Trade_Show_Ticker_Array getService = new Trade_Show_Ticker_Array();

                    getService.setID(history.getString("ID"));
                    getService.setName(history.getString("Name"));

                    trade_show_ticker_arrays.add(getService);
                }
                spinnerSubArray.clear();
                for (int i = 0; i < trade_show_ticker_arrays.size(); i++) {
                    String c_name = trade_show_ticker_arrays.get(i).getName();
                    System.out.println("c_names :" + c_name);

                    spinnerSubArray.add(c_name);
                }

                System.out.println("country array :" + spinnerSubArray.size());
                adapter = new ArrayAdapter<String>(
                        getActivity(),
//                        R.layout.trade_subscription_type_view,
                        android.R.layout.simple_list_item_1,
                        spinnerSubArray
                );

//                adapter.setDropDownViewResource(R.layout.trade_subscription_type_view);
                adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
                spinner_watchlist_tickers.setAdapter(adapter);

            } else {
                Toast.makeText(getActivity(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            //RequestQueueService.showProgressDialog(Activity_Full_Information.this);
        }
    };

    public void getWatchlistTickerName() {
        spinner_watchlist_tickers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Ticker_Name = parent.getItemAtPosition(position).toString();

                System.out.println("watchlist_id :" + watchlist_name);
                SaveWatchlistData();
            }
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    // method to save watchlist data...
    public void SaveWatchlistData() {

        String Url = Constant.SAVE_WATCHLIST;
        System.out.println("SaveWatchlistData url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("ticker", Ticker_Name);
        postParams.put("user_id", mySharedPreference.getisCustomerId());
        postParams.put("watch_list_id", watch_id);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons9, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons9 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
                RequestQueueService.cancelProgressDialog();
                spinner_watchlist_tickers.getText().clear();
                GetWatchlistData();

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };
}
