package com.myfixcost.trade.Dashboard.Orders_Menu.Executed_Orders;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;

import java.util.ArrayList;

public class Trade_Executed_Orders_Adapter extends RecyclerView.Adapter<Trade_Executed_Orders_Adapter.ViewHolder> {
    public ArrayList<Trade_Executed_Order_Array> trade_executedOrderArrays = new ArrayList<>();
    Context context;
    MySharedPreference mySharedPreference;
    String convertedDate;

    public Trade_Executed_Orders_Adapter(ArrayList<Trade_Executed_Order_Array> trade_executed_order_arrays, Context context) {
        this.trade_executedOrderArrays = trade_executed_order_arrays;
        this.context = context;

    }

    @Override
    public Trade_Executed_Orders_Adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.trade_executed_order_adapter, parent, false);
        Trade_Executed_Orders_Adapter.ViewHolder viewHolder = new Trade_Executed_Orders_Adapter.ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Trade_Executed_Orders_Adapter.ViewHolder holder, int position) {
        final Trade_Executed_Order_Array myListData = trade_executedOrderArrays.get(position);

        mySharedPreference = new MySharedPreference(context);

        holder.txt_ticker_name.setText(myListData.getTicker());
        holder.txt_order_type.setText(myListData.getOrderType());
        holder.txt_contract_month.setText(myListData.getContractMonth());
        holder.txt_price.setText("$"+myListData.getPrice());
        holder.txt_quantity.setText(myListData.getQuantity());

        //convertedDate = mySharedPreference.dateFormatter(context, "execute", myListData.getOrderdATE());

        holder.txt_order_date.setText(myListData.getOrderdATE());


//        if (lastSelectedPosition != -1 && lastSelectedPosition == position) {
//            holder.img_tick.setVisibility(View.VISIBLE);
//            holder.txt_doc.setTextColor(ContextCompat.getColor(context, R.color.app_background_color));
//            final Typeface typeface = ResourcesCompat.getFont(context, R.font.opensans_semibold);
//            holder.txt_doc.setTypeface(typeface);
//
//            state = false;
//        }else{
//            holder.img_tick.setVisibility(View.GONE);
//            holder.txt_doc.setTextColor(ContextCompat.getColor(context, R.color.bottomsheet_doc_color));
//            final Typeface typeface = ResourcesCompat.getFont(context, R.font.opensans_regular);
//            holder.txt_doc.setTypeface(typeface);
//            state = true;
//
//        }


    }

    @Override
    public int getItemCount() {
        return trade_executedOrderArrays.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txt_ticker_name, txt_contract_month, txt_order_type, txt_order_date, txt_price, txt_quantity;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txt_ticker_name = itemView.findViewById(R.id.txt_ticker_name);
            this.txt_order_type = itemView.findViewById(R.id.txt_order_type);
            this.txt_contract_month = itemView.findViewById(R.id.txt_contract_month);
            this.txt_price = itemView.findViewById(R.id.txt_price);
            this.txt_quantity = itemView.findViewById(R.id.txt_quantity);
            this.txt_order_date = itemView.findViewById(R.id.txt_order_date);

        }
    }
}
