package com.myfixcost.trade.Trade_Login_Views;

import static androidx.biometric.BiometricManager.Authenticators.BIOMETRIC_STRONG;
import static androidx.biometric.BiometricManager.Authenticators.DEVICE_CREDENTIAL;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricManager;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.Constant.MyGlobalClass;
import com.myfixcost.trade.Dashboard.Position_Menu.Trade_Position_Fragment;
import com.myfixcost.trade.Dashboard.Trade_Dashboard_Activity;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;

public class Trade_Login_Activity extends AppCompatActivity {

    Button btn_create_acc, btn_login, btn_continue_as_guest;
    EditText ed_email_id, ed_password;
    RadioButton rd_live_trading, rd_paper_money;
    ImageView img_biometric;
    LinearLayout lin_forgot_pass;
    String email_data, password_data;
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,100}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    private boolean is8char = false, hasUpper = false, hasnum = false, hasSpecialSymbol = false;
    boolean isEmail = false, isPassword = false;
    boolean is_Trading = true;
    MySharedPreference mySharedPreference;
    KeyguardManager mgr;
    private androidx.biometric.BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private Executor executor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trade_login_activity);

        mySharedPreference = new MySharedPreference(this);

        initUI();

        ed_email_id.setText("");
        ed_password.setText("");

        ((MyGlobalClass) this.getApplicationContext()).setTicker_name("");
        ((MyGlobalClass) this.getApplicationContext()).setClick_position("");
        ((MyGlobalClass) getApplicationContext()).setGuest_User("0");

        if (!mySharedPreference.getisCustomerId().equalsIgnoreCase("") && !mySharedPreference.getisSwitchEnable().equalsIgnoreCase("0")) {
//            img_biometric.setVisibility(View.VISIBLE);
            if (secureDevice()) {
                authenticateDevice();
            } else {
                Toast.makeText(Trade_Login_Activity.this, "Your device has no lock...", Toast.LENGTH_SHORT).show();
            }
        } else {
//            img_biometric.setVisibility(View.GONE);
        }



        btn_create_acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Trade_Login_Activity.this, Trade_SIgn_Up_Activity.class));
            }
        });

        btn_continue_as_guest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent guest_user = new Intent(Trade_Login_Activity.this, Trade_Dashboard_Activity.class);
                startActivity(guest_user);
                ((MyGlobalClass) getApplicationContext()).setGuest_User("1");
                ((MyGlobalClass) getApplicationContext()).setRithmic_id("0");
                ((MyGlobalClass) getApplicationContext()).setClick_position("");
                mySharedPreference.setisCustomerId("127");
            }
        });

        lin_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Trade_Login_Activity.this, Trade_Recent_Password_Activity.class));
            }
        });

        rd_live_trading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rd_live_trading.setChecked(true);
                rd_paper_money.setChecked(false);
                is_Trading = true;
            }
        });

        rd_paper_money.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rd_live_trading.setChecked(false);
                rd_paper_money.setChecked(true);
                is_Trading = true;
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ed_email_id.getText().toString().equalsIgnoreCase("")) {
                    ed_email_id.setError("Enter Email Id");
                } else if (ed_password.getText().toString().equalsIgnoreCase("")) {
                    ed_password.setError("Enter Password");
                } else if (!matchEmail()) {
                    ed_email_id.setError("Invalid Email Id");
                } else if (!is8char || !hasnum || !hasUpper || !hasSpecialSymbol) {
                    ed_password.setError("Invalid Password");
                } else if (!is_Trading) {
                    Toast.makeText(Trade_Login_Activity.this, "Please select trading status", Toast.LENGTH_SHORT).show();
                } else {
                    // call api method...
                    PostLoginData();
                }
            }
        });

//        img_biometric.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (secureDevice()) {
//                    authenticateDevice();
//                } else {
//                    Toast.makeText(Trade_Login_Activity.this, "Your device has no lock...", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }

    // initialize UI...
    private void initUI() {
        btn_create_acc = findViewById(R.id.btn_create_acc);
        btn_login = findViewById(R.id.btn_login);
        btn_continue_as_guest = findViewById(R.id.btn_continue_as_guest);
        ed_email_id = findViewById(R.id.ed_email_id);
        ed_password = findViewById(R.id.ed_password);
        rd_live_trading = findViewById(R.id.rd_live_trading);
        rd_paper_money = findViewById(R.id.rd_paper_money);
        img_biometric = findViewById(R.id.img_biometric);
        lin_forgot_pass = findViewById(R.id.lin_forgot_pass);

        editTextEmail(ed_email_id);
        editTextPassword(ed_password);
        mgr = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        executor = ContextCompat.getMainExecutor(this);


    }

    // get email method...
    public void editTextEmail(EditText edEmail) {
        edEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {

                    email_data = s.toString();


//                    isEmail = true;
//                    if (matchEmail() && isPassword && is8char && hasnum && hasUpper && hasSpecialSymbol) {
//                        btnLogin.setEnabled(true);
//                        btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_button));
//                    }
                }
            }
        });
    }

    // get password method...
    public void editTextPassword(EditText edPassword) {
        edPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {

                    password_data = s.toString();
                    passwordValidate();
//                    isEmail = true;
//                    if (matchEmail() && isPassword && is8char && hasnum && hasUpper && hasSpecialSymbol) {
//                        btnLogin.setEnabled(true);
//                        btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_button));
//                    }
                }
            }
        });
    }

    // validate email method...
    public boolean matchEmail() {
        boolean is_email = false;

        if (!EMAIL_ADDRESS_PATTERN.matcher(email_data.toString().trim()).matches()) {
            //Toast.makeText(this, "Invalid Email Id", Toast.LENGTH_SHORT).show();
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            is_email = false;
        } else {
            is_email = true;
        }
        return is_email;
    }

    public boolean secureDevice() {
        boolean is_lock = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) getApplicationContext().getSystemService(Context.FINGERPRINT_SERVICE);
            if (mgr.isDeviceSecure() && mgr.isKeyguardSecure()) {
                System.out.println();
                is_lock = true;
//            } else if (fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints()) {
            } else if (fingerprintManager != null && fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints()) {
                is_lock = true;
            } else {
                is_lock = false;
            }
        }
        return is_lock;
    }

    private void authenticateDevice() {
        biometricPrompt = new BiometricPrompt(this, executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode, @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
            }

            @Override
            public void onAuthenticationSucceeded(@NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);
                 Toast.makeText(Trade_Login_Activity.this, "Authenticate Successfull", Toast.LENGTH_SHORT).show();

                setLoginUsingBiometric();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
            }
        });

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            promptInfo = new BiometricPrompt.PromptInfo.Builder()
                    .setTitle("Fix My Cost window")
                    .setSubtitle("Allow Fix My Cost to enroll")
                    .setAllowedAuthenticators(BIOMETRIC_STRONG | DEVICE_CREDENTIAL)
                    .setConfirmationRequired(true)
                    .build();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            promptInfo = new BiometricPrompt.PromptInfo.Builder()
                    .setTitle("Fix My Cost window")
                    .setSubtitle("Allow Fix My Cost to enroll")
                    .setConfirmationRequired(true)

                    .setDeviceCredentialAllowed(true)
                    .build();
        }


        getBioEvent();
    }

    private void setLoginUsingBiometric() {
        if (!mySharedPreference.getisCustomerId().equalsIgnoreCase("")) {
            Intent bio_login = new Intent(Trade_Login_Activity.this,Trade_Dashboard_Activity.class);
            startActivity(bio_login);
            if (mySharedPreference.getisRithmicId().equalsIgnoreCase("1")) {
                ((MyGlobalClass) getApplicationContext()).setRithmic_id("1");
            } else {
                ((MyGlobalClass) getApplicationContext()).setRithmic_id("0");
            }
        } else {

        }
    }


    private void getBioEvent() {
        BiometricManager biometricManager = BiometricManager.from(this);
        switch (biometricManager.canAuthenticate(BIOMETRIC_STRONG | DEVICE_CREDENTIAL)) {
            case BiometricManager.BIOMETRIC_SUCCESS:
                System.out.println("bbb there is biometric...");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    FingerprintManager fingerprintManager = (FingerprintManager) getApplicationContext().getSystemService(Context.FINGERPRINT_SERVICE);
                    if (fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints()) {
                        System.out.println("bbb biometric enroll for fingerprint...");
                    } else {
                        System.out.println("bbb biometric enroll for other...");
                    }
                }
                break;
            case BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE:
                System.out.println("bbb No biometric features available on this device.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE:
                System.out.println("bbb Biometric features are currently unavailable.");
                break;
            case BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED:
                // Prompts the user to create credentials that your app accepts.
                System.out.println("bbb please enroll for biometric...");
                break;
        }
        biometricPrompt.authenticate(promptInfo);

    }

    // method is to check password validations...
    private void passwordValidate() {
        String password = ed_password.getText().toString();
        // 8 character
        if (ed_password.length() >= 8) {
            is8char = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            is8char = false;
        }
        //number
        if (password.matches("(.*[0-9].*)")) {
            hasnum = true;
        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasnum = false;

        }
        //upper case
        if (password.matches("(.*[A-Z].*)")) {
            hasUpper = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasUpper = false;

        }
        //symbol
        if (password.matches("^(?=.*[_.()$&@!]).*$")) {
            hasSpecialSymbol = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasSpecialSymbol = false;

        }
    }

    private void PostLoginData() {

        String Url = Constant.LOGIN_USER;
        System.out.println("login url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();

        postParams.put("email", email_data);
        postParams.put("password", password_data);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getApplicationContext(), getMasterResons1, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons1 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(Trade_Login_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                JSONObject info = data.getJSONObject("data");

                String user_id = info.getString("UserId");
                mySharedPreference.setisCustomerId(user_id);
                info.getString("Name");
                info.getString("Contact");
                info.getString("Email");
                info.getString("Address");
                info.getString("Username");
                info.getString("Password");
                info.getString("Otp");
                info.getString("IsOptVerified");
                info.getString("Sub_type");
                info.getString("Promo_code");
                String Rithimic_id = info.getString("Rithimic_id");
                info.getString("SubcriptionMaster_id");
                info.getString("SubscriptionType_id");
                ((MyGlobalClass) getApplicationContext()).setGuest_User("0");
                RequestQueueService.cancelProgressDialog();

                if (!Rithimic_id.equalsIgnoreCase("")) {
                    ((MyGlobalClass) getApplicationContext()).setRithmic_id("1");
                    mySharedPreference.setisRithmicId("1");
                } else {
                    ((MyGlobalClass) getApplicationContext()).setRithmic_id("0");
                    mySharedPreference.setisRithmicId("0");
                }

                Intent dash = new Intent(Trade_Login_Activity.this, Trade_Dashboard_Activity.class);
                startActivity(dash);

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_Login_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_Login_Activity.this);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
//        if (!mySharedPreference.getisCustomerId().equalsIgnoreCase("") && !mySharedPreference.getisSwitchEnable().equalsIgnoreCase("0")) {
//            img_biometric.setVisibility(View.VISIBLE);
//        } else {
//            img_biometric.setVisibility(View.GONE);
//        }

        ed_email_id.setText("");
        ed_password.setText("");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
}