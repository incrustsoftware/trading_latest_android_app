package com.myfixcost.trade.Dashboard.Settings_Menu;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.Constant.MyGlobalClass;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;
import com.myfixcost.trade.Trade_Login_Views.Trade_Promo_Code_Array;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Pattern;

public class Trade_Settings_Fragment extends Fragment {

    LinearLayout lin_acc_settings, lin_subscr_settings;
    RelativeLayout rel_src_subscription_settings;
    TextView txt_acc_settings, txt_subscr_settings, txt_resend_otp;
    ScrollView lin_acc_setting_ui;
    EditText ed_name, ed_email_id, ed_phone_no, ed_enter_otp, ed_password, ed_rithmic_id,ed_rithmic_pass;
    AppCompatButton btn_send_otp;
    Button btn_verify_otp, btn_save_changes;
    MySharedPreference mySharedPreference;
    private boolean is8char = false, hasUpper = false, hasnum = false, hasSpecialSymbol = false;
    EditText ed_credit_card_no, ed_valid_through, ed_cvv, ed_credit_name_on_card;
    Spinner spinner_country_code;
    boolean otp_confirmed;
    Spinner spinner_sub_type;
    ArrayAdapter<String> adapter;
    String subscriptionType_id = "", subcriptionMaster_id = "", rateType, c_code, subscription_amount,CountryCode,selectedCountryCode;
    ArrayList<Trade_Promo_Code_Array> trade_promo_code_arrays = new ArrayList<>();
    ArrayList<String> spinnerSubArray = new ArrayList<>();
    String is_click = "1";
    String email_data = "", name_data = "", enter_otp_data = "", password_data = "", confirm_password_data = "", phone_data = "",password_rithmic_data = "";
    String promocode_data = "", credit_card_no_data = "", credit_card_valid_through_data = "", credit_cvv_data = "", credit_card_name_data = "", rithmic_id_data = "";
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,100}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mySharedPreference = new MySharedPreference(getContext());
    }

    @Override
    public View onCreateView(
            @NonNull LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.trade_settings_fragment, container, false);
        initUI(root);

        lin_acc_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_click = "1";
                lin_acc_setting_ui.setVisibility(View.VISIBLE);
                rel_src_subscription_settings.setVisibility(View.GONE);
                lin_acc_settings.setBackgroundColor(getResources().getColor(R.color.app_color));
                lin_subscr_settings.setBackgroundColor(getResources().getColor(R.color.white));
                txt_acc_settings.setTextColor(getResources().getColor(R.color.white));
                txt_subscr_settings.setTextColor(getResources().getColor(R.color.color_news));
            }
        });

        lin_subscr_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_click = "2";
                lin_acc_setting_ui.setVisibility(View.GONE);
                rel_src_subscription_settings.setVisibility(View.VISIBLE);
                lin_acc_settings.setBackgroundColor(getResources().getColor(R.color.white));
                lin_subscr_settings.setBackgroundColor(getResources().getColor(R.color.app_color));
                txt_acc_settings.setTextColor(getResources().getColor(R.color.color_news));
                txt_subscr_settings.setTextColor(getResources().getColor(R.color.white));
            }
        });

        btn_send_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone_data.equalsIgnoreCase("")) {
                    ed_phone_no.setError("Enter Phone No");
                } else {
                    txt_resend_otp.setVisibility(View.VISIBLE);
                    sendOTP();
                }
            }
        });

        btn_verify_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!enter_otp_data.equalsIgnoreCase("")) {
                    verifyOTP();
                } else {
                    ed_enter_otp.setError("Enter OTP");
                }
            }
        });

        btn_save_changes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (is_click.equalsIgnoreCase("1")) {
                    if (!validateAllChecks()) {
                    } else {
                        UpdatePersonalInfoData();
                    }
                } else {
                    if (credit_card_no_data.equalsIgnoreCase("")) {
                        ed_credit_card_no.setError("Enter Card No");
                    } else if (credit_card_valid_through_data.equalsIgnoreCase("")) {
                        ed_valid_through.setError("Enter Card Validity");
                    } else if (credit_cvv_data.equalsIgnoreCase("")) {
                        ed_cvv.setError("Enter Cvv No");
                    } else {
                        UpdateSubscribeUser();
                    }


                }
            }
        });

        txt_resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendOTP();
            }
        });

        ed_valid_through.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    ed_valid_through.getText().clear();
                }
                return false;
            }
        });

        return root;
    }
    // method to initialize UI on page...
    private void initUI(View root) {
        lin_acc_settings = root.findViewById(R.id.lin_acc_settings);
        lin_subscr_settings = root.findViewById(R.id.lin_subscr_settings);
        txt_acc_settings = root.findViewById(R.id.txt_acc_settings);
        txt_subscr_settings = root.findViewById(R.id.txt_subscr_settings);
        lin_acc_setting_ui = root.findViewById(R.id.lin_acc_setting_ui);
        rel_src_subscription_settings = root.findViewById(R.id.rel_src_subscription_settings);
        txt_resend_otp = root.findViewById(R.id.txt_resend_otp);
        ed_name = root.findViewById(R.id.ed_name);
        ed_email_id = root.findViewById(R.id.ed_email_id);
        ed_phone_no = root.findViewById(R.id.ed_phone_no);
        ed_enter_otp = root.findViewById(R.id.ed_enter_otp);
        ed_password = root.findViewById(R.id.ed_password);
        ed_rithmic_pass = root.findViewById(R.id.ed_rithmic_pass);
        ed_rithmic_id = root.findViewById(R.id.ed_rithmic_id);
        btn_send_otp = root.findViewById(R.id.btn_send_otp);
        btn_verify_otp = root.findViewById(R.id.btn_verify_otp);
        btn_save_changes = root.findViewById(R.id.btn_save_changes);
        ed_credit_card_no = root.findViewById(R.id.ed_credit_card_no);
        ed_valid_through = root.findViewById(R.id.ed_valid_through);
        ed_cvv = root.findViewById(R.id.ed_cvv);
        spinner_country_code = root.findViewById(R.id.spinner_country_code);
        ed_credit_name_on_card = root.findViewById(R.id.ed_credit_name_on_card);
        spinner_sub_type = root.findViewById(R.id.spinner_sub_type);

        String[] arrayCountryCode = new String[] {
                "USA +1","IND +91"
        };

        adapter = new ArrayAdapter<String>(
                getContext(),
                R.layout.trade_country_code_view,
                arrayCountryCode
        );

        adapter.setDropDownViewResource(R.layout.trade_country_code_view);
        spinner_country_code.setAdapter(adapter);

        editTextEmail(ed_email_id);
        editTextName(ed_name);
        editPassword(ed_password);
        editrithmicPassword(ed_rithmic_pass);
        //editConfirmPassword(ed_confirm_password);
        editPhoneNo(ed_phone_no);
        editEnterOTP(ed_enter_otp);
        editrithmicID(ed_rithmic_id);
        //editPromoCode(ed_promo_code);
        editCreditCardValidThrough(ed_valid_through);
        editCreditCardCVV(ed_cvv);
        editCreditCardName(ed_credit_name_on_card);
        editCreditCardNo(ed_credit_card_no);

        GetPersonalInfoData();
        getCountryCode();
        GetSubscriptionType();
        getSubscriptionData();
    }

    // get email method...
    public void editTextEmail(EditText edEmail) {
        edEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    email_data = s.toString();
                }
            }
        });
    }

    // get name method...
    public void editTextName(EditText nameEmail) {
        nameEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    name_data = s.toString();
                }
            }
        });
    }

    // get enter OTP method...
    public void editEnterOTP(EditText otpEmail) {
        otpEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    enter_otp_data = "";
                    txt_resend_otp.setVisibility(View.GONE);
                    btn_verify_otp.setEnabled(false);
                    btn_verify_otp.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_send_otp));
                } else {
                    if (s.toString().length() == 6) {
                        btn_verify_otp.setEnabled(true);
                        txt_resend_otp.setVisibility(View.VISIBLE);
                        btn_verify_otp.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_email_login));
                        enter_otp_data = s.toString();
                    } else {
                        txt_resend_otp.setVisibility(View.GONE);
                        btn_verify_otp.setEnabled(false);
                        btn_verify_otp.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_send_otp));
                    }
                }
            }
        });
    }

    // get password method...
    public void editPassword(EditText passwordEmail) {
        passwordEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    password_data = s.toString();
                    passwordValidate(passwordEmail);
                }
            }
        });
    }

    // get rithmic password method...
    public void editrithmicPassword(EditText passwordRithmic) {
        passwordRithmic.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    password_rithmic_data = s.toString();
                    passwordValidate(passwordRithmic);
                }
            }
        });
    }

    // get phone no method...
    public void editPhoneNo(EditText phoneno) {
        phoneno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    btn_send_otp.setEnabled(false);
                    btn_send_otp.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_send_otp));
                    phone_data = "";
                    txt_resend_otp.setVisibility(View.GONE);
                } else {
                    if (s.toString().length() == 10) {
                        btn_send_otp.setEnabled(true);
                        btn_send_otp.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.background_email_login));
                        phone_data = s.toString();
                    } else {
                        txt_resend_otp.setVisibility(View.GONE);
                        btn_send_otp.setEnabled(false);
                        btn_send_otp.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_send_otp));
                    }
                }
            }
        });
    }

    // get confirm password method...
    public void editConfirmPassword(EditText confirmpasswordEmail) {
        confirmpasswordEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    confirm_password_data = s.toString();
                    passwordValidate(confirmpasswordEmail);
                }
            }
        });
    }

    // get promo code method...
    public void editPromoCode(EditText promocode) {
        promocode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    promocode_data = s.toString();
                }
            }
        });
    }

    // get credit card no method...
    public void editCreditCardNo(EditText ed_credit_card_no) {
        ed_credit_card_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    if (s.toString().length() == 16) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    }
                    credit_card_no_data = s.toString();
                }
            }
        });
    }

    // get credit card valid through method...
    public void editCreditCardValidThrough(EditText ed_valid_through) {
        ed_valid_through.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {

                    if (s.toString().length() == 2) {
                        s.append('/');
                    } else {
                        s.append("");
                    }
                    credit_card_valid_through_data = s.toString();
                }
            }
        });
    }

    // get credit card cvv method...
    public void editCreditCardCVV(EditText ed_credit_cvv) {
        ed_credit_cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    credit_cvv_data = s.toString();
                }
            }
        });
    }

    // get rithmic ID method...
    public void editrithmicID(EditText ed_rithmic_id) {
        ed_rithmic_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    rithmic_id_data = s.toString();
                }
            }
        });
    }

    // get credit card name method...
    public void editCreditCardName(EditText ed_credit_name_on_card) {
        ed_credit_name_on_card.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    credit_card_name_data = s.toString();
                }
            }
        });
    }

    // method is to check password validations...
    private void passwordValidate(EditText ed_password) {
        String password = ed_password.getText().toString();
        // 8 character
        if (ed_password.length() >= 8) {
            is8char = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            is8char = false;
        }
        //number
        if (password.matches("(.*[0-9].*)")) {
            hasnum = true;
        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasnum = false;

        }
        //upper case
        if (password.matches("(.*[A-Z].*)")) {
            hasUpper = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasUpper = false;

        }
        //symbol
        if (password.matches("^(?=.*[_.()$&@!]).*$")) {
            hasSpecialSymbol = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasSpecialSymbol = false;

        }
    }

    // method to post data of personal information on sign up page...
    private void UpdatePersonalInfoData() {
        String Url = Constant.UPDATE_USER_PERSONAL_INFO;
        System.out.println("UpdatePersonalInfoData url : " + Url);
        JSONObject jsonObject = new JSONObject();


        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("name", name_data);
        postParams.put("contact", phone_data);
        postParams.put("email", email_data);
        postParams.put("password", password_data);
        postParams.put("rithimic_id", rithmic_id_data);
        postParams.put("country_code", c_code);
        postParams.put("rithimic_Password", password_rithmic_data);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons1, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons1 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                JSONObject info = data.getJSONObject("data");
                String Rithimic_id = info.getString("Rithimic_id");

                if (!Rithimic_id.equalsIgnoreCase("")) {
                    ((MyGlobalClass) getContext().getApplicationContext()).setRithmic_id("1");
                    mySharedPreference.setisRithmicId("1");
                } else {
                    ((MyGlobalClass) getContext().getApplicationContext()).setRithmic_id("0");
                    mySharedPreference.setisRithmicId("0");
                }
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    private void GetPersonalInfoData() {
        String Url = Constant.GET_USER_PERSONAL_INFO;
        System.out.println("GetPersonalInfoData url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("userId", mySharedPreference.getisCustomerId());

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons2, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons2 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
//                img_second.setImageResource(R.drawable.ic_first_round);
//                lin_per_det.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.color.app_color));
//                src_personal_details.setVisibility(View.GONE);
//                src_subscription_info.setVisibility(View.VISIBLE);

                JSONObject info = data.getJSONObject("data");

                ed_name.setText(info.getString("Name"));
                ed_email_id.setText(info.getString("Email"));
                ed_phone_no.setText(info.getString("Contact"));
                ed_rithmic_id.setText(info.getString("Rithimic_id"));
                CountryCode = info.getString("Country_code");
                setCountryCode();

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    public void setCountryCode() {
        if (CountryCode.equalsIgnoreCase("1")) {
            selectedCountryCode = "USA +1";
            int spinnerPosition = adapter.getPosition(selectedCountryCode);
            spinner_country_code.setSelection(spinnerPosition);
        } else if (CountryCode.equalsIgnoreCase("91")) {
            selectedCountryCode = "IND +91";
            int spinnerPosition = adapter.getPosition(selectedCountryCode);
            spinner_country_code.setSelection(spinnerPosition);
        }
    }


    // method to send otp on mobile no...
    private void sendOTP() {
        String Url = Constant.SEND_OTP;
        System.out.println("sendOTP url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("contact", phone_data);
        postParams.put("email", email_data);
        postParams.put("country_code", c_code);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), getMasterResons, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    // method to verify OTP...
    private void verifyOTP() {
        String Url = Constant.VERIFY_OTP;
        System.out.println("verifyOTP url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("contact", phone_data);
        postParams.put("Otp", enter_otp_data);
        postParams.put("country_code", c_code);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), verifyotp, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner verifyotp = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                otp_confirmed = true;
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();

            } else {
                RequestQueueService.cancelProgressDialog();
                otp_confirmed = false;
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
            otp_confirmed = true;
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    public void getCountryCode() {
        spinner_country_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String country_code = parent.getItemAtPosition(position).toString();

                c_code = country_code.substring(country_code.lastIndexOf("+") + 1);
                System.out.println("country_code :" + c_code);

            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    // validate all checks of personal details...
    public boolean validateAllChecks() {
        boolean is_error = false;

        if (name_data.equalsIgnoreCase("")) {
            ed_name.setError("Enter Name");
            is_error = false;
        } else if (email_data.equalsIgnoreCase("")) {
            ed_email_id.setError("Invalid Email Id");
            is_error = false;
        } else if (phone_data.equalsIgnoreCase("")) {
            ed_phone_no.setError("Enter Phone No");
            is_error = false;
        } else if (enter_otp_data.equalsIgnoreCase("")) {
            ed_enter_otp.setError("Enter OTP");
            is_error = false;
        } else if (password_data.equalsIgnoreCase("")) {
            ed_password.setError("Password needs to be 8 char long  with minimum 1 capital letter, 1 number, 1 special character");
            is_error = false;
        } else if (!matchEmail()) {
            ed_email_id.setError("Invalid Email Id");
            is_error = false;
        } else if (!is8char || !hasnum || !hasUpper || !hasSpecialSymbol) {
            ed_password.setError("Invalid Password");
            is_error = false;
        } else if (!otp_confirmed) {
            ed_enter_otp.setError("OTP not confirmed");
            is_error = false;
        } else {
            is_error = true;
        }
        return is_error;
    }

    // validate email method...
    public boolean matchEmail() {
        boolean is_email = false;

        if (!EMAIL_ADDRESS_PATTERN.matcher(email_data.toString().trim()).matches()) {
            //Toast.makeText(this, "Invalid Email Id", Toast.LENGTH_SHORT).show();
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            is_email = false;
        } else {
            is_email = true;
        }
        return is_email;
    }

    // method to get subscription type list...
    private void GetSubscriptionType() {
        String Url = Constant.GET_SUBSCRIPTION_TYPE;
        System.out.println("GetSubscriptionType url : " + Url);

        try {
            new PostApiRequest().get_request_Object(getContext(), subscriptionType, Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner subscriptionType = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();

                JSONArray service = data.getJSONArray("data");
                for (int i = 0; i < service.length(); i++) {

                    JSONObject history = service.getJSONObject(i);
                    Trade_Promo_Code_Array getService = new Trade_Promo_Code_Array();

                    getService.setId(history.getString("id"));
                    getService.setRateType(history.getString("RateType"));
                    getService.setCalculatedamt(history.getString("Calculatedamt"));
                    getService.setSubscription_type_id(history.getString("Subscription_type_id"));

                    trade_promo_code_arrays.add(getService);
                }
                spinnerSubArray.clear();
                for (int i = 0; i < trade_promo_code_arrays.size(); i++) {
                    String c_name = trade_promo_code_arrays.get(i).getRateType();
                    System.out.println("c_names :" + c_name);

                    spinnerSubArray.add(c_name);
                }

                System.out.println("country array :" + spinnerSubArray.size());
                adapter = new ArrayAdapter<String>(
                        getContext(),
                        R.layout.trade_subscription_type_view,
                        spinnerSubArray
                );

                adapter.setDropDownViewResource(R.layout.trade_subscription_type_view);
                spinner_sub_type.setAdapter(adapter);

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    // method to put subscribe user..
    private void UpdateSubscribeUser() {
        String Url = Constant.UPDATE_SUBSCRIPTION_INFO;
        System.out.println("SubscribeUser url : " + Url);
        String year = credit_card_valid_through_data.substring(credit_card_valid_through_data.lastIndexOf("/") + 1);
        String month = credit_card_valid_through_data.substring(0, credit_card_valid_through_data.indexOf("/"));

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("promo_code", "");
        postParams.put("subcriptionMaster_id", subcriptionMaster_id);
        postParams.put("subscriptionType_id", subscriptionType_id);
        postParams.put("cardnumber", credit_card_no_data);
        postParams.put("month", month);
        postParams.put("year", "20" + year);
        postParams.put("cvv", credit_cvv_data);
        postParams.put("amount", subscription_amount);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getContext(), subscribeuser, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner subscribeuser = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getActivity(), data.getString("message"), Toast.LENGTH_SHORT).show();

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(getContext(), data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(getActivity());
        }
    };

    public void getSubscriptionData() {
        spinner_sub_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String watchlist_name = parent.getItemAtPosition(position).toString();

                System.out.println("watchlist_id :" + watchlist_name);
                for (int i = 0; i < trade_promo_code_arrays.size(); i++) {
                    if (watchlist_name.equalsIgnoreCase(trade_promo_code_arrays.get(i).getRateType())) {
                        subscriptionType_id = trade_promo_code_arrays.get(i).getSubscription_type_id();
                        subcriptionMaster_id = trade_promo_code_arrays.get(i).getId();
                        rateType = trade_promo_code_arrays.get(i).getRateType();
                        subscription_amount = trade_promo_code_arrays.get(i).getCalculatedamt();
                        System.out.println("aa subscriptionType_id :" + subscriptionType_id);
                        System.out.println("aa subcriptionMaster_id :" + subcriptionMaster_id);
                        System.out.println("aa RateType :" + rateType);

                        break;
                    }
                }

//                subscription_amount = watchlist_name.substring(watchlist_name.lastIndexOf("$") + 1);


            } // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }
}
