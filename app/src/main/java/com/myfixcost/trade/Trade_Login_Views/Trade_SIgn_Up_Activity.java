package com.myfixcost.trade.Trade_Login_Views;

import android.app.Activity;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.fingerprint.FingerprintManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.SwitchCompat;
import androidx.biometric.BiometricPrompt;
import androidx.core.content.ContextCompat;

import com.myfixcost.trade.Constant.Constant;
import com.myfixcost.trade.Constant.MyGlobalClass;
import com.myfixcost.trade.Dashboard.Trade_Dashboard_Activity;
import com.myfixcost.trade.Mysharedpreferences.MySharedPreference;
import com.myfixcost.trade.R;
import com.myfixcost.trade.Volley_Requests.FetchDataListner;
import com.myfixcost.trade.Volley_Requests.PostApiRequest;
import com.myfixcost.trade.Volley_Requests.RequestQueueService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.regex.Pattern;

public class Trade_SIgn_Up_Activity extends AppCompatActivity {

    LinearLayout lin_per_det, lin_line_sub_info, lin_terms_conditions;
    ImageView img_first,img_second, img_third,img_clear;
    Button btn_Next, btn_Subscribe;
    AppCompatButton btn_verify_otp;
    RelativeLayout src_subscription_info, rel_success;
    ScrollView src_personal_details;

    // personal details view...
    EditText ed_name, ed_email_id, ed_phone_no, ed_enter_otp, ed_password, ed_confirm_password;
    AppCompatButton btn_send_otp;
    TextView txt_resend_otp;

    // subscription info view
    EditText ed_promo_code, ed_credit_card_no, ed_valid_through, ed_cvv, ed_credit_name_on_card;
    CheckBox chkbox_terms_conditions;
    AppCompatButton btn_apply;

    // success view...
    LinearLayout lin_trade_acc;
    EditText ed_rithmic_id,ed_rithmic_pass;
    Button btn_explore_app;
    SwitchCompat switch_biometric;

    boolean newanConfirmPass = false;
    ArrayList<String> spinnerSubArray = new ArrayList<>();
    ArrayList<Trade_Sub_Info_Array> Trade_Sub_Info_Array = new ArrayList<>();
    ArrayList<Trade_Promo_Code_Array> trade_promo_code_arrays = new ArrayList<>();

    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,100}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    String email_data = "", name_data = "", enter_otp_data = "", password_data = "", confirm_password_data = "", phone_data = "",rithmic_pass_data = "";
    String promocode_data = "", credit_card_no_data = "", credit_card_valid_through_data = "", credit_cvv_data = "", credit_card_name_data = "", rithmic_id_data = "";
    String store_user_id, subscriptionType_id = "", subcriptionMaster_id = "", rateType, c_code, subscription_amount;
    private boolean is8char = false, hasUpper = false, hasnum = false, hasSpecialSymbol = false;
    ArrayAdapter<String> adapter;
    Spinner spinner_sub_type, spinner_country_code;
    ImageView img_back;
    boolean otp_confirmed, terms_conditions = false;
    MySharedPreference mySharedPreference;
    boolean subscribeValue = false;
    boolean newSwitchValue = false;

    KeyguardManager mgr;
    private androidx.biometric.BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;
    private Executor executor;
    RelativeLayout rel_biometric;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trade_signup_activity);

        mySharedPreference = new MySharedPreference(this);

        initUI();
        GetSubscriptionType();
        GetAppData();

        if (secureDevice()) {
            rel_biometric.setVisibility(View.VISIBLE);
        } else {
            rel_biometric.setVisibility(View.GONE);
//            Toast.makeText(Trade_SIgn_Up_Activity.this, "Your device has no lock...", Toast.LENGTH_SHORT).show();
        }

        lin_terms_conditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewDialog alert = new ViewDialog();
                alert.showDialog(Trade_SIgn_Up_Activity.this);
            }
        });

        btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!validateAllChecks()) {

                } else {
                    img_second.setImageResource(R.drawable.ic_first_round);
                    lin_per_det.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.color.app_color));
                    src_personal_details.setVisibility(View.GONE);
                    src_subscription_info.setVisibility(View.VISIBLE);
                    subscribeValue = true;
//                    PostPersonalInfoData();
                }
            }
        });

        btn_Subscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (credit_card_no_data.equalsIgnoreCase("")) {
                    ed_credit_card_no.setError("Enter Card No");
                } else if (credit_card_valid_through_data.equalsIgnoreCase("")) {
                    ed_credit_card_no.setError(null);
                    ed_valid_through.setError("Enter Card Validity");
                } else if (credit_cvv_data.equalsIgnoreCase("")) {
                    ed_valid_through.setError(null);
                    ed_cvv.setError("Enter Cvv No");
                } else if (!terms_conditions) {
                    Toast.makeText(Trade_SIgn_Up_Activity.this, "Please accept Terms And Conditions", Toast.LENGTH_SHORT).show();
                } else {
//                    MySubscribeUser();
                    ed_promo_code.setError(null);
                    SubscribeUser();
                }
            }
        });

        btn_send_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone_data.equalsIgnoreCase("")) {
                    ed_phone_no.setError("Enter Phone No");
                } else {
                    txt_resend_otp.setVisibility(View.VISIBLE);
                    sendOTP();
                }
            }
        });

        btn_verify_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!enter_otp_data.equalsIgnoreCase("")) {
                    verifyOTP();
                } else {
                    ed_enter_otp.setError("Enter OTP");
                }
            }
        });

        btn_explore_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!rithmic_id_data.equalsIgnoreCase("") && !rithmic_pass_data.equalsIgnoreCase("")) {
                    PostrithmicID();
                } else {
                    startActivity(new Intent(Trade_SIgn_Up_Activity.this, Trade_Dashboard_Activity.class));
                    finish();
                    ((MyGlobalClass) getApplicationContext()).setGuest_User("0");
                    ((MyGlobalClass) getApplicationContext()).setRithmic_id("0");
                }
            }
        });

        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (subscribeValue) {
                    img_first.setImageResource(R.drawable.ic_first_round);
                    img_second.setImageResource(R.drawable.ic_second_round);
                    lin_per_det.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.color.subscription_line));
                    src_personal_details.setVisibility(View.VISIBLE);
                    src_subscription_info.setVisibility(View.GONE);
                    subscribeValue = false;
                } else {
                    finish();
                }
            }
        });

        txt_resend_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (phone_data.equalsIgnoreCase("")) {
                    ed_phone_no.setError("Enter Phone No");
                } else {
                    sendOTP();
                }
            }
        });

        btn_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!promocode_data.equalsIgnoreCase("")) {
                    ApplyPromoCode();
                } else {
                    ed_promo_code.setError("Enter PromoCode");
                }
            }
        });

        lin_trade_acc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("url :" + ((MyGlobalClass) getApplicationContext()).getTrading_url());
                Uri uri = Uri.parse(((MyGlobalClass) getApplicationContext()).getTrading_url());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        img_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ed_promo_code.getText().clear();
                GetSubscriptionType();
            }
        });

        ed_valid_through.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    ed_valid_through.getText().clear();
                }
                return false;
            }
        });

        getSubscriptionData();
        getCountryCode();
        checkTermsConditions();

        String[] arrayCountryCode = new String[] {
               "USA +1","IND +91"
        };

        adapter = new ArrayAdapter<String>(
                getApplicationContext(),
                R.layout.trade_country_code_view,
                arrayCountryCode
        );

        adapter.setDropDownViewResource(R.layout.trade_country_code_view);
        spinner_country_code.setAdapter(adapter);

//        Drawable img = getApplicationContext().getResources().getDrawable(R.drawable.edittext_check_circle_24);
//        txtInputEdt_Cprno.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
    }

    private void initUI() {
        lin_terms_conditions = findViewById(R.id.lin_terms_conditions);
        lin_per_det = findViewById(R.id.lin_per_det);
        img_second = findViewById(R.id.img_second);
        img_first = findViewById(R.id.img_first);
        btn_Next = findViewById(R.id.btn_Next);
        src_subscription_info = findViewById(R.id.src_subscription_info);
        rel_success = findViewById(R.id.rel_success);
        src_personal_details = findViewById(R.id.src_personal_details);
        btn_Subscribe = findViewById(R.id.btn_Subscribe);
        img_third = findViewById(R.id.img_third);
        lin_line_sub_info = findViewById(R.id.lin_line_sub_info);
        btn_verify_otp = findViewById(R.id.btn_verify_otp);

        ed_name = findViewById(R.id.ed_name);
        ed_email_id = findViewById(R.id.ed_email_id);
        ed_phone_no = findViewById(R.id.ed_phone_no);
        ed_enter_otp = findViewById(R.id.ed_enter_otp);
        ed_password = findViewById(R.id.ed_password);
        ed_confirm_password = findViewById(R.id.ed_confirm_password);
        btn_send_otp = findViewById(R.id.btn_send_otp);
        txt_resend_otp = findViewById(R.id.txt_resend_otp);

        ed_promo_code = findViewById(R.id.ed_promo_code);
        ed_credit_card_no = findViewById(R.id.ed_credit_card_no);
        ed_valid_through = findViewById(R.id.ed_valid_through);
        ed_cvv = findViewById(R.id.ed_cvv);
        ed_credit_name_on_card = findViewById(R.id.ed_credit_name_on_card);
        chkbox_terms_conditions = findViewById(R.id.chkbox_terms_conditions);
        btn_apply = findViewById(R.id.btn_apply);
        img_clear = findViewById(R.id.img_clear);

        lin_trade_acc = findViewById(R.id.lin_trade_acc);
        ed_rithmic_id = findViewById(R.id.ed_rithmic_id);
        ed_rithmic_pass = findViewById(R.id.ed_rithmic_pass);
        btn_explore_app = findViewById(R.id.btn_explore_app);
        switch_biometric = findViewById(R.id.switch_biometric);

        spinner_sub_type = findViewById(R.id.spinner_sub_type);
        spinner_country_code = findViewById(R.id.spinner_country_code);
        img_back = findViewById(R.id.img_back);
        rel_biometric = findViewById(R.id.rel_biometric);

        editTextEmail(ed_email_id);
        editTextName(ed_name);
        editPassword(ed_password);
        editRithmicPassword(ed_rithmic_pass);
        editConfirmPassword(ed_confirm_password);
        editPhoneNo(ed_phone_no);
        editEnterOTP(ed_enter_otp);
        editrithmicID(ed_rithmic_id);
        editPromoCode(ed_promo_code);
        editCreditCardValidThrough(ed_valid_through);
        editCreditCardCVV(ed_cvv);
        editCreditCardName(ed_credit_name_on_card);
        editCreditCardNo(ed_credit_card_no);

        mgr = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);
        executor = ContextCompat.getMainExecutor(this);

        BiometricSwitchChange(switch_biometric);
        if (newSwitchValue) {
            mySharedPreference.setisSwitchEnable("1");
        } else {
            mySharedPreference.setisSwitchEnable("0");
        }
    }

    private void BiometricSwitchChange(SwitchCompat switch_biometric) {
        switch_biometric.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position
                if (isChecked) {
                    mySharedPreference.setisSwitchEnable("1");
                    newSwitchValue = true;
                } else {
                    mySharedPreference.setisSwitchEnable("0");
                    newSwitchValue = false;
                }
            }
        });
    }

    public class ViewDialog {

        public void showDialog(Activity activity) {
            final Dialog dialog = new Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.setContentView(R.layout.trade_terms_and_conditions_dialog);

            ImageView img_close = dialog.findViewById(R.id.img_close);
            Button btn_ok = dialog.findViewById(R.id.btn_ok);
            TextView txt_terms_condition = dialog.findViewById(R.id.txt_terms_condition);

            txt_terms_condition.setText(((MyGlobalClass) getApplicationContext()).getAgreement_text());

            btn_ok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            img_close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();

        }
    }

    // validate email method...
    public boolean matchEmail() {
        boolean is_email = false;

        if (!EMAIL_ADDRESS_PATTERN.matcher(email_data.toString().trim()).matches()) {
            //Toast.makeText(this, "Invalid Email Id", Toast.LENGTH_SHORT).show();
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            is_email = false;
        } else {
            is_email = true;
        }
        return is_email;
    }

    // get email method...
    public void editTextEmail(EditText edEmail) {
        edEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    email_data = s.toString();
                }
            }
        });
    }

    // get name method...
    public void editTextName(EditText nameEmail) {
        nameEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    name_data = s.toString();
                }
            }
        });
    }

    // get enter OTP method...
    public void editEnterOTP(EditText otpEmail) {
        otpEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    enter_otp_data = "";
                    //txt_resend_otp.setVisibility(View.GONE);
                    btn_verify_otp.setEnabled(false);
                    btn_verify_otp.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_send_otp));
                } else {
                    if (s.toString().length() == 6) {
                        btn_verify_otp.setEnabled(true);
//                        txt_resend_otp.setVisibility(View.VISIBLE);
                        btn_verify_otp.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_email_login));
                        enter_otp_data = s.toString();
                    } else {
//                        txt_resend_otp.setVisibility(View.GONE);
                        btn_verify_otp.setEnabled(false);
                        btn_verify_otp.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_send_otp));
                    }
                }
            }
        });
    }

    // get password method...
    public void editPassword(EditText passwordEmail) {
        passwordEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    password_data = "";
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    password_data = s.toString();
                    passwordValidate(passwordEmail);
                }
            }
        });
    }

    // get phone no method...
    public void editPhoneNo(EditText phoneno) {
        phoneno.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    btn_send_otp.setEnabled(false);
                    btn_send_otp.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_send_otp));
                    phone_data = "";
                    txt_resend_otp.setVisibility(View.GONE);
                } else {
                    if (s.toString().length() == 10) {
                        btn_send_otp.setEnabled(true);
                        btn_send_otp.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_email_login));
                        phone_data = s.toString();
                    } else {
                        txt_resend_otp.setVisibility(View.GONE);
                        btn_send_otp.setEnabled(false);
                        btn_send_otp.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_send_otp));
                    }
                }
            }
        });
    }

    // get confirm password method...
    public void editConfirmPassword(EditText confirmpasswordEmail) {
        confirmpasswordEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    confirm_password_data = s.toString();
                    passwordValidate(confirmpasswordEmail);
                }
            }
        });
    }

    // get promo code method...
    public void editPromoCode(EditText promocode) {
        promocode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    img_clear.setVisibility(View.GONE);
                    promocode_data = "";
                    btn_apply.setEnabled(false);
                    btn_apply.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_send_otp));
                } else {
                    promocode_data = s.toString();
                    img_clear.setVisibility(View.VISIBLE);
                    btn_apply.setEnabled(true);
                    btn_apply.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_email_login));

                }
            }
        });
    }

    // get credit card no method...
    public void editCreditCardNo(EditText ed_credit_card_no) {
        ed_credit_card_no.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    if (s.toString().length() == 16) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                    }
                    credit_card_no_data = s.toString();
                }
            }
        });
    }


    // get credit card valid through method...
    public void editCreditCardValidThrough(EditText ed_valid_through) {
        ed_valid_through.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {

                    if (s.toString().length() == 2) {
                        s.append('/');
                    } else {
                        s.append("");
                       // ed_valid_through.setText(ed_valid_through.getText().delete(ed_valid_through.length() - 1, ed_valid_through.length()));
                    }
                    credit_card_valid_through_data = s.toString();
                }
            }
        });
    }

    // get credit card cvv method...
    public void editCreditCardCVV(EditText ed_credit_cvv) {
        ed_credit_cvv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    credit_cvv_data = s.toString();
                }
            }
        });
    }

    // get rithmic ID method...
    public void editrithmicID(EditText ed_rithmic_id) {
        ed_rithmic_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    rithmic_id_data = s.toString();
                }
            }
        });
    }

    // get rithmic password method...
    public void editRithmicPassword(EditText Rithmicpassword) {
        Rithmicpassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
                    rithmic_pass_data = "";
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    rithmic_pass_data = s.toString();
                    passwordValidate(Rithmicpassword);
                }
            }
        });
    }

    // get credit card name method...
    public void editCreditCardName(EditText ed_credit_name_on_card) {
        ed_credit_name_on_card.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().length() == 0) {
//                    btnLogin.setEnabled(false);
//                    btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
                } else {
                    credit_card_name_data = s.toString();
                }
            }
        });
    }

    // method is to check password validations...
    private void passwordValidate(EditText ed_password) {
        String password = ed_password.getText().toString();
        // 8 character
        if (ed_password.length() >= 8) {
            is8char = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            is8char = false;
        }
        //number
        if (password.matches("(.*[0-9].*)")) {
            hasnum = true;
        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasnum = false;

        }
        //upper case
        if (password.matches("(.*[A-Z].*)")) {
            hasUpper = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasUpper = false;

        }
        //symbol
        if (password.matches("^(?=.*[_.()$&@!]).*$")) {
            hasSpecialSymbol = true;

        } else {
            //ed_password.setError("Invalid Password");
//            btnLogin.setEnabled(false);
//            btnLogin.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.drawable.background_sign_in_disable_button));
            hasSpecialSymbol = false;

        }
    }

    // method to post data of personal information on sign up page...
    private void PostPersonalInfoData() {
        String Url = Constant.API_PERSONAL_INFO;
        System.out.println("PostPersonalInfoData url : " + Url);
        JSONObject jsonObject = new JSONObject();

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("userId", "0");
        postParams.put("name", name_data);
        postParams.put("contact", c_code + "" + phone_data);
        postParams.put("email", email_data);
        postParams.put("password", password_data);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getApplicationContext(), getMasterResons1, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons1 = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
                img_second.setImageResource(R.drawable.ic_first_round);
                lin_per_det.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.color.app_color));
                src_personal_details.setVisibility(View.GONE);
                src_subscription_info.setVisibility(View.VISIBLE);

                JSONObject info = data.getJSONObject("data");
                store_user_id = info.getString("UserId");
                mySharedPreference.setisCustomerId(store_user_id);
                info.getString("Name");
                info.getString("Contact");
                info.getString("Email");

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_SIgn_Up_Activity.this);
        }
    };

    // validate all checks of personal details...
//    public boolean validateAllChecks() {
//        boolean is_error = false;
//        if (name_data.equalsIgnoreCase("")) {
//            ed_name.setError("Enter Name");
//            is_error = false;
//        } else if (email_data.equalsIgnoreCase("")) {
//            ed_email_id.setError("Invalid Email");
//            is_error = false;
//        } else if (password_data.equalsIgnoreCase("")) {
//            ed_password.setError("Password needs to be 8 char long  with minimum 1 capital letter, 1 number, 1 special character");
//            is_error = false;
//        } else if (confirm_password_data.equalsIgnoreCase("")) {
//            ed_confirm_password.setError("Enter Confirm Password");
//            is_error = false;
//        } else if (enter_otp_data.equalsIgnoreCase("")) {
//            ed_enter_otp.setError("Enter OTP");
//            is_error = false;
//        } else if (phone_data.equalsIgnoreCase("")) {
//            ed_phone_no.setError("Enter Phone No");
//            is_error = false;
//        } else if (!matchEmail()) {
//            ed_email_id.setError("Invalid EmailID");
//            is_error = false;
//        } else if (!is8char || !hasnum || !hasUpper || !hasSpecialSymbol) {
//            ed_password.setError("Invalid Password");
//            is_error = false;
//        } else if (!checkNewAnConfirmPass()) {
//            is_error = false;
//        } else if (!otp_confirmed) {
//            ed_enter_otp.setError("OTP not confirmed");
//            is_error = false;
//        } else {
//            is_error = true;
//        }
//        return is_error;
//    }

    public boolean validateAllChecks() {
        boolean is_error = false;
        if (name_data.equalsIgnoreCase("")) {
            ed_name.setError("Enter Name");
            is_error = false;
        } else if (email_data.equalsIgnoreCase("")) {
            ed_email_id.setError("Invalid Email Id");
            is_error = false;
        } else if (phone_data.equalsIgnoreCase("")) {
            ed_phone_no.setError("Enter Phone No");
            is_error = false;
        } else if (enter_otp_data.equalsIgnoreCase("")) {
            ed_enter_otp.setError("Enter OTP");
            is_error = false;
        } else if (password_data.equalsIgnoreCase("")) {
            ed_password.setError("Password needs to be 8 char long  with minimum 1 capital letter, 1 number, 1 special character");
            is_error = false;
        } else if (confirm_password_data.equalsIgnoreCase("")) {
            ed_confirm_password.setError("Enter Confirm Password");
            is_error = false;
        } else if (!matchEmail()) {
            ed_email_id.setError("Invalid Email Id");
            is_error = false;
        } else if (!is8char || !hasnum || !hasUpper || !hasSpecialSymbol) {
            ed_password.setError("Invalid Password");
            is_error = false;
        } else if (!checkNewAnConfirmPass()) {
            is_error = false;
        } else if (!otp_confirmed) {
            ed_enter_otp.setError("OTP not confirmed");
            is_error = false;
        } else {
            is_error = true;
        }
        return is_error;
    }

    // method to check password and confirm password...
    public boolean checkNewAnConfirmPass() {
        if (password_data.equalsIgnoreCase(confirm_password_data)) {
            newanConfirmPass = true;
        } else {
            newanConfirmPass = false;
            ed_confirm_password.setError("Password and Confirm password should be same");
        }
        return newanConfirmPass;
    }

    // method to send otp on mobile no...
    private void sendOTP() {
        String Url = Constant.SEND_OTP;
        System.out.println("sendOTP url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("contact", phone_data);
        postParams.put("email", email_data);
        postParams.put("flag", "Create");
        postParams.put("country_code", c_code);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getApplicationContext(), getMasterResons, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getMasterResons = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_SIgn_Up_Activity.this);
        }
    };

    // method to verify OTP...
    private void verifyOTP() {
        String Url = Constant.VERIFY_OTP;
        System.out.println("verifyOTP url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("contact", phone_data);
        postParams.put("Otp", enter_otp_data);
        postParams.put("country_code", c_code);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getApplicationContext(), verifyotp, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner verifyotp = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                otp_confirmed = true;
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();

            } else {
                RequestQueueService.cancelProgressDialog();
                otp_confirmed = false;
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
            otp_confirmed = true;
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_SIgn_Up_Activity.this);
        }
    };

    // method to put rithmic ID
    private void PostrithmicID() {
        String Url = Constant.ADD_RITHMIC_ID;
        System.out.println("PostrithmicID url : " + Url);

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("rithimic_id", rithmic_id_data);
        postParams.put("rithimic_Password", rithmic_pass_data);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().put_request_Object(getApplicationContext(), rithmicID, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner rithmicID = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                JSONObject info = data.getJSONObject("data");
                String Rithimic_id = info.getString("Rithimic_id");

                if (!Rithimic_id.equalsIgnoreCase("")) {
                    ((MyGlobalClass) getApplicationContext()).setRithmic_id("1");
                    mySharedPreference.setisRithmicId("1");
                } else {
                    ((MyGlobalClass) getApplicationContext()).setRithmic_id("0");
                    mySharedPreference.setisRithmicId("0");
                }

                startActivity(new Intent(Trade_SIgn_Up_Activity.this, Trade_Dashboard_Activity.class));
                finish();
                ((MyGlobalClass) getApplicationContext()).setGuest_User("0");
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_SIgn_Up_Activity.this);
        }
    };

    // method to get subscription type list...
    private void GetSubscriptionType() {
        String Url = Constant.GET_SUBSCRIPTION_TYPE;
        System.out.println("GetSubscriptionType url : " + Url);
        trade_promo_code_arrays.clear();
        try {
            new PostApiRequest().get_request_Object(getApplicationContext(), subscriptionType, Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner subscriptionType = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();

                JSONArray service = data.getJSONArray("data");
                for (int i = 0; i < service.length(); i++) {

                    JSONObject history = service.getJSONObject(i);
                    Trade_Promo_Code_Array getService = new Trade_Promo_Code_Array();

                    getService.setId(history.getString("id"));
                    getService.setRateType(history.getString("RateType"));
                    getService.setCalculatedamt(history.getString("Calculatedamt"));
                    getService.setSubscription_type_id(history.getString("Subscription_type_id"));

                    trade_promo_code_arrays.add(getService);
                }
                spinnerSubArray.clear();
                for (int i = 0; i < trade_promo_code_arrays.size(); i++) {
                    String c_name = trade_promo_code_arrays.get(i).getRateType();
                    System.out.println("c_names :" + c_name);

                    spinnerSubArray.add(c_name);
                }

                System.out.println("country array :" + spinnerSubArray.size());
                adapter = new ArrayAdapter<String>(
                        getApplicationContext(),
                        R.layout.trade_subscription_type_view,
                        spinnerSubArray
                );

                adapter.setDropDownViewResource(R.layout.trade_subscription_type_view);
                spinner_sub_type.setAdapter(adapter);

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            //RequestQueueService.showProgressDialog(Activity_Full_Information.this);
        }
    };

    // method to apply promo code...
    private void ApplyPromoCode() {
        String Url = Constant.APPLY_PROMO_CODE;
        System.out.println("ApplyPromoCode url : " + Url);

        trade_promo_code_arrays.clear();

        final HashMap<String, String> postParams = new HashMap<String, String>();
        postParams.put("pro_code", promocode_data);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().post_request_Object(getApplicationContext(), applypromocode, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner applypromocode = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();

                JSONObject get_data = data.getJSONObject("data");
                JSONArray service = get_data.getJSONArray("SubList");
                for (int i = 0; i < service.length(); i++) {
                    JSONObject history = service.getJSONObject(i);
                    Trade_Promo_Code_Array getService = new Trade_Promo_Code_Array();

                    getService.setId(history.getString("id"));
                    getService.setRateType(history.getString("RateType"));
                    getService.setCalculatedamt(history.getString("Calculatedamt"));
                    getService.setSubscription_type_id(history.getString("Subscription_type_id"));

                    trade_promo_code_arrays.add(getService);
                }
                spinnerSubArray.clear();
                for (int i = 0; i < trade_promo_code_arrays.size(); i++) {
                    String c_name = trade_promo_code_arrays.get(i).getRateType();
                    System.out.println("c_names :" + c_name);

                    spinnerSubArray.add(c_name);
                }

                System.out.println("country array :" + spinnerSubArray.size());
                adapter = new ArrayAdapter<String>(
                        getApplicationContext(),
                        R.layout.trade_subscription_type_view,
                        spinnerSubArray
                );

                adapter.setDropDownViewResource(R.layout.trade_subscription_type_view);
                spinner_sub_type.setAdapter(adapter);

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_SIgn_Up_Activity.this);
        }
    };

    // method to put subscribe user..
    private void SubscribeUser() {
        String Url = Constant.SUBSCRIBE_USER;
        System.out.println("SubscribeUser url : " + Url);
        String year = credit_card_valid_through_data.substring(credit_card_valid_through_data.lastIndexOf("/") + 1);
        String month = credit_card_valid_through_data.substring(0, credit_card_valid_through_data.indexOf("/"));

        final HashMap<String, String> postParams = new HashMap<String, String>();
        //postParams.put("userId", mySharedPreference.getisCustomerId());
        postParams.put("name", name_data);
        postParams.put("contact", phone_data);
        postParams.put("email", email_data);
        postParams.put("password", password_data);
        postParams.put("promo_code", promocode_data);
        postParams.put("subcriptionMaster_id", subcriptionMaster_id);
        postParams.put("subscriptionType_id", subscriptionType_id);
        postParams.put("cardnumber", credit_card_no_data);
        postParams.put("month", month);
        postParams.put("year", "20" + year);
        postParams.put("cvv", credit_cvv_data);
        postParams.put("amount", subscription_amount);
        postParams.put("country_code", c_code);

        System.out.println("jsonobject :" + postParams.toString());

        try {
            new PostApiRequest().put_request_Object(getApplicationContext(), subscribeuser, new JSONObject(postParams), Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner subscribeuser = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();

                img_third.setImageResource(R.drawable.ic_first_round);
                lin_line_sub_info.setBackground(ContextCompat.getDrawable(getApplicationContext(), R.color.app_color));
                src_personal_details.setVisibility(View.GONE);
                src_subscription_info.setVisibility(View.GONE);
                rel_success.setVisibility(View.VISIBLE);
                subscribeValue = false;

                JSONObject info = data.getJSONObject("data");
                store_user_id = info.getString("UserId");
                mySharedPreference.setisCustomerId(store_user_id);
                info.getString("Name");
                info.getString("Contact");
                info.getString("Email");

            } else {
                RequestQueueService.cancelProgressDialog();
                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            RequestQueueService.showProgressDialog(Trade_SIgn_Up_Activity.this);
        }
    };


    public void getSubscriptionData() {
        spinner_sub_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String watchlist_name = parent.getItemAtPosition(position).toString();

                System.out.println("watchlist_id :" + watchlist_name);
                for (int i = 0; i < trade_promo_code_arrays.size(); i++) {
                    if (watchlist_name.equalsIgnoreCase(trade_promo_code_arrays.get(i).getRateType())) {
                        subscriptionType_id = trade_promo_code_arrays.get(i).getSubscription_type_id();
                        subcriptionMaster_id = trade_promo_code_arrays.get(i).getId();
                        rateType = trade_promo_code_arrays.get(i).getRateType();
                        subscription_amount = trade_promo_code_arrays.get(i).getCalculatedamt();
                        System.out.println("aa subscriptionType_id :" + subscriptionType_id);
                        System.out.println("aa subcriptionMaster_id :" + subcriptionMaster_id);
                        System.out.println("aa RateType :" + rateType);

                        break;
                    }
                }
            } // to close the onItemSelected

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void getCountryCode() {
        spinner_country_code.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String country_code = parent.getItemAtPosition(position).toString();

                c_code = country_code.substring(country_code.lastIndexOf("+") + 1);
                System.out.println("country_code :" + c_code);

            }

            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void checkTermsConditions() {
        chkbox_terms_conditions.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    terms_conditions = true;
                } else {
                    terms_conditions = false;
                }

            }
        });
    }

    // method to get app data...
    private void GetAppData() {
        String Url = Constant.GET_APP_DATA;
        System.out.println("GetAppData url : " + Url);

        try {
            new PostApiRequest().get_request_Object(getApplicationContext(), getappdata, Url);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    FetchDataListner getappdata = new FetchDataListner() {
        @Override
        public void onFetchComplete(JSONArray data) {

        }

        @Override
        public void onFetchComplete(JSONObject data) throws JSONException {
            System.out.println("#@ get services : " + data.toString());

            if (data.getString("status").equalsIgnoreCase("200")) {
//                RequestQueueService.cancelProgressDialog();
//                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();

                JSONArray service = data.getJSONArray("data");
                JSONObject url = service.getJSONObject(0);
                JSONObject agrre = service.getJSONObject(1);

                String trade_url = url.getString("Values");
                String agreement_text = agrre.getString("Values");

                ((MyGlobalClass) getApplicationContext()).setTrading_url(trade_url);
                ((MyGlobalClass) getApplicationContext()).setAgreement_text(agreement_text);

                System.out.println("result :" + trade_url + "  " + agreement_text);


            } else {
//                RequestQueueService.cancelProgressDialog();
//                Toast.makeText(Trade_SIgn_Up_Activity.this, data.getString("message"), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onFetchFailure(String msg) {
            System.out.println("#@ Response of master Reson Fail : " + msg);
            Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
            RequestQueueService.cancelProgressDialog();
        }

        @Override
        public void onFetchStart() {
            System.out.println("#@ Master reson Api resone  :");
            //RequestQueueService.showProgressDialog(Activity_Full_Information.this);
        }
    };

    public boolean secureDevice() {
        boolean is_lock = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) getApplicationContext().getSystemService(Context.FINGERPRINT_SERVICE);
            if (mgr.isDeviceSecure() && mgr.isKeyguardSecure()) {
                System.out.println();
                is_lock = true;
//            } else if (fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints()) {
            } else if (fingerprintManager != null && fingerprintManager.isHardwareDetected() && fingerprintManager.hasEnrolledFingerprints()) {
                is_lock = true;
            } else {
                is_lock = false;
            }
        }
        return is_lock;
    }
}
